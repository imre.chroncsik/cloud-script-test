
//  https://git.vzbuilders.com/immersive/scripting-api/blob/master/types/arsdk/index.d.ts

 declare module "arsdk" {
   
  export class ARExperience {
    readonly uid: string;

    /**
     * Set the entry point of each object script.
     * An object script gets executed whenever the (GLTF) object is instantiated, either in 3D preview mode or in AR mode.
     */
    addObjectScript(objectUid: String, onObjectInstantiated: (root: Entity) => void, onObjectDestroyed?: () => void): void;

    readonly currentMode: ARMode | PreviewMode;
    /**
     * Sets a callback for when the experience has changed between AR and 3D preview mode.
     */
    addOnModeChangedListener(onModeChanged: (currentMode: ARMode | PreviewMode) => void): void;

    addSystem(system: System): void;
    removeSystem(system: System): void;

    forEachComponent<T extends Component>(classDef: ComponentClassDefinition<T>, callback: (component: T) => void): void;
  }

  export class ARMode {
    public readonly isAR: true;
    /**
     * The AR camera.
     * Its Transform is read-only. Writing to it will throw an exception.
     */
    public readonly camera: Entity;
  }

  export class PreviewMode {
    public readonly isAR: false;
    public readonly camera: PreviewCamera;
    //public background: Color;
  }

  /**
   * The camera in 3D Preview Mode. Has an OrbitCameraController component.
   */
   export class PreviewCamera {
    public readonly entity: Entity;
    public get transform(): Transform;
    public get orbitCameraController(): OrbitCameraController;
  }

  type ComponentClassDefinition<T> = new (...args: any[]) => T;

  export class Entity {
    name: string;

    /**
     * Shortcut for entity.transform.parent.entity.
     * If the parent is null, this means this entity is attached to the scene root.
     */
    parent: Entity | null;

    /**

     * This is the same as getComponent(Transform), but can be accessed directly as every Entity has exactly one Transform component.
     */
    readonly transform: Transform;

    forEachDescendantInHierarchy(callback: (entity: Entity) => void): void;
    findDescendantInHierarchy(predicate: (entity: Entity) => Boolean): Entity | null;

    forEachChild(callback: (child: Entity) => void): void;
    findChild(predicate: (child: Entity) => Boolean): Entity | null;

    getChild(index: number): Entity | null;
    childCount(): number;

    /**
     * Some component types can only be added once to an entity.
     * If a component could not be added, this function returns null.
     */
    addComponent<T extends Component>(component: T): T | null;
    /**
     * Returns the component that was removed or null if the component could not be removed.
     * Transform components can not be removed.
     */
    removeComponent<T extends Component>(component: T): T | null;

    /**
     * Returns the {@link Component} of a specific type if such a Component is attached to this Entity.
     * If multiple Components of the same type are attached, then this function returns the first one.
     * Returns null if no such Component is attached.
     */
    getComponent<T extends Component>(classDef: ComponentClassDefinition<T>): T | null;

    /**
     * Returns all components of a specific type of this Entity. This array is returned as a copy.
     */
    getComponents<T extends Component>(classDef: ComponentClassDefinition<T>): Array<T>;

    /**
     * Sets the active state of this Entity.
     * If this Entity is inactive, then all components of this Entity
     * and all components of its child entities will not be updated or have any effect.
     */
    setActive(active: Boolean): void;

    /**
     * Destroys this Entity. This will destroy all components and all child entities and their components.
     *
     */
    destroy(): void;
  }

  /**
   * Can be subclassed to create custom components.
   */
  export class Component {
    readonly entity: Entity | null;
  }

  /**
   * An entity has always exactly one Transform.
   */
  export class Transform extends Component {
    /**
     * If the parent is null, this means this transform is attached to the scene root.
     */
    parent: Transform | null;

    forEachDescendantInHierarchy(callback: (transform: Transform) => void): void;
    findDescendantInHierarchy(predicate: (transform: Transform) => Boolean): Transform | null;

    forEachChild(callback: (child: Transform) => void): void;
    findChild(predicate: (child: Transform) => Boolean): Transform | null;

    getChild(index: number): Transform | null;
    childCount(): number;

    //these are returned as references to avoid memory allocations
    readonly localPosition: Vector3;
    readonly localRotation: Quaternion;
    readonly localScale: Vector3;

    readonly worldPosition: Vector3;
    readonly worldRotation: Quaternion;
    readonly worldScale: Vector3;

    /**
     * Returns if this Transform is readonly.
     * If it is read-only then writing any of its fields will thrown an exception.
     */
    readonly isReadOnly: boolean;
  }

  /**
   * Component to handle tap interactions. onTapCallback is called whenever the bounding box of the entity is tapped.
   */
  export class OnTap extends Component {
    constructor(onTapCallback: (hitPointWorld: Vector3) => void);
  }

  /**
   * A MeshRenderable can contain mutiple "submeshes/primitives", each having their own material.
   * This matches the GLTF concept of meshes with primitives
   * , see https://github.com/KhronosGroup/glTF-Tutorials/blob/master/gltfTutorial/gltfTutorial_009_Meshes.md
   */
  export class MeshRenderable extends Component {
    /**
     * This component is automatically created when a GLTF object is loaded. Instantiating it manually is invalid.
     * TODO: How do we handle this?
     */
    constructor();
    //enabled: boolean;

    castShadows: boolean;
    receiveShadows: boolean;

    getPrimitiveCount(): number;
    //shortcut for materials[0]
    material: Material;

    setMaterialAt(primitive: number, material: Material): void;
    readonly materials: ReadonlyArray<Material>;
  }

  /**
   * Materials will have only the uniforms that are defined in the GLTF.
   *
   * Supported extensions:
   * https://docs.google.com/spreadsheets/d/19m8WCLFmcKZdNHQhAXpHvEynB1MeCSqcvbCqWhj-48k/edit#gid=0
   */
  export class Material {
    clone(): Material;

    //every GLTF defined material has a color factor
    color: Color;
    //Setting the opacity of an opaque material has no effect. Use "setFade()" if you want to fade a material.
    opacity: number | null;
    metalness: number | null;
    roughness: number | null;
    emissive: Color | null;

    //For fading opaque materials, we would want to have an option to change the transparency mode,
    // however ThreeJS currently does not support "Transparent" mode, only "Fade".
    //see https://google.github.io/filament/Materials.html#materialdefinitions/materialblock/blendingandtransparency:blending
    //https://docs.unity3d.com/Manual/StandardShaderMaterialParameterRenderingMode.html
    //blendingMode : BlendingMode (Opaque, Transparent, Fade, Masked)

    //Fade a material to a specified opacity. This may internally change the rendering mode of the material.
    //TODO this helper function should be removed and replaced with a BlendingMode setting.
    setFade(opacity: number): any;

    /*
      In the future we would probably want to expose access to arbitrary named uniforms.
      
      - Unused uniforms can be requested as GLTF extras.
      - We will have to align exposed uniform names between platforms.
      For example "baseColorFactor", "baseColorMap", "metallicFactor", "roughnessFactor", "metallicRoughnessMap"

      getFloat(uniform: string): number | null;
      getFloatArray(uniform: string): Array<number> | null;
      getTexture(uniform: string): Texture | null;
      getColor(uniform: string): Color | null
  
      setFloat(uniform: string, value: number): void;
      setFloatArray(uniform: string, values: Array<number>): void
      setColor(uniform: string, color: Color): void
      setTexture(uniform: string, texture: Texture): void;
      //For video textures
      //setExternalTexture(uniform: string, texture: ExternalTexture): void;
      //This would be useful for debugging
      //getAvailableMaterialParameters():  Array<string>;
      */
  }

  /**
   * Only one ObjectAnimator can be attached to an entity.
   */
  export class ObjectAnimator extends Component {
    /**
     * This component is automatically created when a GLTF object is loaded. Instantiating it manually is invalid.
     * TODO: How do we handle this?
     */
    constructor();

    /**
     * Returns a reference to an Animation defined for this ObjectInstance.
     * If a new Animation is started, an already running Animation will be cancelled.
     */
    getAnimation(animationName: string): Animation;
    getAnimations(): ReadonlyArray<Animation>; 
  }

  /**
   * Only one Animation can be running on one ObjectAnimator.
   * If a new Animation is started, an already running Animation will be cancelled.
   */
  export class Animation {
    private constructor();
    readonly name: string;
    /**
     * Whether the Animation replays after it finishes. When set to false on a playing Animation, it is stopped at the end of the loop.
     */
    loop: boolean;
    // /**
    //  * If two animations are in the same layer, only one of them can play at a time.
    //  * Starting one will stop the other. If two animations exist on separate layers, they can play at the same time.
    //  */
    layer: number;
    start(): Animation;
    pause(): Animation;
    resume(): Animation;
    isRunning(): boolean;
    /**
     * float, in seconds
     */
    getDuration(): number;
    /**
     * float, in percent [0;1]
     */
    getCurrentProgress(): number;
  }

  export class Vector3 {
    /**
     *
     * @param x the x value of this vector. Default is 0.
     * @param y the y value of this vector. Default is 0.
     * @param z the z value of this vector. Default is 0.
     */
    constructor(x?: number, y?: number, z?: number);
    x: number;
    y: number;
    z: number;

    /**
     * Adds v to this vector.
     */
    add(v: Vector3): this;

    /**
     * Adds the scalar value s to this vector's x, y and z values.
     */
    addScalar(s: number): this;

    /**
     * Adds the multiple of v and s to this vector.
     */
    addScaledVector(v: Vector3, s: number): this;

    /**
     * Sets this vector to a + b.
     */
    addVectors(a: Vector3, b: Vector3): this;

    /**
     * axis - A normalized Vector3.
     * angle - An angle in degrees.
     * Applies a rotation specified by an axis and an angle to this vector.
     */
    applyAxisAngle(axis: Vector3, angleDegrees: number): this;

    /**
     *  Applies a Quaternion transform to this vector.
     */
    applyQuaternion(quaternion: Quaternion): this;

    /**
     * Returns the angle between this vector and vector v in degrees.
     */
    angleTo(v: Vector3): number;

    /**
     *  Returns a new vector3 with the same x, y and z values as this one.
     */
    clone(): Vector3;

    /***
     *  Copies the values of the passed vector3's x, y and z properties to this vector3.
     */
    copy(v: Vector3): this;

    /**
     *  Sets this vector to cross product of itself and v.
     */
    cross(v: Vector3): this;

    /**
     * Sets this vector to cross product of a and b.
     */
    crossVectors(a: Vector3, b: Vector3): this;

    /**
     * Computes the distance from this vector to v.
     */
    distanceTo(v: Vector3): number;

    /**
     * Computes the squared distance from this vector to v. If you are just comparing the distance with another distance, you should compare the distance squared instead as it is slightly more efficient to calculate.
     */
    distanceToSquared(v: Vector3): number;

    /**
     * Calculate the dot product of this vector and v.
     */
    dot(v: Vector3): number;

    /**
     * Checks for strict equality of this vector and v.
     */
    equals(v: Vector3): Boolean;

    /**
     * Computes the Euclidean length (straight-line length) from (0, 0, 0) to (x, y, z).
     */
    length(): number;

    /**
     * Computes the square of the Euclidean length (straight-line length) from (0, 0, 0) to (x, y, z). If you are comparing the lengths of vectors, you should compare the length squared instead as it is slightly more efficient to calculate.
     */
    lengthSq(): number;

    /**
     * Linearly interpolate between this vector and v, where alpha is the percent distance along the line - alpha = 0 will be this vector, and alpha = 1 will be v.
     * @param v   Vector3 to interpolate towards.
     * @param alpha  interpolation factor, typically in the closed interval [0, 1].
     */
    lerp(v: Vector3, alpha: number): this;

    /**
     * Sets this vector to be the vector linearly interpolated between v1 and v2 where alpha is the percent distance along the line connecting the two vectors - alpha = 0 will be v1, and alpha = 1 will be v2.
     *
     * @param v1 the starting Vector3.
     * @param v2 Vector3 to interpolate towards.
     * @param alpha interpolation factor, typically in the closed interval [0, 1].
     */
    lerpVectors(v1: Vector3, v2: Vector3, alpha: number): this;

    /**
     * If this vector's x, y or z value is less than v's x, y or z value, replace that value with the corresponding max value.
     */
    max(v: Vector3): this;

    /**
     * If this vector's x, y or z value is greater than v's x, y or z value, replace that value with the corresponding min value.
     */
    min(v: Vector3): this;

    /**
     * Multiplies this vector by v.
     */
    multiply(v: Vector3): this;

    /**
     * Multiplies this vector by scalar s.
     */
    multiplyScalar(s: number): this;

    /**
     * Sets this vector equal to a * b, component-wise.
     */
    multiplyVectors(a: Vector3, b: Vector3): this;

    /**
     * Inverts this vector - i.e. sets x = -x, y = -y and z = -z.
     */
    negate(): this;

    /**
     * Convert this vector to a unit vector - that is, sets it equal to a vector with the same direction as this one, but length 1.
     */
    normalize(): this;

    /**
     * Projects this vector onto a plane by subtracting this vector projected onto the plane's normal from this vector.
     * @param planeNormal A vector representing a plane normal.
     */
    projectOnPlane(planeNormal: Vector3): this;

    /**
     * Projects this vector onto v.
     */
    projectOnVector(v: Vector3): this;

    /**
     * Reflect this vector off of plane orthogonal to normal. Normal is assumed to have unit length.
     * @param normal the normal to the reflecting plane
     */
    reflect(normal: Vector3): this;

    /**
     * Sets the x, y and z components of this vector.
     */
    set(x: number, y: number, z: number): this;

    /**
     * Set this vector to a vector with the same direction as this one, but length l.
     */
    setLength(l: number): this;

    /**
     * Subtracts v from this vector.
     */
    sub(v: Vector3): this;

    /**
     * Sets this vector to a - b.
     */
    subVectors(a: Vector3, b: Vector3): this;
  }

  export class Quaternion {
    /**
     *
     * @param x the x value of this vector. Default is 0.
     * @param y the y value of this vector. Default is 0.
     * @param z the z value of this vector. Default is 0.
     * @param z the w value of this vector. Default is 1.
     */
    constructor(x?: number, y?: number, z?: number, w?: number);
    x: number;
    y: number;
    z: number;
    w: number;

    /**
     * Returns the angle between this quaternion and quaternion q in degrees.
     */
    angleTo(q: Quaternion): number;

    /**
     * Creates a new Quaternion with identical x, y, z and w properties to this one.
     */
    clone(): Quaternion;

    /**
     * Returns the rotational conjugate of this quaternion. The conjugate of a quaternion represents the same rotation in the opposite direction about the rotational axis.
     */
    conjugate(): this;

    /**
     * Copies the x, y, z and w properties of q into this quaternion.
     */
    copy(q: Quaternion): this;

    /**
     * Compares the x, y, z and w properties of v to the equivalent properties of this quaternion to determine if they represent the same rotation
     * @param v Quaternion that this quaternion will be compared to.
     */
    equals(v: Quaternion): Boolean;

    /**
     * Calculates the dot product of quaternions v and this one.
     */
    dot(v: Quaternion): number;

    /**
     * Sets this quaternion to the identity quaternion; that is, to the quaternion that represents "no rotation".
     */
    identity(): this;

    /**
     * Inverts this quaternion - calculates the conjugate. The quaternion is assumed to have unit length.
     */
    invert(): this;

    /**
     * Computes the Euclidean length (straight-line length) of this quaternion, considered as a 4 dimensional vector.
     */
    length(): number;

    /**
     * Computes the squared Euclidean length (straight-line length) of this quaternion, considered as a 4 dimensional vector. This can be useful if you are comparing the lengths of two quaternions, as this is a slightly more efficient calculation than length().
     */
    lengthSq(): number;

    /**
     * Normalizes this quaternion - that is, calculated the quaternion that performs the same rotation as this one, but has length equal to 1.
     */
    normalize(): this;

    /**
     * Multiplies this quaternion by q.
     */
    multiply(q: Quaternion): this;

    /**
     * Sets this quaternion to a x b.
     * Adapted from the method outlined here.
     */
    multiplyQuaternions(a: Quaternion, b: Quaternion): this;

    /**
     * Pre-multiplies this quaternion by q.
     */
    premultiply(q: Quaternion): this;

    /**
     * Rotates this quaternion by a given angular step to the defined quaternion q. The method ensures that the final quaternion will not overshoot q.
     * @param q The target quaternion.
     * @param step The angular step in degrees.
     */
    rotateTowards(q: Quaternion, stepDegrees: number): this;

    /**
     * Handles the spherical linear interpolation between quaternions. t represents the amount of rotation between this quaternion (where t is 0) and qb (where t is 1). This quaternion is set to the result. Also see the static version of the slerp below.
     * @param qb The other quaternion rotation
     * @param t interpolation factor in the closed interval [0, 1].
     */
    slerp(qb: Quaternion, t: number): this;

    /**
     * Performs a spherical linear interpolation between the given quaternions and stores the result in this quaternion.
     * @param t interpolation factor in the closed interval [0, 1].
     */
    slerpQuaternions(qa: Quaternion, qb: Quaternion, t: number): this;

    /**
     * Sets x, y, z, w properties of this quaternion.
     */
    set(x: number, y: number, z: number, w: number): this;

    /**
     * Sets this quaternion from rotation specified by axis and angle.
     * Axis is assumed to be normalized, angle is in degrees.
     */
     setFromAxisAngle(axis: Vector3, angleDegrees: number): this;

    /**
     * Sets this quaternion to the rotation required to rotate direction vector vFrom to direction vector vTo.
     * Adapted from the method here. http://www.euclideanspace.com/maths/geometry/rotations/conversions/angleToQuaternion/index.htm
     * vFrom and vTo are assumed to be normalized.
     */
    setRotationBetweenVectors(vFrom: Vector3, vTo: Vector3): this;

    /**
     * Sets this quaternion from the rotation specified by Euler angles. Applying the rotations in XYZ order.
     * @param eulerAngles - the angles in degrees.
     */
    setEulerAngles(eulerAngles: Vector3): this;
  }

  export class Color {
    r: number;
    g: number;
    b: number;
    a: number;
    /**
     *
     * @param red float [0;1]
     * @param green float [0;1]
     * @param blue float [0;1]
     * @param alpha float [0;1], default: 1
     */
    constructor(red: number, green: number, blue: number, alpha?: number);

    /**
     * Creates a new color from a hex value.
     * @param hex Hex color can either be a string in format "#RRGGBB", "#RRGGBBAA", "RRGGBB", "RRGGBBAA".
     * Lower case is also allowed.
     * Make sure you know the input color space and convert to linear RGB if necessary using {@link Color.convertSRGBToLinear}.
     */
    static fromHex(hex: string): Color;

    /**
     * Sets this colors r,g,b,a values from a hex string.
     * @param hex Hex color can either be a string in format "#RRGGBB", "#RRGGBBAA", "RRGGBB", "RRGGBBAA".
     * Lower case is also allowed.
     * Make sure you know the input color space and convert to linear RGB if necessary using {@link Color.convertSRGBToLinear}.
     */
    setHex(hex: string): this;

    /**
     * Creates a new Color with identical r, g, b and a properties to this one.
     */
    clone(): Color;

    /**
     * Copies the r, g, b and a properties of c into this Color.
     */
    copy(c: Color): this;

    /**
     * Converts this color from linear space to sRGB space.
     */
    convertLinearToSRGB(): this;

    /**
     * Converts this color from sRGB space to linear space.
     */
    convertSRGBToLinear(): this;

    /**
     * Sets r, g, b, a properties of this color.
     */
    set(r: number, g: number, b: number, a: number): this;
  }

  export abstract class System {
    /**
     * Called once per frame.
     */
    abstract update(dt: number): void;
  }

  /**
   * An AudioSource component only can play audio, when it is attached to an entity.
   */
  export class AudioSource extends Component {
    /**
     * float [0;1]
     */
    volume: number;
    loop: boolean;
    /**
     * Sets the audio file "clip" that should be played.
     * After a new clip is set, play() must be called again.
     */
    setClip(clip: String): void;
    play(): void;
    pause(): void;
    resume(): void;
    stop(): void;
    isPlaying(): boolean;

    /**
     * float, in seconds
     */
    getDuration(): number;

    /**
     * float, in seconds
     */
    getCurrentTime(): number;

    /**
     * float, in percent [0;1]
     */
    getCurrentProgress(): number;
  }

  export class OrbitCameraController extends Component {
    readonly pivotPosition: Vector3;
    //same as cameraEntity.getComponent(Transform).worldPosition
    readonly cameraPosition: Vector3;

    /**
     * Convenience function for tweening the camera to the new location. Using an easeInOut interpolator.
     */
    animateTo(pivotPosition: Vector3, cameraPosition: Vector3, duration: number): void;
  }

  /**
   * Sealed class for now.
   */
  export abstract class Collider extends Component {}
  export class BoxCollider extends Collider {}

  export const experience: ARExperience;

}   //  declare module "arsdk"