
Note techinically some of the files here are not iOS-specific, 
but currently they're only used on iOS. 
Specifically: arscript.ts, most of remote/*. 
(Some small parts of remote/ do depend on iOS stuff, 
those should be split out into separate files.)

---


    Overview

*   In script-land, we have mirrors for all the classes (or protocols)
    exported from native.
    We also call these "replicated classes".
*   Instances of replicated classes can be replicated between script and native.
    In other words, replicated objects exist on both side,
    as a pair of instances of the corresponding native + script classes.
    The script and native side representations of an object are linked
    by a shared objectId (a uuid).
*   Most such objects are created by the native side and replicated to script,
    but the opposite direction is also possible.
*   The implementations of exported functions in the script-side mirror classes
    simply call executeNative(), passing it the objectId, the method name,
    and also forwarding all incoming parameters.
*   executeNative() converts parameters to strings, then calls executeNative_str().
*   In production mode, when running the script on device,
    executeNative_str() is implemented in Swift, and injected into the JSCore context.
*   In remote script development mode, when the script is running in a cloud IDE
    (but the native calls are still executed on an iPhone,
    connected to the development environment via websocket),
    then the remote version of executeNative_str() (implemented in remote_app.ts),
    gets executed, and sends the stringified call description to the native app.
*   In both cases, the call ultimately gets dispatched to the native side,
    where the underlying native implementation gets called.
    The result is sent back to script.
*   All this communication is fully synchronous.
    The calling script sees it as a normal function call.
*   Because the cross-platform scripting API can be in some cases pretty different
    from ARSDK.iOS internal implementation details, the Swift -> JS class mirroring
    is actually split into two layers on the JS side:
    *   One layer is aligned with ARSDK.iOS, strictly follows what's exposed
        from the SDK, just makes it available in script-land.
    *   Another layer implements the cross-platform scripting API,
        built on top of the first layer.
    *   See also the "Implementation layers" section below.


    Implementation layers    

*   The functionality described in Overview is implemented in the following layers:
*   String-based Remote Procedure Call interface. 
    *   Defines a simple, generic, JSON-string-based RPC protocol. 
    *   This layer is directly exposed (injected into the JS context) by ARSDK.iOS. 
        That is, elements of this layer don't exist inside the JS repo, 
        but higher layers in the JS repo build on them,
        and expect them to be somehow present in the JS context. 
    *   (The above is true when executing a script on device. 
        There's also a remote / cloud scripting setup, in which case the string-RPC interface 
        is also defined in JS/TS code, by sending the RPC data to a remote side 
        via a network connection. See details below.)
    *   The most important element of the RPC interface is an executeNative_str() function. 
        *   Takes a string that encodes a JSON object that describes a function call. 
        *   Returns a string that encodes a JSON object that describes the result of 
            executing that function call on the native side. 
    *   It is actually agnostic of ARSDK, 
        could be reused in other projects to expose their APIs to script.
    *   The RPC protocol is also agnostic of the JavaScript language, 
        could also be used with other scripting languages, 
        or even with visual scripting systems. 
*   arscript.ts: Generic (ARSDK-agnostic) JavaScript / TypeScript tools for 
    calling native-side functions. 
    *   executeNative(): calls a native function, built on executeNative_str(). 
        Works with JS values, not strings, but it's still untyped (params are `any` values). 
    *   Type conversions between the RPC string format and JS values. 
    *   Helpers for object replication. 
        For each native instance of a script-exported class, 
        a corresponding script-side object should exist too. 
        The two representations (native and script) of an object 
        are connected by a shared objectId. 
    *   This layer is still ARSDK-agnostic (but it's specific to JavaScript / TypeScript), 
        so could be reused in other projects for communication between a native and scripts. 
*   arsdk_ios_raw.ts: Mirroring the raw ARSDK.iOS API. 
    *   Contains definitions of classes and variables that mirror those exported from 
        the native iOS ARSDK. 
    *   Note this layer is aligned with the iOS implementation, 
        not with the cross-platform Immersive Scripting API. 
    *   Most of it is definitions of class methods that simply forward to the native side
        (using arscript.executeNative()). 
*   arsdk_ios.ts: Implementing the cross-platform Immersive Scripting API. 
    *   This is the layer that implements the cross-platform API, 
        on top of the iOS-ARSDK-specific raw API. 
    *   Should directly correspond to the type definitions in arsdk/index.d.ts; 
        implement all symbols in the type definition. 
        This module is the runtime counterpart of the .d.ts. 
    *   Hides the differences between the cross-platform API definition, 
        and the interface directly exposed by ARSDK.iOS. 


    Bootstrapping

*   When executing an experience script, the entry point is _not_ 
    the main user script (main.ts), but rather a bootstrapper. 
    main.ts is loaded later as a module. 
    *   When bundling for on-device execution, the entry point is ios-bootstrap.ts, 
        main.ts is bundled as a module. 
        When executing the bundle, the code in bootstrap.ts will load and execute main.ts. 
    *   When cloud scripting, launch.json is set up to execute ios-remote-bootstrap-xp.ts, 
        and again, the bootstrapper later loads main.ts as a module. 
*   The bootstrapper does not immediately load and execute the main user script. 
    *   The reasons is that the native side may need to take certain steps 
        _after_ the script-side scaffolding code (eg. ARSDK class definition) has been loaded, 
        but _before_ the main user script would execute. 
    *   The most important example is exporting global objects. 
        *   Exporting a global `x` of type `X` is something like `var x = new X()` on the script side. 
        *   For this to work, `class X` must be defined. 
        *   On iOS, such class definitions are part of the script bundle 
            (as opposed to being directly injected into the JS context by the native SDK, like on Android), 
            which means that such var declarations only work _after_ loading the script bundle.  
        *   But, we can only execute the user script once globals are already available. 
        *   So, we need to first load the script bundle, then the native side can export globals, 
            and only then can we execute the user script. 
    *   The above-described behavior is implemented as follows: 
        *   The entry point is a bootstrapper, not the main user script. 
        *   We install a script-side callback handler for callbackId == "start", 
            and expect the native SDK to send such a "start" callback once 
            its initialization is done (eg. all necessary global vars have been exported). 
        *   The "start" callback handler imports the main user script module. 
            *   Note with ES modules this can only be done using dynamic import(), 
                only available with module format ES2017, not with ES2015. 
                (Normal `import` would preload all modules before executing the importing module.)
            *   Also, bundlers tend to interpret such dynamic imports as a request for code splitting, 
                creating a separate bundle for the main user script. 
                We don't want this (at least not for now, may reconsider later), 
                so we need to take extra steps to prevent bundlers from doing so. 
                (Eg. with `webpack` this is why we use LimitChunkCountPlugin.)
*   See also 'Remote Bootstrapping' below. 


    Bundling / Publishing

*   We use `webpack` to bundle all modules, including ARSDK modules, user script modules, 
    and possibly npm packages referenced by the user script, 
    into a single `bundle.js` (under the `dist` folder). 
*   This `bundle.js` can then be copy-pasted for being published, 
    and referenced from an experience JSON. 
*   Bundling is done using an npm script called "bundle", 
    see the top-level package.json 
    (in the future it may get moved to arsdk/package.json). 


    Module resolution

*   We're using ES6 modules. 
*   When writing code, type checking, code completion etc. is done by the IDE 
    using arsdk/index.d.ts (which is only an interface definition, no implementation). 
*   At runtime, we need any imports from `arsdk` to resolve to an `arsdk` module 
    that actually implements all the symbols defined in arsdk/index.d.ts. 
*   Exactly how that's done is different in on-device vs remote execution mode, 
    but there are some shared steps. 
*   Shared:
    *   arsdk/tsconfig.json is set up to produce output in node_modules/arsdk. 
    *   arsdk/package.json has an `exports` section, 
        mapping module identifiers (as referenced in `import` statements) 
        to implementation .js files. 
        Most importantly, it maps the `arsdk` identifier to `arsdk/ios/arsdk-ios.js`. 
*   When running on device: 
    *   Our webpack config adds `node_modules` to the module resolution search paths
        (buildProcess.resolve.modules). 
    *   The combination of this search path and the module mapping in arsdk/package.json
        results in webpack finding the implementation in `node_modules/arsdk/ios/arsdk-ios.js`. 
*   When running in a cloud IDE (remote-controlling an ARSDK app running on a device): 
    *   We're running on top of node.js. 
    *   But since we're running with ES6 modules, we still need to explicitly tell 
        node to use classic node-style module resolution rules, 
        using the `--experimental-specifier-resolution=node` flag (see .vscode/launch.json). 
        This makes node look for modules in the node_modules folder. 
    *   At runtime, node will find the implementation for the `arsdk` module 
        in `node_modules/arsdk/ios/arsdk-ios.js`
*   It's not impossible that all this could be simplified, 
    by _not_ forcing node-style module resolution 
    (build arsdk to node_modules, then set up runtime resolution to look into node_modules), 
    but instead using default ES6 module resolution paths. 
    I couldn't get this to work, but may revisit later. 


    Script-to-native call

*   Most of this is implemented in arscript.ts. 
*   The script calls a function implemented on the native side, eg. `myObj.foo(42)`.
*   The script-side MyObject mirror class has a `foo()` function with a body that goes something like
    `requirements.executeNative(self.objectId, "foo", 42)`.
    *   `requirements` is something that provides a suitable `executeNative()` function, 
        dependency-injected on script launch.
        Usually an instance of the ARScript TypeScript class. 
    *   All parameters are forwarded to `executeNative()`, it has a variadic parameter list.
*   `ARScript.executeNative()` converts parameters to string,
    builds a JSON RPC string out of the params, objectId, method name,
    and passes it to `executeNative_str()`.
    *   When converting parameters to string, some types and values are handled specially,
        see `ARScript.scriptValueToNativeValue()`.
        Examples include `null`, functions (replaced with a callbackId),
        objects (replaced with their objectId), arrays.
    *   `executeNative_str()` is dependency-injected.
        Exactly where from, that depends on whether we're running the script on device or remotely;
        see device-bootstrap.ts, remote-bootstrap.ts.
        In either case, it sends the JSON RPC call string to the native side.
    *   In production mode (when running the script on-device), 
        `executeNative_str()` is implemented by the native side (JSCoreScriptExecutor)
        and injected into the JS context. 
*   `executeNative_str()` is responsible for executing the call, 
    as encoded into the call string, on the native side, 
    and return the result of that call, again encoded in a stringified JSON object. 
*   `executeNative()` parses the result string using `scritpValueFromNativeCallResultJsonString()`, 
    and returns the result to the mirror method (to `MyObject.foo()` in the above example). 


    Native-to-script callbacks

*   Native calls the global script function __arscript_executeCallback(). 
    *   (This is in production, on-device mode. In cloud dev mode, native 
        sends a RemoteMessage with its scriptCallback field filled in.)
*   __arscript_executeCallback() forwards to ARScript.executeCallback(). 
*   executeCallback(): 
    *   Parses the JSON string that encodes the callback. 
    *   Converts parameters from string representations to JS values
        using nativeValueStringToScriptValue(). 
    *   Looks up the callback function by callbackId
        (callbackId is part of the JSON callback descriptor). 
    *   Calls the callback function. 
    *   Packages the result into a stringified JSON object. 


    Passing / replicating objects

*   Objects are passed as a string objectId. 
    The objectId can be used on the other side of the bridge to look up the corresponding 
    representation of the object on that side. 
*   In some cases we also pass a class name along with the objectId. 
    This is important if it's a new object that doesn't yet exist on the target side, 
    and will need to be replicated there. 
*   One important difference between replicating from native to script vs the other way around 
    is that instances of replicated classes created on the script side are immediately 
    replicated to native, 
    while objects created on the native side only get replicated to script 
    when they're first passed to script. 
*   When passing an object from script to native: 
    *   We only pass an objectId, because the object is guaranteed to 
        already be replicated to the native side. 
    *   This is handled in `ARScript.scriptValueToNativeValue()`. 
*   Passing an object from native to script is a bit more complicated, 
    because it may also involve on-demand native -> script replication of the object. 
    *   We pass not only the objectId, but also the object's class name; 
        the latter will be necessary if this is a new object that needs to be replicated. 
    *   Objects can be passed from native to script in a few different cases: 
        *   As a parameter to a native -> script callback. 
        *   As the result of a script -> native function call (executeNative()). 
        *   A special case of the latter is the result of a script -> native _constructor_ call; 
            as this happens in the middle of the process of replicating an object 
            from script to native, it requires some special handling, see below. 
        *   When exporting global objects. 
    *   In most cases (except for global objects), nativeValueStringToScriptValue() gets called. 
        *   It detects that the value to be converted to a script value is a JSON object, 
            and it has fields for objectId and className. 
        *   If the isConstructorResult parameter of nativeValueStringToScriptValue() is `true`, 
            that means that the value was received as the result of calling a native constructor, 
            which means we're in the middle of replicating the object from script to native
            (the call chain was replicateNewObject() -> executeNative() -> nativeValueToScriptValue()). 
            In this case just return the objId and className to replicateNewObject(). 
        *   If isConstructorResult == false, then check if the object is already replicated to script 
            (can be found in the objectId-to-object map). If so, just return it. 
        *   If isConstructorResult == false and the object cannot be found by id on the script side, 
            then request replicating it by calling replicateNativeToScript(). 
        *   replicateNativeToScript() looks up the script-side mirror class using the class name 
            that came with the object from the native side, and creates an instance of it, 
            setting up its objectId to match that of the native-side object.     
    *   When exporting global objects: 
        *   The native side calls the global __arscript_replicateNativeToScript(), 
            which forwards directly to ARScript.replicateNativeToScript(). 
        *   Also passes a name for the replicated object to replicateNativeToScript(), 
            replicateNativeToScript() uses that name to add a new variable to the global namespace. 
        *   (Exporting globals when remote scripting works similarly, 
            but instead of a direct call to __arscript_replicateNativeToScript(), 
            the global object's id and className ar passed in a RemoteMessage. 
            See the exportObject fields of RemoteMessage in remote-app.ts.)


    Remote Bootstrapping

*   In remote script development mode, the entry point is remote-bootstrap.ts. 
*   It builds a remote scripting environment using arscript.ts, 
    arsdk-raw-ios.ts, arsdk-ios.ts, remote-app.ts; 
    then establishes a network communication channel between the native and script sides, 
    and later launches the user script. 
*   The native side is running in an ARSDK app on a mobile device, 
    while the script side is running remotely from that device, 
    in a separate JavaScript execution environment, 
    eg. in Gitpod, or on a desktop computer with VSCode. 
    Calls between the two sides will travel over a network channel. 
*   remote-bootstrap.ts creates an instance of ARScript, and also an instance of RemoteApp. 
    It sets up the requirements defined by each, to make them work together; 
    eg. it sets up ARScript's executeNative_str() to forward to RemoteApp.executeNative_str(). 
*   On construction, RemoteApp starts a worker using an instance of ScriptConnectionWorker. 
*   ScriptConnectionWorker starts a websocket server, handles incoming connection requests, 
    listens for incoming messages, and can send messages to the remote side. 


    Remote native <-> script communication

*   See also the comments in remote-app.ts, script-connection-worker.ts. 
*   When ScriptConnectionWorker receives a message from the remote (native) side, 
    it passes on that message to the RemoteApp instance.
    *   Stores the message in a shared buffer, then notifies RemoteApp. 
    *   Actually sends two different notifications, because RemoteApp can only react 
        to one of them at any time; which one depends on RemoteApp's state, 
        if it's idle or blocked waiting for the result of a script -> native call. 
    *   When RemoteApp is idle, it can receive the notification using `worker.on()`. 
    *   When RemoteApp is blocked waiting for a call result, it uses `Atomics.wait`. 
*   Script -> native calls
    *   The same as in on-device mode up to the point where executeNative_str() gets called. 
        During bootstrapping, executeNative_str() was set up to call RemoteApp.executeNative_str(). 
    *   RemoteApp.executeNative_str() sends the call string to the remote (native) side
        using the ScriptConnectionWorker instance. 
        Then waits (synchronously) for a response to arrive. 
        (Needs to be synchronous because such script -> native calls are sync when running on-device, 
        and we want the user script to be unchanged when running in remote dev mode. 
        We want to hide all differences between on-device and remote mode from the script author.)
*   Native -> script callbacks
    *   ScriptConnectionWorker receives a callback messages, passes it to RemoteApp, 
        RemoteApp.handleCallbackMessage() calls requirements.executeCallback(). 
    *   During bootstrapping, RemoteApp.requirements.executeCallback() is set up to call 
        ARScript.executeCallback(). 
    *   Starting with ARScript.executeCallback(), the flow is the same as in the on-device case. 
    *   RemoteApp.handleCallbackMessage() then the sends the result back to the remote native side. 


