
/*
    Script-side mirrors for classes, methods, global vars exposed by ARSDK.iOS. 
    Note this is not yet exactly the cross-platform scripting API defined in arsdk.d.ts, 
    instead this is aligned with what's easy to expose from ARSDK.iOS specifically. 
    The cross-platform API will be implemented in a separate layer on top of this one, 
    see arsdk-ios.ts. 
    (Later on, this whole file should be auto-generated from the iOS native-side 
    type information.)

    Implemented on top of a narrow interface, defined by Requirements. 
    You pass in a suitable implementation of Requirements, 
    you get back working implementations of ARSDK.iOS classes and functions. 
    For Requirements, you most likely will want to use an instance of ARScript
    (and ARScript will need an implementation of executeNative_str). 
    Usage example: 
        import * as arsdk from "../narrow-intf/arsdk-impl"
        import { ARScript } from "../arscript"
        let arscript = new ARScript()
        arsdk.init(arscript)
    See ios-bootstrap.ts for a usage example. 

    Note in some cases there might be typing differences between what ARSDK.iOS exposes
    and the implementations here. One example is constructors: 
    the implementation here also allows special 
    constructor calls, eg. when replicating an object created on the native side. 

    TODO is there even a point in this file being TS not JS? 
        pretty much all functions here just forward to something that 
        takes all parameters as `any`. 
        at the call site, type checking is done based on arsdk.d.ts, not this file. 
        this is loaded dynamically by the bootstrapper, 
        and then the symbols are type-blindly paired up with the symbols in arsdk.d.ts. 
        any type mismatch would go unnoticed anyway. 
        maybe keep it TS just for documentation purposes 
        but don't fret over types in mirror classes. 
*/

import { 
    NewObjectReplicationInfo, 
    arDoNotReplicateConstructorTag, 
    classesByName, 
    ryot_nullify } 
    from "./arsdk-ios-tools.js"

export interface Requirements { 
    executeNative(objectId: string, methodName: string, ...params: any[]): any
    replicateNewObject(newObject: any, className: string, ...params: any[]): void
}
var requirements_: Requirements

export function init(requirements: Requirements) { 
    requirements_ = requirements
}

export function replicateNewObject(newObject: any, className: string, ...params: any[]): void {
    if (requirements_ === undefined)
        throw "requirements === undefined"
    requirements_.replicateNewObject(newObject, className, ...params)
}

//  --- ARSDK ---

class ARScript_Printer {
    private objectId: string
	static print(p0: string) { 
        if (requirements_ === undefined)
            throw "reqs === undef"
        return requirements_.executeNative("arsdk_class_ARScript_Printer", "print", p0) 
    }
    constructor(replicationInfo?: NewObjectReplicationInfo) {
        this.objectId = "arsdk_uninitialized_objectId"
        requirements_.replicateNewObject(this, "ARScript_Printer", replicationInfo)
    }
}
classesByName["ARScript_Printer"] = ARScript_Printer

export function print(str: any) {
    console.log(str.toString())
    ARScript_Printer.print(str.toString())
}

export interface ReplicatedClass { 
    nativeClassName: string    //  used eg. in getComponent()
}

const nonInheritableNativeClassNameKey = "nonInheritableNativeClassName"

export function nativeClassNameForReplicatedClass(replicatedClass: ReplicatedClass): string | undefined { 
    /*
        i don't like this, but at the moment i don't have a better idea. 
        the challenge is that: 
        *   we want all user-created subclasses of a replicated class X 
            to automatically have a nativeClassName, 
            referencing a native-side concrete class 
            (eg. for all script-only Component subclasses, 
            we want nativeClassName == "ECSBasicComponent").  
            the way to implement this would be for the subclasses to inherit 
            nativeClassName from their base class. 
        *   however, for the replicated base class X itself, we may need a _different_ 
            nativeClassName than for all the subclasses. 
            eg. in the case of Component, we need nativeClassName == "ECSComponent", 
            not "ECSBasicComponent". (native getComponentByClassName("ECSBasicComponent")
            would not return all components, only instances of ECSBasicComponent.)
        *   so, we want one nativeClassName value for the base class X itself, 
            and another, different one that all its subclasses would inherit
            (basically a default value for subclasses). 
        our solution is to define two static fields: 
        nativeClassName and nonInheritableNativeClassName. 
        technically nonInheritableNativeClassName also gets inherited of course, 
        but here we check if its inherited or not. 
        a non-inherited nonInheritableNativeClassName overrides nativeClassName, 
        but an inherited nonInheritableNativeClassName does not. 
    */
    if (replicatedClass.hasOwnProperty(nonInheritableNativeClassNameKey))
        return (replicatedClass as any)[nonInheritableNativeClassNameKey]
    return replicatedClass.nativeClassName
}


//  TODO remove the "Raw" prefix
export interface RawComponentClass 
    extends ReplicatedClass {
    isComponentClass: boolean
}
function isRawComponentClass(a: any): a is RawComponentClass { 
    return "isComponentClass" in a
}

export interface ConstructibleRawComponentClass<T extends ECSComponent> extends RawComponentClass { 
    new (...args: any[]): T    
}


export class ECSEntity { 
    static nativeClassName = "ECSEntity"
    protected objectId: string
    constructor(replicationInfo?: NewObjectReplicationInfo) {
        this.objectId = "arsdk_uninitialized_objectId"
        requirements_.replicateNewObject(this, ECSEntity.nativeClassName, replicationInfo)
    }
    get raw_name(): string | null { return requirements_.executeNative(this.objectId, "get__name") }
    set raw_name(newValue: string | null) { requirements_.executeNative(this.objectId, "set__name", newValue) }
    get isDestroyed(): boolean { return requirements_.executeNative(this.objectId, "get__isDestroyed") }
    destroy(): void { requirements_.executeNative(this.objectId, "destroy") }

    raw_addComponent(component: ECSComponent): void { requirements_.executeNative(this.objectId, "addComponent", component) }
    raw_removeComponent(component: ECSComponent): void { requirements_.executeNative(this.objectId, "removeComponent", component) }

    raw_getComponents<T extends ECSComponent>(
        componentClass: ConstructibleRawComponentClass<T>, 
        key?: string)
        : T[] { 
        
        /*
            `componentClass` may be a class that doesn't even exist on the native side. 
            on the native side, script-only subclasses are represented as instances of 
            a replicated superclass (that exists on both sides), identified by 
            scriptSubclass.nativeClassName. 
            we ask the native side for a list of all components 
            with types derived from nativeClassName, 
            then filter the list down on the script side 
            to instances of the requested script-only subclass. 
        */
        let nativeClassName = nativeClassNameForReplicatedClass(componentClass)
        if (nativeClassName === undefined)
            throw `nativeClassNameForReplicatedClass(${componentClass}) === undefined`
        const components: ECSComponent[] = this.getComponentsByClassName(nativeClassName, key)
        const filteredComponents = components.filter(component => component instanceof componentClass)
        const castComponents = filteredComponents.map(component => (component as T))
        return castComponents
    }

    raw_getComponent<T extends ECSComponent>(
        componentClass: ConstructibleRawComponentClass<T>, 
        key?: string)
        : T | null { 

        const components = this.raw_getComponents(componentClass, key)
        if (components.length < 1) 
            return null
        return components[0]
    }

    getComponentsByClassName(componentTypeName: string, key?: string): ECSComponent[] { 
        return requirements_.executeNative(this.objectId, "getComponentsByClassScriptName", componentTypeName, ryot_nullify(key)) 
    }
}
classesByName["ECSEntity"] = ECSEntity

export class ECSObjectInstance extends ECSEntity { 
    static nativeClassName = "ECSObjectInstance"
    constructor()
    constructor(replicationInfo: NewObjectReplicationInfo)
    constructor(replicationInfo?: NewObjectReplicationInfo)
    constructor(replicationInfo?: NewObjectReplicationInfo) {
        super(arDoNotReplicateConstructorTag)
        requirements_.replicateNewObject(this, ECSObjectInstance.nativeClassName, replicationInfo)
    }
}
classesByName["ECSObjectInstance"] = ECSObjectInstance

export class ECSExperience extends ECSEntity { 
    static nativeClassName = "ECSExperience"
    constructor(replicationInfo?: NewObjectReplicationInfo) {
        super(arDoNotReplicateConstructorTag)
        requirements_.replicateNewObject(this, ECSExperience.nativeClassName, replicationInfo)
    }
    get experienceDefinitionUid(): string { return requirements_.executeNative(this.objectId, "get__experienceDefinitionUid") }
    addObjectScript(objectDefinitionUid: string, onInstanceCreated: (newEntity: ECSEntity) => void) { 
        return requirements_.executeNative(this.objectId, "addObjectScript", objectDefinitionUid, onInstanceCreated)
    }
}
classesByName["ECSExperience"] = ECSExperience

export class ECSComponent { 
    static isComponentClass = true

    //  see also arsdk-ios nativeClassNameForReplicatedClass()
    static nativeClassName = "ECSBasicComponent"

    objectId: string
    constructor(replicationInfo?: NewObjectReplicationInfo) { 
        this.objectId = "arsdk_uninitialized_objectId"
        requirements_.replicateNewObject(this, ECSComponent.nativeClassName, replicationInfo)
    }    
    get entity(): ECSEntity | null { 
        if (this.objectId === "arsdk_uninitialized_objectId")
            return null
        return requirements_.executeNative(this.objectId, "get__ownerEntity") 
    }
}
classesByName["ECSComponent"] = ECSComponent

export class ECSScriptTransform extends ECSComponent { 
    static nativeClassName = "ECSScriptTransform"
    constructor(replicationInfo?: NewObjectReplicationInfo) { 
        super(arDoNotReplicateConstructorTag)
        requirements_.replicateNewObject(this, ECSScriptTransform.nativeClassName, replicationInfo) 
    }    

    // getParent(): ECSScriptTransform | null { return requirements_.executeNative(this.objectId, "getParent") }
    // setParent(newParent: ECSScriptTransform | null) { requirements_.executeNative(this.objectId, "setParent", newParent) }
    getChildCount(): number { return requirements_.executeNative(this.objectId, "getChildCount") }
    getChild(index: number): ECSScriptTransform | null { return requirements_.executeNative(this.objectId, "getChild", index) }
    get raw_parent(): ECSScriptTransform | null { return requirements_.executeNative(this.objectId, "get__parent") }
    set raw_parent(newParent: ECSScriptTransform | null) { requirements_.executeNative(this.objectId, "set__parent", newParent) }
    get raw_children(): ECSScriptTransform[] { return requirements_.executeNative(this.objectId, "get__children") }

    get raw_worldPosition(): number[] { return requirements_.executeNative(this.objectId, "get__worldPosition") }
    set raw_worldPosition(newValue: number[]) { requirements_.executeNative(this.objectId, "set__worldPosition", newValue) }
    get raw_worldOrientation(): number[] { return requirements_.executeNative(this.objectId, "get__worldOrientation") }
    set raw_worldOrientation(newValue: number[]) { requirements_.executeNative(this.objectId, "set__worldOrientation", newValue) }
    get raw_worldEulerAngles(): number[] { return requirements_.executeNative(this.objectId, "get__worldEulerAngles") }
    set raw_worldEulerAngles(newValue: number[]) { requirements_.executeNative(this.objectId, "set__worldEulerAngles", newValue) }
    get raw_worldScale(): number[] { return requirements_.executeNative(this.objectId, "get__worldScale") }
    set raw_worldScale(newValue: number[]) { requirements_.executeNative(this.objectId, "set__worldScale", newValue) }

    get raw_localPosition(): number[] { return requirements_.executeNative(this.objectId, "get__localPosition") }
    set raw_localPosition(newValue: number[]) { requirements_.executeNative(this.objectId, "set__localPosition", newValue) }
    get raw_localOrientation(): number[] { return requirements_.executeNative(this.objectId, "get__localOrientation") }
    set raw_localOrientation(newValue: number[]) { requirements_.executeNative(this.objectId, "set__localOrientation", newValue) }
    get raw_localEulerAngles(): number[] { return requirements_.executeNative(this.objectId, "get__localEulerAngles") }
    set raw_localEulerAngles(newValue: number[]) { requirements_.executeNative(this.objectId, "set__localEulerAngles", newValue) }
    get raw_localScale(): number[] { return requirements_.executeNative(this.objectId, "get__localScale") }
    set raw_localScale(newValue: number[]) { requirements_.executeNative(this.objectId, "set__localScale", newValue) }
}
classesByName["ECSScriptTransform"] = ECSScriptTransform

export namespace ECSOnFrame { 
    export type OnFrameAction = (dt: number) => void
}
export class ECSOnFrame extends ECSComponent { 
    static nativeClassName = "ECSOnFrame"
    constructor(replicationInfo: NewObjectReplicationInfo)
    constructor(onFrameAction: ECSOnFrame.OnFrameAction)
    constructor()
    constructor(onFrameActionOrReplicationInfo?: ECSOnFrame.OnFrameAction | NewObjectReplicationInfo) { 
        super(arDoNotReplicateConstructorTag)
        requirements_.replicateNewObject(this, ECSOnFrame.nativeClassName, onFrameActionOrReplicationInfo ?? null) 
    }    

    set onFrameAction(action: ECSOnFrame.OnFrameAction) { requirements_.executeNative(this.objectId, "set__onFrameAction", action) }
}
classesByName["ECSOnFrame"] = ECSOnFrame

export namespace ECSOnTap { 
    export type OnTapAction = (worldPosition: number[]) => void
}
export class ECSOnTap extends ECSComponent { 
    static nativeClassName = "ECSOnTap"
    constructor(replicationInfo: NewObjectReplicationInfo)
    constructor(onTapAction: ECSOnTap.OnTapAction)
    constructor()
    constructor(onTapActionOrReplicationInfo?: ECSOnTap.OnTapAction | NewObjectReplicationInfo) { 
        super(arDoNotReplicateConstructorTag)
        requirements_.replicateNewObject(this, ECSOnTap.nativeClassName, onTapActionOrReplicationInfo) 
    }    
    setOnTapAction(action: (worldPos: number[]) => void): void { 
        return requirements_.executeNative(this.objectId, "setOnTapAction", action) 
    }
}
classesByName["ECSOnTap"] = ECSOnTap

export class ECSMesh extends ECSComponent { 
    static nativeClassName = "ECSMesh"
    constructor(replicationInfo?: NewObjectReplicationInfo) { 
        super(arDoNotReplicateConstructorTag)
        requirements_.replicateNewObject(this, ECSMesh.nativeClassName, replicationInfo) 
    }    

    get castShadows(): boolean { return requirements_.executeNative(this.objectId, "get__castShadows") }
    set castShadows(value: boolean) { requirements_.executeNative(this.objectId, "set__castShadows", value) }
    get receiveShadows(): boolean { return requirements_.executeNative(this.objectId, "get__receiveShadows") }
    set receiveShadows(value: boolean) { requirements_.executeNative(this.objectId, "set__receiveShadows", value) }

    getPrimitiveCount(): number { return requirements_.executeNative(this.objectId, "getPrimitiveCount") }
    get material(): ECSMaterial { return requirements_.executeNative(this.objectId, "get__material") }
    set material(newValue: ECSMaterial) { requirements_.executeNative(this.objectId, "set__material", newValue) }
    get materials(): ECSMaterial[] { return requirements_.executeNative(this.objectId, "get__materials") }
    setMaterialAt(primitive: number, material: ECSMaterial) { requirements_.executeNative(this.objectId, "setMaterialAtIndex", primitive, material)}
}
classesByName["ECSMesh"] = ECSMesh

export class ECSMaterial  { 
    static nativeClassName = "ECSMaterial"
    protected objectId: string
    constructor(replicationInfo?: NewObjectReplicationInfo) { 
        this.objectId = "arsdk_uninitialized_objectId"
        requirements_.replicateNewObject(this, ECSMaterial.nativeClassName, replicationInfo) 
    }

    get raw_color(): number[] { return requirements_.executeNative(this.objectId, "get__color") }
    set raw_color(value: number[]) { requirements_.executeNative(this.objectId, "set__color", value) }
    get opacity(): number | null { return requirements_.executeNative(this.objectId, "get__opacity") }
    set opacity(value: number | null) { requirements_.executeNative(this.objectId, "set__opacity", value) }
    get metalness(): number | null { return requirements_.executeNative(this.objectId, "get__metalness") }
    set metalness(value: number | null) { requirements_.executeNative(this.objectId, "set__metalness", value) }
    get roughness(): number | null { return requirements_.executeNative(this.objectId, "get__roughness") }
    set roughness(value: number | null) { requirements_.executeNative(this.objectId, "set__roughness", value) }
    get emissive(): number | null { return requirements_.executeNative(this.objectId, "get__emissive") }
    set emissive(value: number | null) { requirements_.executeNative(this.objectId, "set__emissive", value) }
}
classesByName[ECSMaterial.nativeClassName] = ECSMaterial

export class ECSEntityRegistry extends ECSComponent { 
    static nativeClassName = "ECSEntityRegistry"
    constructor(replicationInfo?: NewObjectReplicationInfo) { 
        super(arDoNotReplicateConstructorTag)
        requirements_.replicateNewObject(this, ECSEntityRegistry.nativeClassName, replicationInfo) 
    }    
    get allEntities(): ECSEntity[] { return requirements_.executeNative(this.objectId, "get__allEntities") }
    getComponentsByClassName(componentClassName: string): ECSComponent[] { return requirements_.executeNative(this.objectId, "getComponentsByClassName", componentClassName) }
}
classesByName["ECSEntityRegistry"] = ECSEntityRegistry

//  TODO these is SceneKit-specific, should be removed
export class ECSSceneKitAssociatedNode extends ECSComponent { 
    static nativeClassName = "ECSSceneKitAssociatedNode"
    constructor(replicationInfo?: NewObjectReplicationInfo) { 
        super(arDoNotReplicateConstructorTag)
        requirements_.replicateNewObject(this, ECSSceneKitAssociatedNode.nativeClassName, replicationInfo) 
    }    
    get nodeName(): string { return requirements_.executeNative(this.objectId, "get__nodeName") }
    set nodeName(newValue: string) { requirements_.executeNative(this.objectId, "set__nodeName", newValue) }
}
classesByName["ECSSceneKitAssociatedNode"] = ECSSceneKitAssociatedNode

export class ECSPresentationMode extends ECSComponent { 
    static nativeClassName = "ECSPresentationMode"
    constructor(replicationInfo: NewObjectReplicationInfo) {
        super(arDoNotReplicateConstructorTag)
        requirements_.replicateNewObject(this, ECSPresentationMode.nativeClassName, replicationInfo) 
    } 
    get cameraEntity(): ECSEntity { return requirements_.executeNative(this.objectId, "get__cameraEntity") }
}
classesByName[ECSPresentationMode.nativeClassName] = ECSPresentationMode

export class ECSARMode extends ECSPresentationMode { 
    static nativeClassName = "ECSARMode"
    constructor(replicationInfo: NewObjectReplicationInfo) { 
        super(arDoNotReplicateConstructorTag)
        requirements_.replicateNewObject(this, ECSPresentationMode.nativeClassName, replicationInfo) 
    }    
}
classesByName[ECSARMode.nativeClassName] = ECSARMode

export class ECSPreviewMode extends ECSPresentationMode { 
    static nativeClassName = "ECSPreviewMode"
    constructor(replicationInfo: NewObjectReplicationInfo) { 
        super(arDoNotReplicateConstructorTag)
        requirements_.replicateNewObject(this, ECSPreviewMode.nativeClassName, replicationInfo) 
    }    
}
classesByName[ECSPreviewMode.nativeClassName] = ECSPreviewMode

export class ECSOnMediatorMessage extends ECSComponent { 
    static nativeClassName = "ECSOnMediatorMessage"
    constructor(replicationInfo?: NewObjectReplicationInfo) { 
        super(arDoNotReplicateConstructorTag)
        requirements_.replicateNewObject(this, ECSOnMediatorMessage.nativeClassName, replicationInfo) 
    }    
    get onObjectPreviewModeStateChangedAction(): ((newIsActive: boolean) => void) { 
        return requirements_.executeNative(this.objectId, "get__onObjectPreviewModeStateChangedAction")
    }
    set onObjectPreviewModeStateChangedAction(newAction: ((newIsActive: boolean) => void)) { 
        requirements_.executeNative(this.objectId, "set__onObjectPreviewModeStateChangedAction", newAction)
    }
}
classesByName[ECSOnMediatorMessage.nativeClassName] = ECSOnMediatorMessage

export class ECSEntityGetter extends ECSComponent { 
    static nativeClassName = "ECSGetterComponent<ECSEntity>"
    constructor(replicationInfo?: NewObjectReplicationInfo) { 
        super(arDoNotReplicateConstructorTag)
        requirements_.replicateNewObject(this, ECSEntityGetter.nativeClassName, replicationInfo) 
    }    
    get value(): ECSEntity { return requirements_.executeNative(this.objectId, "get__value") }
}
classesByName[ECSEntityGetter.nativeClassName] = ECSEntityGetter

export class ECSOrbitCameraController extends ECSComponent { 
    static nativeClassName = "ECSOrbitCameraController"
    constructor(replicationInfo?: NewObjectReplicationInfo) { 
        super(arDoNotReplicateConstructorTag)
        requirements_.replicateNewObject(this, ECSEntityGetter.nativeClassName, replicationInfo) 
    } 
    get raw_pivotPosition(): number[] { return requirements_.executeNative(this.objectId, "get__pivotPosition") }
    set raw_pivotPosition(newValue: number[]) { requirements_.executeNative(this.objectId, "set__pivotPosition", newValue) }
    get raw_cameraPosition(): number[] { return requirements_.executeNative(this.objectId, "get__cameraPosition") }
    set raw_cameraPosition(newValue: number[]) { requirements_.executeNative(this.objectId, "set__cameraPosition", newValue) }
    raw_animateTo(pivotPosition: number[], cameraPosition: number[], duration: number) { requirements_.executeNative(this.objectId, "animateTo", pivotPosition, cameraPosition, duration) }
}
classesByName[ECSOrbitCameraController.nativeClassName] = ECSOrbitCameraController

export class ECSObjectAnimator extends ECSComponent { 
    static nativeClassName = "ECSObjectAnimator"
    constructor(replicationInfo?: NewObjectReplicationInfo) { 
        super(arDoNotReplicateConstructorTag)
        requirements_.replicateNewObject(this, ECSObjectAnimator.nativeClassName, replicationInfo) 
    } 
    getOrCreateAnimationInstanceNamed(animationName: string): ECSObjectAnimationInstance | null { 
        return requirements_.executeNative(this.objectId, "getOrCreateAnimationInstanceNamed", animationName) 
    }
    getOrCreateAnimationInstancesForAllTemplates(): [ECSObjectAnimationInstance] { 
        return requirements_.executeNative(this.objectId, "getOrCreateAnimationInstancesForAllTemplates")
    }
}
classesByName[ECSObjectAnimator.nativeClassName] = ECSObjectAnimator

export class ECSObjectAnimationInstance { 
    static nativeClassName = "ECSObjectAnimationInstance"
    protected objectId: string
    constructor(replicationInfo: NewObjectReplicationInfo) { 
        this.objectId = "arsdk_uninitialized_objectId"
        requirements_.replicateNewObject(this, ECSObjectAnimationInstance.nativeClassName, replicationInfo)
    }
    get name(): string { return requirements_.executeNative(this.objectId, "get__name") }
    get loop(): boolean { return requirements_.executeNative(this.objectId, "get__loop") }
    set loop(newValue: boolean) { requirements_.executeNative(this.objectId, "set__loop", newValue) }
    get layer(): number { return requirements_.executeNative(this.objectId, "get__layer") }
    set layer(newValue: number) { requirements_.executeNative(this.objectId, "set__layer", newValue) }
    get raw_isRunning(): boolean { return requirements_.executeNative(this.objectId, "get__isRunning") }
    get duration(): number { return requirements_.executeNative(this.objectId, "get__duration") }
    get progress(): number { return requirements_.executeNative(this.objectId, "get__progress") }

    //  TODO instead of start / pause / resume, just use play / pause? 
    raw_start() { requirements_.executeNative(this.objectId, "start") }
    raw_pause() { requirements_.executeNative(this.objectId, "pause") }
    raw_resume() { requirements_.executeNative(this.objectId, "resume") }
}
classesByName[ECSObjectAnimationInstance.nativeClassName] = ECSObjectAnimationInstance

export class ECSAudioSource extends ECSComponent { 
    static nativeClassName = "ECSAudioSource"
    constructor(replicationInfo: NewObjectReplicationInfo)
    constructor(experience: ECSExperience)
    constructor()
    constructor(experienceOrReplicationInfo?: ECSExperience | NewObjectReplicationInfo) { 
        super(arDoNotReplicateConstructorTag)
        requirements_.replicateNewObject(this, ECSAudioSource.nativeClassName, experienceOrReplicationInfo) 
    }
    get firstClip(): ECSAudioClipInstance | null { return requirements_.executeNative(this.objectId, "get__firstClip") } 
    getOrCreateAudioClipInstanceNamed(audioClipName: string): ECSAudioClipInstance | null { 
        return requirements_.executeNative(this.objectId, "getOrCreateAudioClipInstanceNamed", audioClipName) 
    }
}
classesByName[ECSAudioSource.nativeClassName] = ECSAudioSource

export class ECSAudioClipInstance { 
    static nativeClassName = "ECSAudioClipInstance"
    protected objectId: string
    constructor(replicationInfo: NewObjectReplicationInfo) { 
        this.objectId = "arsdk_uninitialized_objectId"
        requirements_.replicateNewObject(this, ECSAudioClipInstance.nativeClassName, replicationInfo)
    }
    get name(): string { return requirements_.executeNative(this.objectId, "get__name") }
    get volume(): number { return requirements_.executeNative(this.objectId, "get__volume") }
    set volume(newValue: number) { requirements_.executeNative(this.objectId, "set__volume", newValue) }
    get loops(): boolean { return requirements_.executeNative(this.objectId, "get__loops") }
    set loops(newValue: boolean) { requirements_.executeNative(this.objectId, "set__loops", newValue) }
    get layer(): number { return requirements_.executeNative(this.objectId, "get__layer") }
    set layer(newValue: number) { requirements_.executeNative(this.objectId, "set__layer", newValue) }
    get isRunning(): boolean { return requirements_.executeNative(this.objectId, "get__isRunning") }
    get duration(): number { return requirements_.executeNative(this.objectId, "get__duration") }
    get progress(): number { return requirements_.executeNative(this.objectId, "get__progress") }

    //  TODO instead of start / pause / resume, just use play / pause? 
    start() { requirements_.executeNative(this.objectId, "start") }
    stop() { requirements_.executeNative(this.objectId, "stop") }
    pause() { requirements_.executeNative(this.objectId, "pause") }
    resume() { requirements_.executeNative(this.objectId, "resume") }
}
classesByName[ECSAudioClipInstance.nativeClassName] = ECSAudioClipInstance
