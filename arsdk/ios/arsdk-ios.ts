
/*
    This is where we implement the cross-platform API 
    (as defined in arsdk/index.d.ts)
    on top of what arsdk-ios-raw provides 
    (in other words, on top of what the iOS SDK exposes). 

        --- Implementation Notes ---

        Subclass constructors and arsdk-ios-raw superclass constructors

    Someone needs to handle object replication. Ideally only one of either the superclass 
    or the subclass, though theoretically it isn't an issue if they try to both handle it. 
    
    In most cases if we have a superclass in arsdk-ios-raw, 
    and a subclass of it in arsdk-ios, 
    then the subclass still uses the same native-side class 
    (eg. `Entity extends raw.ECSEntity` => Entity still replicates as ECSEntity on the native side). 
    This means that we can leave replication to the base class. 
    To do so, we simply need to pass along the replicationInfo parameter to the super constructor. 

    If a subclass wanted to replicate as a native-side class other than the one used 
    by the superclass, then the subclass would need to call super(arDoNotReplicateConstructorTag), 
    and handle replication itself (by calling replicateNewObject()). 

    Component constructors are a special case, see below. 

        Component constructors

    iOS native-side ECSComponent and its subclasses require an owner Entity 
    as a mandatory constructor parameter (at least now; may change later). 
    On the other hand, on the script side we want to allow the creation of Components 
    with no owner Entity, eg. to enable the `entity.addComponent(new MyComponent())` pattern. 
    We resolve this interface incompatibility as follows: 
    *   The script-side constructor takes an optional NewObjectReplicationInfo parameter. 
    *   If replication info is provided (probably happens when an object was created
        on the native side, now being replicated to script), then just passes it along 
        to the superclass constructor. 
    *   If replication info is not provided, then the creation of the new Component instance
        was initiated by script. 
        We don't yet have an owner Entity, so we can't yet replicate the new Component to native. 
        Instead, we call the super-constructor passing `arDoNotReplicate`. 
        This prevents the super-constructor from trying to call the native-side constructor 
        (which would fail without passing an owner Entity). 
        Instead, we handle replication later, when setEntity() gets called. 
    *   A consequence of the above is that some functions in Component classes may not work 
        until the Component is added to an Entity. 
        This is because before adding the Component to an Entity, the Component doesn't have a 
        native-side counterpart, and some functions (those implemented on the native side)
        can't work without that.     
*/

import { NewObjectReplicationInfo, classesByName, ryot_nullify, arDoNotReplicateConstructorTag } from "./arsdk-ios-tools.js"
import * as raw from "./arsdk-ios-raw.js"
import { Vector3, arsdk_createReplicatedVector } from "./vector3-ios.js"
export { Vector3 }
import { Quaternion, arsdk_createReplicatedQuaternion } from "./quaternion-ios.js"
export { Quaternion }
import { Color, arsdk_createReplicatedColor } from "./color-ios.js"
export { Color }

//  https://git.vzbuilders.com/immersive/scripting-api/blob/master/types/arsdk/index.d.ts



// function replicate(object: any, ...params: any[]) { 
//     let cls = object.constructor
//     if (!isComponentClass(cls)) 
//         throw "!isComponentClass: " + cls
//     let nativeClassName = raw.nativeClassNameForReplicatedClass(cls)
//     if (nativeClassName === undefined)
//         throw `nativeClassNameForReplicatedClass(${cls}) === undefined`
//     raw.replicateNewObject(object, nativeClassName, ...params)
// }

function notNull<T>(value: T | null): value is T { return value !== null }



export type OnModeChangeListener = (newCurrentMode: ARMode | PreviewMode) => void

export class ARExperience extends raw.ECSExperience {
    constructor(replicationInfo?: NewObjectReplicationInfo) {
        super(replicationInfo)
        this.onModeChangeListeners = []

        this.internalCurrentMode = 
            (this.raw_getComponent(raw.ECSPreviewMode) === null)
            ? new ARMode()
            : new PreviewMode()

        this.startObservingObjectPreviewModeStateChanged()
    }

    get uid(): string { return this.experienceDefinitionUid }
  
    addObjectScript(
        objectUid: string,
        onObjectInstantiated: (root: Entity) => void,
        onObjectDestroyed?: () => void) { 

        //  TODO pass onObjectDestroyed to native
        super.addObjectScript(
            objectUid, 
            (ecsEntity: raw.ECSEntity) => {
                if (!(ecsEntity instanceof Entity))
                    throw "!(ecsEntity instanceof Entity)"
                let entity = ecsEntity as Entity
                onObjectInstantiated(entity)
            })
    }
  
    get currentMode(): ARMode | PreviewMode { return this.internalCurrentMode }
    
    addOnModeChangedListener(onModeChanged: (currentMode: ARMode | PreviewMode) => void): void { 
        this.onModeChangeListeners.push(onModeChanged)
    }

    addSystem(system: System): void { 
        // this.raw_addComponent(system)
        this.raw_addComponent(system.arsdk_onFrameComponent)
    }

    removeSystem(system: System): void { 
        // this.raw_removeComponent(system)
        this.raw_removeComponent(system.arsdk_onFrameComponent)
    }
  
    forEachComponent<T extends Component>(
        classDef: ComponentClassDefinition<T>, 
        action: (component: T) => void): void { 

        let entityRegistry = this.raw_getComponent(raw.ECSEntityRegistry)
        if (entityRegistry === null)
            return

        let nativeClassName = raw.nativeClassNameForReplicatedClass(classDef)
        if (nativeClassName === undefined)
            throw `nativeClassNameForReplicatedClass(${classDef}) === undefined`
        const ecsComponents: raw.ECSComponent[] = 
            entityRegistry.getComponentsByClassName(nativeClassName)
        const maybeComponents: (T | null)[] = ecsComponents.map(ecsComponent => { 
            if (!isComponentClass(ecsComponent.constructor))
                return null
            if (!(ecsComponent instanceof classDef))
                return null
            return ecsComponent as T
        })
        const components: T[] = maybeComponents.filter(notNull)
        components.forEach(action)
    }

    startObservingObjectPreviewModeStateChanged() {
        let onMediatorMessageComponent = this.raw_getComponent(raw.ECSOnMediatorMessage)
        if (onMediatorMessageComponent !== null) { 
            onMediatorMessageComponent.onObjectPreviewModeStateChangedAction = 
                (isNewActive) => this.onObjectPreviewModeStateChanged(isNewActive)
        }
    } 

    onObjectPreviewModeStateChanged(isNewActive: boolean) { 
        let inPreviewMode = isNewActive
        if (inPreviewMode && this.currentMode instanceof ARMode)
            this.internalCurrentMode = new PreviewMode()
        if (!inPreviewMode && this.currentMode instanceof PreviewMode)
            this.internalCurrentMode = new ARMode()
        
        for (let onModeChangeListener of this.onModeChangeListeners)
            onModeChangeListener(this.currentMode)
    }

    //  module-internal

    internalCurrentMode: ARMode | PreviewMode
    onModeChangeListeners: OnModeChangeListener[]

    get cameraEntity(): Entity { 
        // const cameraECSEntity: raw.ECSEntity | null | undefined = 
        //     this.raw_getComponent(raw.ECSEntityGetter, "cameraEntity")?.value
        // if (cameraECSEntity === null)
        //     throw "cameraECSEntity === null"
        // if (cameraECSEntity === undefined)
        //     throw "cameraECSEntity === undefined"
        // if (!(cameraECSEntity instanceof Entity))
        //     throw "!(cameraECSEntity instanceof Entity)"
        // return cameraECSEntity as Entity

        const presentationMode = ryot_nullify(this.raw_getComponent(raw.ECSPresentationMode))
        if (presentationMode === null)
            throw "presentationMode === null"
        const cameraECSEntity = presentationMode.cameraEntity
        if (!(cameraECSEntity instanceof Entity))
            throw "!(cameraECSEntity instanceof Entity)"
        return cameraECSEntity as Entity
    }
}
classesByName["ECSExperience"] = ARExperience

export class ARMode {
    public readonly isAR = true
    public get camera(): Entity { return experience!.cameraEntity }
}

class PreviewMode {
    public readonly isAR = false
    public readonly camera: PreviewCamera = new PreviewCamera()
}

class PreviewCamera {
    public get entity(): Entity { return experience!.cameraEntity }
    public get transform(): Transform { return this.entity.transform }
    public get orbitCameraController(): OrbitCameraController { 
        let orbitCameraControllerComponent = this.entity.getComponent(OrbitCameraController)!
        if (orbitCameraControllerComponent === null)
            throw "orbitCameraControllerComponent === null"
        return orbitCameraControllerComponent
    }
}


interface AnyComponentClassDefinition {
    isComponentClass: boolean
    nativeClassName: string
    allowMultipleInstancesPerEntity: boolean
    new (...args: any[]): Component
}
function isComponentClass(value: any): value is AnyComponentClassDefinition { 
    return 'isComponentClass' in value
}

interface ComponentClassDefinition<T extends Component> 
    /*
        it's important that ComponentClassDefinition does _not_ extend AnyComponentClassDefinition. 
        i'm not sure why, but doing so seems to confuse the typescript type checker. 
        in that case, `let component = entity.getComponent(SomeComponent)` 
        would infer the type of `component` as `Component`, not `SomeComponent`. 
        (interestingly, if we explicitly type `component`, as in 
        `let component: SomeComponent = entity.getComponent(SomeComponent)`, that still works.)
    */
    // extends AnyComponentClassDefinition 
    { 

    isComponentClass: boolean
    nativeClassName: string
    allowMultipleInstancesPerEntity: boolean
    new (...args: any[]): T
}
  

export class Entity extends raw.ECSEntity {
    constructor()
    constructor(replicationInfo: NewObjectReplicationInfo)
    constructor(replicationInfo?: NewObjectReplicationInfo) {
        super(replicationInfo)
        if (replicationInfo === undefined) {
            //  Entity created by script, add default components
            this.addComponent(new Transform())
        }
    }

    get name(): string { return this.raw_name ?? "" }
    set name(newValue: string) { this.raw_name = newValue }

    get transform(): Transform { 
        const transform = this.getComponent(Transform)
        return transform!
    }

    get parent(): Entity | null {  
        return ryot_nullify(this.transform.parent?.entity)
    }

    set parent(parentEntity: Entity | null) { 
        this.transform.parent = ryot_nullify(parentEntity?.transform)
    }

    get children(): ReadonlyArray<Entity> { 
        let maybeChildEntities = this.transform.children.map(tr => tr.entity)
        let childEntities: ReadonlyArray<Entity> = maybeChildEntities.filter(notNull)
        return childEntities
    }
  
    childCount(): number { return this.transform.childCount() }

    getChild(index: number): Entity | null { 
        if (index < 0 || index >= this.transform.getChildCount())
            return null
        const childTransform = this.transform.getChild(index)
        return ryot_nullify(childTransform?.entity)
    }

    forEachDescendantInHierarchy(action: (entity: Entity) => void): void { 
        action(this)
        const childEntites = this.children
        childEntites.forEach(childEntity => childEntity.forEachDescendantInHierarchy(action))
    }
    findDescendantInHierarchy(predicate: (entity: Entity) => Boolean): Entity | null { 
        if (predicate(this))
            return this
        const childEntites = this.children
        for (let childEntity of childEntites) { 
            const foundInChildEntity = childEntity.findDescendantInHierarchy(predicate)
            if (foundInChildEntity !== null)
                return foundInChildEntity
        }
        return null
    }

    forEachChild(action: (child: Entity) => void): void { 
        const childEntites = this.children
        childEntites.forEach(action)
    }
    findChild(predicate: (child: Entity) => Boolean): Entity | null { 
        const childEntites = this.children
        for (const childEntity of childEntites) { 
            if (predicate(childEntity))
                return childEntity
        }
        return null
    }

    addComponent<T extends Component>(component: T): T | null { 
        if (component.entity !== null)
            return null

        const componentClass = component.constructor
        if (!isComponentClass(componentClass))
            throw "!isComponentClassDefinition: " + componentClass

        if (!componentClass.allowMultipleInstancesPerEntity) { 
            const existingComponent = this.getComponent(componentClass)
            if (existingComponent !== null)
                return null
        }

        // component.setEntity(this)
        super.raw_addComponent(component)
        return component
    }

    removeComponent<T extends Component>(component: T): T | null { 
        //  TODO read an isRemovable field from the class def instead of hardwiring
        if (component.constructor === Transform)
            return null
        if (component.constructor === OrbitCameraController)
            return null
        if (component.constructor === MeshRenderable)
            return null
        // if (component.constructor === ObjectAnimator)
        //     return null
        if (component.constructor === BoxCollider)
            return null
        this.raw_removeComponent(component)
        return component
    }

    getComponent<T extends Component>(classDef: ComponentClassDefinition<T>): T | null { 
        // if (!isIOSRawComponentClass(classDef))
        //     return null
        return this.raw_getComponent(classDef)
    }

    getComponents<T extends Component>(classDef: ComponentClassDefinition<T>): Array<T> { 
        // if (!isIOSRawComponentClass(classDef))
        //     return []
        return this.raw_getComponents(classDef)
    }

    setActive(active: boolean) { throw "unimplemented" }

    //  private

}
classesByName["ECSEntity"] = Entity
classesByName["ECSObjectInstance"] = Entity

/*
export abstract class System extends raw.ECSOnFrame { 
    abstract update(dt: number): void
    constructor() { 
        if (experience === null)
            throw "experience === null"
        super(dt => this.update(dt))
        experience.raw_addComponent(this)
    }
}
*/

export abstract class System { 
    abstract update(dt: number): void
    constructor() { 
        if (experience === null)
            throw "experience === null"
        this.arsdk_onFrameComponent = new raw.ECSOnFrame()
        this.arsdk_onFrameComponent.onFrameAction = (dt: number) => { this.update(dt) }
        // this.arsdk_onFrameComponent.onFrameAction = this.update
    }

    readonly arsdk_onFrameComponent: raw.ECSOnFrame
}

export class Component extends raw.ECSComponent {
    static nonInheritableNativeClassName = "ECSComponent"

    static allowMultipleInstancesPerEntity = true

    // constructor()
    // constructor(replicationInfo: NewObjectReplicationInfo)
    // constructor(replicationInfo?: NewObjectReplicationInfo) { 
    //     //  see comments at the top ("Component constructors")
    //     super(replicationInfo ?? arDoNotReplicateConstructorTag)
    // }

    get entity(): Entity | null { 
        // if (this.objectId === "arsdk_uninitialized_objectId")
        //     return null
        // return this.getEntity() as Entity 
        let ecsEntity = super.entity
        if (ecsEntity === null) 
            return null
        return ecsEntity as Entity
    }

    // /* fileprivate */ setEntity(entity: Entity): void {         
    //     if (this.entity !== null)
    //         throw "this.entity !== null"
    //     replicate(this, entity)
    // }
}
classesByName["ECSComponent"] = Component
  
export class Transform extends raw.ECSScriptTransform implements Component {
    get parent(): Transform | null {  
        let rawTransform = this.raw_parent 
        if (rawTransform === null) 
            return null
        return rawTransform as Transform
    }
    set parent(newParentTransform: Transform | null) { 
        if (newParentTransform !== null && newParentTransform.entity === null)
            throw "newParentTransform.entity === null"
        this.raw_parent = newParentTransform
    }

    // constructor()
    // constructor(replicationInfo: NewObjectReplicationInfo)    
    // constructor(replicationInfo?: NewObjectReplicationInfo) { 
    //     super(replicationInfo ?? arDoNotReplicateConstructorTag)
    // }

    //  TODO impl array references, make iterator functions work with array refs

    forEachDescendantInHierarchy(action: (transform: Transform) => void): void { 
        action(this)
        const childTransforms = this.children
        childTransforms.forEach(childTransform => childTransform.forEachDescendantInHierarchy(action))
    }

    findDescendantInHierarchy(predicate: (transform: Transform) => Boolean): Transform | null { 
        if (predicate(this))
            return this
        const childTransforms = this.children
        for (const childTransform of childTransforms) { 
            const foundInChild = childTransform.findDescendantInHierarchy(predicate)
            if (foundInChild !== null)
                return foundInChild
        }
        return null
    }

    forEachChild(action: (child: Transform) => void): void { 
        const childTransforms = this.children
        childTransforms.forEach(action)
    }
    findChild(predicate: (child: Transform) => Boolean): Transform | null { 
        const childTransforms = this.children
        for (const childTransform of childTransforms) { 
            if (predicate(childTransform))
                return childTransform
        }
        return null
    }
    
    get children(): ReadonlyArray<Transform> { 
        // let children: Transform[] = []
        // for (let i = 0; i < this.getChildCount(); ++i) { 
        //     children.push(this.getChild(i) as Transform)
        // }
        // return children
        return this.raw_children.map(ch => ch as Transform)
    }

    //  ---

    //  TODO instead of re-implementing entity() and setEntity() in each Component subclass, use mixins. 

    static allowMultipleInstancesPerEntity = false

    get entity(): Entity | null { 
        // if (this.objectId === "arsdk_uninitialized_objectId")
        //     return null
        // return this.getEntity() as Entity 
        let ecsEntity = super.entity
        if (ecsEntity === null) 
            return null
        return ecsEntity as Entity
    }

    // /* fileprivate */ setEntity(entity: Entity): void { 
    //     if (this.entity !== null)
    //         throw "this.entity !== null"
    //     replicate(this, entity)        
    // }

    get worldPosition(): Vector3 { return arsdk_createReplicatedVector(this.raw_worldPosition, v => this.raw_worldPosition = v) }
    get worldRotation(): Quaternion { return arsdk_createReplicatedQuaternion(this.raw_worldOrientation, q => this.raw_worldOrientation = q) }
    get worldScale(): Vector3 { return arsdk_createReplicatedVector(this.raw_worldScale, v => this.raw_worldScale = v) }

    get localPosition(): Vector3 { return arsdk_createReplicatedVector(this.raw_localPosition, v => this.raw_localPosition = v) }
    get localRotation(): Quaternion { return arsdk_createReplicatedQuaternion(this.raw_localOrientation, q => this.raw_localOrientation = q) }
    get localScale(): Vector3 { return arsdk_createReplicatedVector(this.raw_localScale, v => this.raw_localScale = v) }

    get isReadOnly(): boolean { 
        //  TODO fix
        return false
    }

    //  TODO remove
    getChild(index: number): Transform | null { 
        const child = super.getChild(index) 
        if (child === null) 
            return null
        if (!(child instanceof Transform))
            return null
        return child as Transform
    }
    childCount(): number { return super.getChildCount() }
}
classesByName["ECSScriptTransform"] = Transform
  
export namespace OnTap {
    export type OnTapCallback = (hitPointWorld: Vector3) => void
}
export class OnTap extends raw.ECSOnTap implements Component {
    constructor(onTapCallback: OnTap.OnTapCallback)
    constructor(onTapCallbackOrReplicationInfo: OnTap.OnTapCallback | NewObjectReplicationInfo) { 
        if (onTapCallbackOrReplicationInfo instanceof NewObjectReplicationInfo)
            throw "OnTap must not be replicated native-to-script"
        // super(arDoNotReplicateConstructorTag)
        // this.onTap = onTapCallbackOrReplicationInfo
        let onTap = onTapCallbackOrReplicationInfo
        super(
            (worldPosition: number[]) => { 
                const hitPointWorld = Vector3.fromArray(worldPosition)
                if (hitPointWorld === null) return
                onTap(hitPointWorld)
            })
    }

    //  TODO instead of re-implementing entity() and setEntity() in each Component subclass, use mixins. 
    static allowMultipleInstancesPerEntity = true

    get entity(): Entity | null { 
        // if (this.objectId === "arsdk_uninitialized_objectId")
        //     return null
        // return this.getEntity() as Entity 
        let ecsEntity = super.entity
        if (ecsEntity === null) 
            return null
        return ecsEntity as Entity
    }

    // /* fileprivate */ setEntity(entity: Entity): void { 
    //     if (this.entity !== null)
    //         throw "this.entity !== null"
    //     replicate(
    //         this, 
    //         entity, 
    //         (worldPosition: number[]) => { 
    //             const hitPointWorld = Vector3.fromArray(worldPosition)
    //             if (hitPointWorld === null) return
    //             this.onTap(hitPointWorld)
    //         })
    // }
    
    // private onTap: (hitPointWorld: Vector3) => void
}
classesByName["ECSOnTap"] = OnTap
  
export class MeshRenderable extends raw.ECSMesh {
    //  castShadows, receiveShadows, material, materials, getPrimitiveCount, setMaterialAt inherited from ECSMesh

    get material(): Material { return super.material as Material }
    get materials(): Material[] { return super.materials as Material[] }
}
classesByName["ECSMesh"] = MeshRenderable 

export class Material extends raw.ECSMaterial {
    //  opacity, metalness, roughness, emissive inherited from ECSMaterial

    get color(): Color { return arsdk_createReplicatedColor(this.raw_color, v => this.raw_color = v) }
    set color(newValue: Color) { this.raw_color = [newValue.r, newValue.g, newValue.b, newValue.a] }

    clone(): Material { 
        let copy = new Material()
        copy.color = this.color
        copy.opacity = this.opacity
        copy.metalness = this.metalness
        copy.roughness = this.roughness
        copy.emissive = this.emissive
        return copy
    }

    setFade(opacity: number) { 
        //  TODO useFadeBlendingMode()
        this.opacity = opacity
    }
}
classesByName["ECSMaterial"] = Material 

export class OrbitCameraController extends raw.ECSOrbitCameraController implements Component {
    static allowMultipleInstancesPerEntity = false

    get entity(): Entity | null { 
        // if (this.objectId === "arsdk_uninitialized_objectId")
        //     return null
        // return this.getEntity() as Entity 
        let ecsEntity = super.entity
        if (ecsEntity === null) 
            return null
        return ecsEntity as Entity
    }

    // /* fileprivate */ setEntity(entity: Entity): void { 
    //     if (this.entity !== null)
    //         throw "this.entity !== null"
    //     replicate(this, entity)        
    // }

    get pivotPosition(): Vector3 { return arsdk_createReplicatedVector(this.raw_pivotPosition, v => this.raw_pivotPosition = v) }
    // get cameraPosition(): Vector3 { return arsdk_createReplicatedVector(this.raw_cameraPosition, v => this.raw_cameraPosition = v) }
    get cameraPosition(): Vector3 { return this.entity!.transform!.worldPosition }

    animateTo(pivotPosition: Vector3, cameraPosition: Vector3, duration: number) { 
        this.raw_animateTo(pivotPosition.asArray(), cameraPosition.asArray(), duration)
    }
}
classesByName[raw.ECSOrbitCameraController.nativeClassName] = OrbitCameraController

export class ObjectAnimator extends raw.ECSObjectAnimator implements Component { 
    get entity(): Entity | null { 
        // if (this.objectId === "arsdk_uninitialized_objectId")
        //     return null
        // return this.getEntity() as Entity 
        let ecsEntity = super.entity
        if (ecsEntity === null) 
            return null
        return ecsEntity as Entity
    }

    // /* fileprivate */ setEntity(entity: Entity): void { 
    //     if (this.entity !== null)
    //         throw "this.entity !== null"
    //     replicate(this, entity)        
    // }

    getAnimation(animationName: string): Animation { 
        let rawAnimation = this.getOrCreateAnimationInstanceNamed(animationName)
        if (rawAnimation === null)
            throw "animation not found by name: " + animationName
        return rawAnimation as Animation
    }

    getAnimations(): ReadonlyArray<Animation> { 
        let rawAnimations = this.getOrCreateAnimationInstancesForAllTemplates()
        let animations = rawAnimations.map(rawAnimation => { return rawAnimation as Animation })
        return animations
    }
}
classesByName[raw.ECSObjectAnimator.nativeClassName] = ObjectAnimator

export class Animation extends raw.ECSObjectAnimationInstance {
    isRunning(): boolean { return this.raw_isRunning }
    getDuration(): number { return this.duration }
    getCurrentProgress(): number { return this.progress }
    start(): this { this.raw_start(); return this }
    pause(): this { this.raw_pause(); return this }
    resume(): this { this.raw_resume(); return this }
}
classesByName[raw.ECSObjectAnimationInstance.nativeClassName] = Animation

export class AudioSource extends raw.ECSAudioSource implements Component {
    get entity(): Entity | null { 
        // if (this.objectId === "arsdk_uninitialized_objectId")
        //     return null
        // return this.getEntity() as Entity 
        let ecsEntity = super.entity
        if (ecsEntity === null) 
            return null
        return ecsEntity as Entity
    }

    // /* fileprivate */ setEntity(entity: Entity): void { 
    //     if (this.entity !== null)
    //         throw "this.entity !== null"
    //     if (experience === null)
    //         throw "experience === null"
    //     replicate(this, entity, experience)
    // }

    // constructor()
    // constructor(replicationInfo: NewObjectReplicationInfo)    
    // constructor(replicationInfo?: NewObjectReplicationInfo) { 
    //     super(replicationInfo ?? arDoNotReplicateConstructorTag)
    // }
    constructor(replicationInfo?: NewObjectReplicationInfo) { 
        if (replicationInfo === undefined) {
            if (experience === null)
                throw "experience === null"
            super(experience)
        } else 
            super(replicationInfo)
    }
    
    get volume(): number { return this.firstClip?.volume ?? 1 }
    set volume(newValue: number) { if (this.firstClip !== null) this.firstClip.volume = newValue }
    get loop(): boolean { return this.firstClip?.loops ?? false }
    set loop(newValue: boolean) { if (this.firstClip !== null) this.firstClip.loops = newValue }

    setClip(clip: string): void {  
        this.firstClip?.stop()
        this.getOrCreateAudioClipInstanceNamed(clip)
    }
    play(): void { 
        this.checkFirstClip()
        //  TODO should call resume() instead of start() if paused
        this.firstClip?.start() 
    }
    pause(): void { this.checkFirstClip(); this.firstClip?.pause() }
    stop(): void { this.checkFirstClip(); this.firstClip?.stop() }

    isPlaying(): boolean { return this.firstClip?.isRunning ?? false }
    getDuration(): number { this.checkFirstClip(); return this.firstClip?.duration ?? 0 }
    getCurrentTime(): number { return this.getCurrentProgress() * this.getDuration() }
    getCurrentProgress(): number { return this.firstClip?.progress ?? 0 }

    private checkFirstClip() { 
        if (this.firstClip === null)
            throw "AudioSource.play(): no clip"
    }
}
classesByName["ECSAudioSource"] = AudioSource



export abstract class Collider extends Component {}
export class BoxCollider extends Collider {}



export let experience: ARExperience | null = null

//  TODO remove
// export let legacyExperience: raw.ScriptExperience

export function setGlobal(name: string, value: any) { 
    if (name === "experience")
        experience = value
}
