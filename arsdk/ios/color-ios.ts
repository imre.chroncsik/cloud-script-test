
export function arsdk_createReplicatedColor(
    maybeRawColorArray: number[] | null, 
    syncToNative: (v: number[]) => void)
    : Color { 

    let replicated = Color.fromArray(maybeRawColorArray ?? [0, 0, 0, 1])
    replicated._didChangeCallback = () => { syncToNative([replicated.r, replicated.g, replicated.b, replicated.a]) }
    return replicated
}

export class Color {
    static fromHex(hex: string): Color { 
        return new Color(0, 0, 0).setHex(hex)
    }

    static fromArray(array: number[]): Color { 
        if (array.length == 3)
            array.push(1)   //  default alpha
        if (array.length !== 4)
            throw "array.length !== 4"
        return new Color(array[0], array[1], array[2], array[3])
    }

    private _r: number
    private _g: number
    private _b: number
    private _a: number

    _didChangeCallback: (() => void) | null = null
    private didChange() { 
        if (this._didChangeCallback !== null)
            this._didChangeCallback()
    }

    get r() { return this._r }
    set r(value: number) { this._r = value; this.didChange() }
    get g() { return this._g }
    set g(value: number) { this._g = value; this.didChange() }
    get b() { return this._b }
    set b(value: number) { this._b = value; this.didChange() }
    get a() { return this._a }
    set a(value: number) { this._a = value; this.didChange() }

    constructor(red: number, green: number, blue: number, alpha?: number) {
        this._r = red
        this._g = green
        this._b = blue
        this._a = alpha ?? 1
    }

    setHex(hex: string): this {
        //  accepted formats: #rrggbb, #rrggbbaa, rrggbb, rrggbbaa
        if (hex[0] !== "#")
            hex = "#" + hex
        if (hex.length === 7)
            hex = hex + "00"
        if (hex.length !== 9)
            throw "wrong format"
        hex = "0x" + hex.substring(1)
        const rgba = Number(hex)
        this._r = ((rgba & 0xff000000) >> 24) / 255
        this._g = ((rgba & 0x00ff0000) >> 16) / 255
        this._b = ((rgba & 0x0000ff00) >> 8) / 255
        this._a = ((rgba & 0x000000ff) >> 0) / 255
        this.didChange()
        return this
    }

    clone(): Color { return new Color(this.r, this.g, this.b, this.a) }
    
    copy(/*from*/c: Color): this {
        this._r = c._r
        this._g = c._g
        this._b = c._b
        this._a = c._a
        this.didChange()
        return this
    }

    convertLinearToSRGB(): this {
        this._r = this.sRGBComponentFromLinear(this._r)
        this._g = this.sRGBComponentFromLinear(this._g)
        this._b = this.sRGBComponentFromLinear(this._b)
        this.didChange()
        return this
    }

    convertSRGBToLinear(): this {
        this._r = this.linearComponentFromSRGB(this._r)
        this._g = this.linearComponentFromSRGB(this._g)
        this._b = this.linearComponentFromSRGB(this._b)
        this.didChange()
        return this
    }

    set(r: number, g: number, b: number, a?: number): this {
        this._r = r
        this._g = g
        this._b = b
        this._a = a ?? 1
        this.didChange()
        return this
    }

    private sRGBComponentFromLinear(l: number): number { 
        if (l <= 0.0031308)
            return l * 12.92
        return 1.055 * Math.pow(l, 1 / 2.4) - 0.055
    }

    private linearComponentFromSRGB(s: number): number { 
        if (s <= 0.04045)
            return s / 12.92
        return Math.pow(((s + 0.055) / 1.055), 2.4)
    }
}
