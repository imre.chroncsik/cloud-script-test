
/*
    bootstrap a script execution environment, running on an ios device.
    wire up a pre-existing narrow-intf implementation 
    (should already be available in the global namespace before loading this)
    with arscript and arsdk-ios-raw, 
    in order to get a fully functioning environment. 
    then set up a "start" callback which launches the main user script. 
    see also arsdk/README.md > Bootstrapping. 
*/

// let mainModuleResolvedFilename_: string
let importMain_: () => void
export function init(importMain: () => void) { 
    importMain_ = importMain
}


//  having a suitable executeNative_str() in the global namespace 
//  is a requirement, check for that. 
// let executeNative_str = (global as any).arsdk__executeNative_str__
// declare var arsdk__executeNative_str__: (nativeCallJsonString: string) => string
// let executeNative_str = arsdk__executeNative_str__
let executeNative_str = (globalThis as any).arsdk__executeNative_str__
if (executeNative_str === undefined)
    throw "'executeNative_str' is undefined"
if (typeof executeNative_str !== "function")
    throw `'executeNative_str' must be a function (is ${typeof executeNative_str})`

//  use the global executeNative_str() to create an ARScript instance, 
//  and use the ARScript instance to init() arsdk-ios-raw. 
//  the latter gives us definitions (not only declarations, also impl)
//  for iOS ARSDK symbols (eg. classes). 

import { ARScript } from "./arscript.js"
import * as arsdk_ios from "./arsdk-ios.js"
import * as arsdk_ios_raw from "./arsdk-ios-raw.js"

let arscript = new ARScript()
arscript.executeNative_str = executeNative_str
arscript.setGlobal = arsdk_ios.setGlobal
arsdk_ios_raw.init(arscript)


//  expose some functions into the global namespace, 
//  so that the native side can easily call them. 
;(globalThis as any).__arscript_replicateNativeToScript = 
    (nativeObject: { className: string, objectId: string }, 
    name: string | undefined)
    : any => { 
        return arscript.replicateNativeToScript(nativeObject, name) 
    }
;(globalThis as any).__arscript_executeCallback = 
    (callbackJsonString: string): string => { 
        const result = arscript.executeCallback(callbackJsonString) 
        if (typeof result !== "string")
            throw "typeof result !== string"
        return result
    }

/*
    we only load main.ts upon receiving a "start" message 
    (as opposed to auto-loading it immediately), 
    to give the native side a chance to do further initialization 
    in between loading the script bundle and running the main user script. 
    see also arsdk/README.md > Bootstrapping. 
*/
arscript.callbacks["start"] = () => { 
    // import('../../src/main') 
    importMain_()
}

