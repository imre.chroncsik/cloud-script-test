//  interface defined in arsdk/index.d.ts
//  implementation mostly borrowed from ThreeJS.Quaternion
//      (https://github.com/mrdoob/three.js/blob/dev/src/math/Quaternion.js)

import * as arsdk_math from "./arsdk-math-ios.js"

export function arsdk_createReplicatedQuaternion(
    maybeRawQuaternionArray: number[] | null, 
    syncToNative: (v: number[]) => void)
    : Quaternion { 

    let replicated = Quaternion.fromArray(maybeRawQuaternionArray ?? [0, 0, 0])
    replicated._didChangeCallback = () => { syncToNative([replicated.x, replicated.y, replicated.z, replicated.w]) }
    return replicated
}

export class Quaternion {
    static fromArray(array: number[]): Quaternion { 
        if (array.length !== 4)
            throw "array.length !== 4"
        return new Quaternion(array[0], array[1], array[2], array[3])
    }

    private _x: number
    private _y: number
    private _z: number
    private _w: number

    _didChangeCallback: (() => void) | null = null
    private didChange() { 
        if (this._didChangeCallback !== null)
            this._didChangeCallback()
    }

    get x() { return this._x }
    set x(value: number) { this._x = value; this.didChange() }
    get y() { return this._y }
    set y(value: number) { this._y = value; this.didChange() }
    get z() { return this._z }
    set z(value: number) { this._z = value; this.didChange() }
    get w() { return this._w }
    set w(value: number) { this._w = value; this.didChange() }

    constructor(x?: number, y?: number, z?: number, w?: number) { 
        this._x = x ?? 0
        this._y = y ?? 0
        this._z = z ?? 0
        this._w = w ?? 1
    }

    asArray(): number[] { return [this._x, this._y, this._z, this._w] }

    angleTo(q: Quaternion): number { 
        const clampedDot = Math.max(-1, Math.min(1, this.dot(q)))
        const angleRadians = 2 * Math.acos(Math.abs(clampedDot))
        const angleDegrees = angleRadians * arsdk_math.rad2deg
        return angleDegrees
    }

    clone(): Quaternion { 
        return new Quaternion(this.x, this.y, this.z, this.w)
    }
     
    conjugate(): this { 
		this._x *= -1
		this._y *= -1
		this._z *= -1
		this.didChange()
		return this
    }
     
    copy(q: Quaternion): this { 
		this._x = q.x
		this._y = q.y
		this._z = q.z
		this._w = q.w
		this.didChange()
		return this
    }
     
    equals(q: Quaternion): Boolean { 
        return (q._x === this._x) && (q._y === this._y) && (q._z === this._z) && (q._w === this._w)
    }
     
    dot(v: Quaternion): number { 
        return this._x * v._x + this._y * v._y + this._z * v._z + this._w * v._w
    }
     
    identity(): this { return this.set(0, 0, 0, 1) }
    invert(): this { return this.conjugate() }
     
    length(): number {
        return Math.sqrt(this._x * this._x + this._y * this._y + this._z * this._z + this._w * this._w)
    }
     
    lengthSq(): number { 
        return this._x * this._x + this._y * this._y + this._z * this._z + this._w * this._w
    }
     
    normalize(): this { 
		let l = this.length()
		if (l === 0) {
			this._x = 0
			this._y = 0
			this._z = 0
			this._w = 1
		} else {
			l = 1 / l
			this._x = this._x * l
			this._y = this._y * l
			this._z = this._z * l
			this._w = this._w * l
		}
		this.didChange()
		return this
    }
    
    multiply(q: Quaternion): this { return this.multiplyQuaternions(this, q) }
    premultiply(q: Quaternion): this { return this.multiplyQuaternions(q, this) }

    multiplyQuaternions(a: Quaternion, b: Quaternion): this { 
		const qax = a._x, qay = a._y, qaz = a._z, qaw = a._w
		const qbx = b._x, qby = b._y, qbz = b._z, qbw = b._w

        this._x = qax * qbw + qaw * qbx + qay * qbz - qaz * qby
		this._y = qay * qbw + qaw * qby + qaz * qbx - qax * qbz
		this._z = qaz * qbw + qaw * qbz + qax * qby - qay * qbx
		this._w = qaw * qbw - qax * qbx - qay * qby - qaz * qbz

        this.didChange()
		return this
    }
    
    rotateTowards(q: Quaternion, stepDegrees: number): this { 
        const stepRadians = stepDegrees * arsdk_math.deg2rad
		const angleDegrees = this.angleTo(q)
        const angleRadians = angleDegrees * arsdk_math.deg2rad
		if (angleRadians === 0) 
            return this
		const t = Math.min(1, stepRadians / angleRadians)
		this.slerp(q, t)
		return this
    }
     
    slerp(qb: Quaternion, t: number): this { 
		if (t === 0) return this
		if (t === 1) return this.copy(qb)

		const x = this._x, y = this._y, z = this._z, w = this._w

		let cosHalfTheta = w * qb._w + x * qb._x + y * qb._y + z * qb._z;

		if (cosHalfTheta < 0) {
			this._w = - qb._w
			this._x = - qb._x
			this._y = - qb._y
			this._z = - qb._z
			cosHalfTheta = - cosHalfTheta
		} else {
			this.copy(qb)
		}

		if (cosHalfTheta >= 1.0) {
			this._w = w
			this._x = x
			this._y = y
			this._z = z
			return this
		}

		const sqrSinHalfTheta = 1.0 - cosHalfTheta * cosHalfTheta
		if (sqrSinHalfTheta <= Number.EPSILON) {
			const s = 1 - t
			this._w = s * w + t * this._w
			this._x = s * x + t * this._x
			this._y = s * y + t * this._y
			this._z = s * z + t * this._z

			this.normalize()
			this.didChange()
			return this
		}

		const sinHalfTheta = Math.sqrt(sqrSinHalfTheta)
		const halfTheta = Math.atan2(sinHalfTheta, cosHalfTheta)
		const ratioA = Math.sin((1 - t) * halfTheta) / sinHalfTheta,
			ratioB = Math.sin(t * halfTheta) / sinHalfTheta

		this._w = (w * ratioA + this._w * ratioB)
		this._x = (x * ratioA + this._x * ratioB)
		this._y = (y * ratioA + this._y * ratioB)
		this._z = (z * ratioA + this._z * ratioB)

		this.didChange()
		return this
    }
     
    slerpQuaternions(qa: Quaternion, qb: Quaternion, t: number): this { 
        this.copy(qa).slerp(qb, t)
        return this
    }
     
    set(x: number, y: number, z: number, w: number): this { 
		this._x = x
		this._y = y
		this._z = z
		this._w = w
		this.didChange()
		return this        
    }
     
    setFromAxisAngle(axis: Vector3, angleDegrees: number): this { 
        let angleRadians = angleDegrees * arsdk_math.deg2rad
		const halfAngle = angleRadians / 2
        const s = Math.sin(halfAngle)

		this._x = axis.x * s
		this._y = axis.y * s
		this._z = axis.z * s
		this._w = Math.cos(halfAngle)

		this.didChange()
		return this
    }
     
    setRotationBetweenVectors(vFrom: Vector3, vTo: Vector3): this { 
        const xyz = vFrom.cross(vTo)
        this._x = xyz.x
        this._y = xyz.y
        this._z = xyz.z
        this._w = Math.sqrt(vFrom.lengthSq() + vTo.lengthSq()) + vFrom.dot(vTo)
        return this.normalize()
    }
    
    setEulerAngles(eulerAngles: Vector3): this { 
		const x = eulerAngles.x * arsdk_math.deg2rad
        const y = eulerAngles.y * arsdk_math.deg2rad
        const z = eulerAngles.z * arsdk_math.deg2rad

		const cos = Math.cos
		const sin = Math.sin

		const c1 = cos(x / 2)
		const c2 = cos(y / 2)
		const c3 = cos(z / 2)

		const s1 = sin(x / 2)
		const s2 = sin(y / 2)
		const s3 = sin(z / 2)

        this._x = s1 * c2 * c3 - c1 * s2 * s3
        this._y = c1 * s2 * c3 + s1 * c2 * s3
        this._z = c1 * c2 * s3 - s1 * s2 * c3
        this._w = c1 * c2 * c3 + s1 * s2 * s3

		this.didChange()
		return this
    }
}

//  TODO rename to RefQuaternion? 
/*
class ReplicatedQuaternion extends Quaternion { 
    static fromArray(array: number[]): ReplicatedQuaternion { 
        if (array.length !== 4)
            throw "array.length !== 4"
        return new ReplicatedQuaternion(array[0], array[1], array[2], array[3])
    }

    syncToNative: (() => void) | null = null
    private syncToNative_() { 
        if (this.syncToNative === null) 
            return
        this.syncToNative()
    }
    
    get x() { return this._x }
    set x(value: number) { this._x = value; this.syncToNative_() }
    get y() { return this._y }
    set y(value: number) { this._y = value; this.syncToNative_() }
    get z() { return this._z }
    set z(value: number) { this._z = value; this.syncToNative_() }
    get w() { return this._w }
    set w(value: number) { this._w = value; this.syncToNative_() }
}
*/

import { Vector3 } from "./vector3-ios.js"

let _quaternion: Quaternion
let _vector: Vector3

function initHelperObjects() { 
    //  see comments in vector3-ios.ts, initHelperObjects
    if (_quaternion === undefined)
        _quaternion = new Quaternion()
    if (_vector === undefined) 
        _vector = new Vector3()
}
