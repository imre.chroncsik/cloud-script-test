
/*
    ScriptConnectionWorker
    *   Starts a websocket server that the remote app can connect to. 
    *   Primarily used by RemoteApp. 
    *   Should be launched as a worker thread. 
    *   Can send and receive (string) messages to / from the remote app. 
    *   Once receives a message, notifies its parent thread about it. 
    *   Actually sends two notifications: 
        *   Atomics.notify(this.messageLengthArray, 0, 1)
        *   parentPort?.postMessage("didReceiveWorkerMessage")
    *   It's up to the parent to decide which notification to listen to. 
        RemoteApp uses them both: 
        Atomics.notify() -> Atomics.wait() while blocked waiting for a call response, 
        postMessage() -> worker.on('message') otherwise. 
        See the comments in remote-app.ts for details. 
*/

import { workerData, parentPort } from 'worker_threads'
import WebSocket from "ws"
import { assert } from 'console'

export class ScriptConnectionWorker { 
    constructor() { 
        this.messageLengthArray = new Int32Array(workerData.messageLengthBuffer)
        this.messageArray = new Uint8Array(workerData.messageBuffer)
        parentPort?.on('message', (message) => { this.didReceiveMainThreadMessage(message) })
        this.startServer()
    }

    private messageLengthArray: Int32Array
    private messageArray: Uint8Array
    private wsServer?: WebSocket.Server = undefined
    private webSocket?: WebSocket = undefined

    startServer() {  
        this.wsServer = new WebSocket.Server({ port: 2897 })
        this.wsServer.on('connection', (webSocket: WebSocket) => {
            this.webSocket = webSocket
            webSocket.on('message', (message) => { 
                this.didReceiveRemoteMessage(message)
            })
            this.log("clientDidConnect")
        })
    }

    didReceiveRemoteMessage(remoteMessage: any) { 
        let messagesString: string = remoteMessage.toString()
        let messageStrings = messagesString.split("###arsdk-message-separator###")
        for (const messageString of messageStrings) {
            if (messageString === "") continue

            if (messageString.length > this.messageArray.length) { 
                this.sendToMainThread("error: buffer overflow " + 
                    messageString.length + " > " + this.messageArray.length)
                return
            }

            this.sendToMainThread(messageString)
        }
    }

    log(message: string) { 
        this.sendToMainThread("log: " + message)
    }

    error(message: string) { 
        this.sendToMainThread("error: " + message)
    }

    sendToMainThread(message: string) { 
        assert(message.length <= this.messageArray.length)

        ;(new TextEncoder()).encodeInto(message, this.messageArray)
        let messageLength = message.length
        this.messageLengthArray[0] = messageLength
        Atomics.notify(this.messageLengthArray, 0, 1)

        parentPort?.postMessage("didReceiveWorkerMessage")

        //  wait for the main thread to set length back to 0 once it finished receiving the message. 
        Atomics.wait(this.messageLengthArray, 0, messageLength)     
    }

    didReceiveMainThreadMessage(message: string) { 
        this.sendToNative(message)
    }

    sendToNative(message: string) { 
        this.webSocket?.send(message)
    }
}

export let scriptConnectionWorker = new ScriptConnectionWorker()
