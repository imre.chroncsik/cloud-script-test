
//  interface defined in arsdk/index.d.ts
//  implementation mostly borrowed from ThreeJS.Vector3
//      (https://github.com/mrdoob/three.js/blob/dev/src/math/Vector3.js)

import * as arsdk_math from "./arsdk-math-ios.js"

export function arsdk_createReplicatedVector(
    maybeRawVector3Array: number[] | null, 
    syncToNative: (v: number[]) => void)
    : Vector3 { 

    let replicated = Vector3.fromArray(maybeRawVector3Array ?? [0, 0, 0])
    replicated._didChangeCallback = () => { syncToNative([replicated.x, replicated.y, replicated.z]) }
    return replicated
}

export class Vector3 {
    static fromArray(array: number[]): Vector3 { 
        if (array.length !== 3)
            throw "array.length !== 4"
        return new Vector3(array[0], array[1], array[2])
    }

    private _x: number
    private _y: number
    private _z: number    

    _didChangeCallback: (() => void) | null = null
    private didChange() { 
        if (this._didChangeCallback !== null)
            this._didChangeCallback()
    }

    get x() { return this._x }
    set x(value: number) { this._x = value; this.didChange() }
    get y() { return this._y }
    set y(value: number) { this._y = value; this.didChange() }
    get z() { return this._z }
    set z(value: number) { this._z = value; this.didChange() }

    constructor(x?: number, y?: number, z?: number) { 
        this._x = x ?? 0
        this._y = y ?? 0
        this._z = z ?? 0
    }

    asArray(): number[] { return [this._x, this._y, this._z] }

    add(v: Vector3): this { 
        this._x += v.x
        this._y += v.y 
        this._z += v.z
        this.didChange()
        return this
    }

    addScalar(s: number): this { 
        this._x += s
        this._y += s
        this._z += s
        this.didChange()
        return this
    }
 
    addScaledVector(v: Vector3, s: number): this { 
        this._x += v._x * s
        this._y += v._y * s
        this._z += v._z * s
        this.didChange()
        return this
    }
 
    addVectors(a: Vector3, b: Vector3): this { 
        this._x = a._x + b._x
        this._y = a._y + b._y
        this._z = a._z + b._z
        this.didChange()
        return this 
    }
 
    applyAxisAngle(axis: Vector3, angleDegrees: number): this { 
        return this.applyQuaternion(_quaternion.setFromAxisAngle(axis, angleDegrees));
    }
 
    applyQuaternion(quaternion: Quaternion): this { 
        const x = this._x, y = this._y, z = this._z
        const qx = quaternion.x, qy = quaternion.y, qz = quaternion.z, qw = quaternion.w

        const ix = qw * x + qy * z - qz * y
        const iy = qw * y + qz * x - qx * z
        const iz = qw * z + qx * y - qy * x
        const iw = - qx * x - qy * y - qz * z

        this._x = ix * qw + iw * - qx + iy * - qz - iz * - qy
        this._y = iy * qw + iw * - qy + iz * - qx - ix * - qz
        this._z = iz * qw + iw * - qz + ix * - qy - iy * - qx

        this.didChange()
        return this
    }
 
    angleTo(v: Vector3): number { 
		const denominator = Math.sqrt(this.lengthSq() * v.lengthSq())
		if (denominator === 0) 
            return Math.PI / 2

		const theta = this.dot(v) / denominator
		const angle = Math.acos(Math.max(-1, Math.min(1, theta)))
        const angleDegrees = angle * arsdk_math.deg2rad
        return angleDegrees
    }
 
    clone(): Vector3 { return new Vector3(this.x, this.y, this.z) }
 
    copy(v: Vector3): this { 
        this._x = v.x
        this._y = v.y
        this._z = v.z
        this.didChange()        
        return this
    }
 
    cross(v: Vector3): this { return this.crossVectors( this, v ) }
 
    crossVectors(a: Vector3, b: Vector3): this { 
		const ax = a._x, ay = a._y, az = a._z
		const bx = b._x, by = b._y, bz = b._z

		this._x = ay * bz - az * by
		this._y = az * bx - ax * bz
		this._z = ax * by - ay * bx

        this.didChange()
		return this      
    }
 
    distanceTo(v: Vector3): number { return Math.sqrt(this.distanceToSquared(v)) }
 
    distanceToSquared(v: Vector3): number { 
		const dx = this._x - v._x, dy = this._y - v._y, dz = this._z - v._z
		return dx * dx + dy * dy + dz * dz
    }
 
    dot(v: Vector3): number { return this._x * v._x + this._y * v._y + this._z * v._z }
    equals(v: Vector3): boolean { return ((v._x === this._x) && (v._y === this._y) && (v._z === this._z)) }
    length(): number { return Math.sqrt(this._x * this._x + this._y * this._y + this._z * this._z) } 
    lengthSq(): number { return this._x * this._x + this._y * this._y + this._z * this._z }
 
    lerp(v: Vector3, alpha: number): this { 
		this._x += (v._x - this._x) * alpha
		this._y += (v._y - this._y) * alpha
		this._z += (v._z - this._z) * alpha
        this.didChange()
		return this       
    }
 
    lerpVectors(v1: Vector3, v2: Vector3, alpha: number): this { 
		this._x = v1._x + (v2._x - v1._x) * alpha
		this._y = v1._y + (v2._y - v1._y) * alpha
		this._z = v1._z + (v2._z - v1._z) * alpha
        this.didChange()
		return this
    }
 
    max(v: Vector3): this { 
		this._x = Math.max(this._x, v._x)
		this._y = Math.max(this._y, v._y)
		this._z = Math.max(this._z, v._z)
        this.didChange()
		return this
    }
 
    min(v: Vector3): this { 
		this._x = Math.min(this._x, v._x)
		this._y = Math.min(this._y, v._y)
		this._z = Math.min(this._z, v._z)
        this.didChange()
		return this
    }
 
    multiply(v: Vector3): this { 
		this._x *= v._x
		this._y *= v._y
		this._z *= v._z
        this.didChange()
		return this
    }
 
    multiplyScalar(s: number): this { 
		this._x *= s
		this._y *= s
		this._z *= s
        this.didChange()
		return this
    }
 
    multiplyVectors(a: Vector3, b: Vector3): this { 
		this._x = a._x * b._x
		this._y = a._y * b._y
		this._z = a._z * b._z
        this.didChange()
		return this  
    }
 
    negate(): this { 
		this._x = - this._x
		this._y = - this._y
		this._z = - this._z
        this.didChange()
		return this
    }
 
    normalize(): this { 
        return this.multiplyScalar( 1 / this.length() || 1 )
    }
 
    projectOnPlane(planeNormal: Vector3): this { 
		_vector.copy(this).projectOnVector(planeNormal)
		return this.sub(_vector)
    }
 
    projectOnVector(v: Vector3): this { 
		const denominator = v.lengthSq()
		if (denominator === 0) 
            return this.set(0, 0, 0)
		const scalar = v.dot(this) / denominator
		return this.copy(v).multiplyScalar(scalar)
    }
 
    reflect(normal: Vector3): this { 
        return this.sub(_vector.copy(normal).multiplyScalar(2 * this.dot(normal)))
    }
 
    set(x: number, y: number, z: number): this { 
        this._x = x
        this._y = y
        this._z = z
        this.didChange()
        return this
    }
 
    setLength(l: number): this { return this.normalize().multiplyScalar(l) }
 
    sub(v: Vector3): this { 
		this._x -= v._x
		this._y -= v._y
		this._z -= v._z
        this.didChange()
		return this
    }
 
    subVectors(a: Vector3, b: Vector3): this { 
		this._x = a._x - b._x
		this._y = a._y - b._y
		this._z = a._z - b._z
        this.didChange()
		return this  
    }
}


/*
//  TODO rename to RefVector3? 
class ReplicatedVector3 extends Vector3 { 
    static fromArray(array: number[]): ReplicatedVector3 { 
        if (array.length !== 3)
            throw "array.length !== 3"
        return new ReplicatedVector3(array[0], array[1], array[2])
    }

    syncToNative: (() => void) | null = null
    private syncToNative_() { 
        if (this.syncToNative === null) 
            return
        this.syncToNative()
    }
    
    get x() { return this.x_ }
    set x(value: number) { this.x_ = value; this.syncToNative_() }
    get y() { return this.y_ }
    set y(value: number) { this.y_ = value; this.syncToNative_() }
    get z() { return this.z_ }
    set z(value: number) { this.z_ = value; this.syncToNative_() }
}
*/

import { Quaternion } from "./quaternion-ios.js"

let _quaternion: Quaternion
let _vector: Vector3

function initHelperObjects() { 
    //  with es2015 modules we can't just do `let _quaternion = new Quaternion()`. 
    //  even though the Quaternion import is down here, with es2015 modules the loader
    //  pre-parses the script for imports, builds an import tree, 
    //  then tries to load and execute them one by one, 
    //  resulting in errors like "Cannot access 'Vector3' before initialization"
    //  due to the Vector - Quaternion circular dependencies. 
    //  this is resolved by delaying the creation of these instances. 
    if (_quaternion === undefined)
        _quaternion = new Quaternion()
    if (_vector === undefined) 
        _vector = new Vector3()
}
