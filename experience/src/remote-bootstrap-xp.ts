

import { init } from "arsdk/ios/remote/remote-bootstrap"

async function importMain() { 
    return import("./main")
    .catch(reason => { console.log(reason) })
    .then(() => console.log("import.then"))
}

try {
    init(importMain)
} catch(x) { 
    console.log(x)
    console.log(x)
}
