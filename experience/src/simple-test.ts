
import * as arsdk from 'arsdk'
import { print } from 'arsdk/ios/arsdk-ios-raw'

var tapCount = 0

try {
    arsdk.experience.addObjectScript(
        "baymax1", 
        (object: arsdk.Entity) => { 
            console.log("object placed")
            // object.opacity = 0.5
            //  object.playAnimation(object.getAnimationNamed()[0])
            object.addComponent(
                new arsdk.OnTap(
                    hitWorldPos => { 
                        console.log("object tapped")
                        tapCount++
                        // const animNames = object.getAnimationNames()
                        // const animName = animNames[tapCount % animNames.length]
                        // console.log(animName)
                        // object.playAnimation(animName)
                    }))
        })            
} catch(x) {
    print("exception:")
    print(x)
}

