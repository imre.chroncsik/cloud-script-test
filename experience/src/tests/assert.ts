import { Color, experience, Quaternion, Vector3 } from "arsdk";

export function assert(condition: Boolean, message?: string): asserts condition {
  if (!condition) throw new Error("assert failed " + (message ?? ""));
}

export function assertNull(actual: any, message?: string) {
  if (actual != null) throw new Error("assertNull failed: Expected null, actual value: " + actual + " " + (message ?? ""));
}

export function assertNotNull(actual: any, message?: string) {
  if (actual == null) throw new Error("assertNotNull failed: Expected not null: " + (message ?? ""));
}

export function assertEqual(actual: any, expected: any, message?: string) {
  if (actual != expected) throw new Error("assertEqual failed: Expected " + expected + " actual value: " + actual + " " + (message ?? ""));
}

export function assertStrictEqual(actual: any, expected: any, message?: string) {
  if (actual !== expected)
    throw new Error("assertStrictEqual failed: Expected " + expected + " actual value: " + actual + " " + (message ?? ""));
}

export function assertNotEqual(actual: any, notExpected: any, message?: string) {
  if (actual == notExpected) throw new Error("assertNotEqual failed: Did not expect " + actual + " " + (message ?? ""));
}

export function assertNotStrictEqual(actual: any, notExpected: any, message?: string) {
  if (actual === notExpected)
    throw new Error("assertNotStrictEqual failed: Did not expect " + notExpected + " actual value: " + actual + " " + (message ?? ""));
}

const EPSILON = 0.00001;
export function assertFloatAlmostEqual(actual: number, expected: number, message?: string) {
  if (Math.abs(actual - expected) > EPSILON)
    throw new Error("assertFloatAlmostEqual failed: Expected " + expected + " actual value: " + actual + " " + (message ?? ""));
}

export function assertVectorsAlmostEqual(actual: Vector3, expected: Vector3, message?: string) {
  if (!vectorsAlmostEqual(actual, expected))
    throw new Error("assertVectorsAlmostEqual failed: Expected " + expected + " actual value: " + actual + " " + (message ?? ""));
}

export function assertVectorsEqual(actual: Vector3, expected: Vector3, message?: string) {
  if (!actual.equals(expected))
    throw new Error("assertVectorsEqual failed: Expected " + expected + " actual value: " + actual + " " + (message ?? ""));
}

export function assertQuaternionsAlmostEqual(actual: Quaternion, expected: Quaternion, message?: string) {
  if (!quaternionsAlmostEqual(actual, expected))
    throw new Error("assertQuaternionsAlmostEqual failed: Expected " + expected + " actual value: " + actual + " " + (message ?? ""));
}

export function assertQuaternionsEqual(actual: Quaternion, expected: Quaternion, message?: string) {
  if (!actual.equals(expected))
    throw new Error("assertVectorsEqual failed: Expected " + expected + " actual value: " + actual + " " + (message ?? ""));
}

export function vectorsAlmostEqual(actual: Vector3, expected: Vector3): Boolean {
  return !(
    Math.abs(actual.x - expected.x) > EPSILON ||
    Math.abs(actual.y - expected.y) > EPSILON ||
    Math.abs(actual.z - expected.z) > EPSILON
  );
}
export function quaternionsAlmostEqual(actual: Quaternion, expected: Quaternion): Boolean {
  return !(
    Math.abs(actual.x - expected.x) > EPSILON ||
    Math.abs(actual.y - expected.y) > EPSILON ||
    Math.abs(actual.z - expected.z) > EPSILON ||
    Math.abs(actual.w - expected.w) > EPSILON
  );
}
export function assertIsNaN(actual: any, message?: string) {
  if (!isNaN(actual)) throw new Error("assertIsNaN failed: Expected NaN, actual value: " + actual + " " + (message ?? ""));
}

export function assertColorsEqual(actual: Color, expected: Color, message?: string) {
  if (!colorsEqual(actual, expected))
    throw new Error("assertColorsEqual failed: Expected " + expected + " actual value: " + actual + " " + (message ?? ""));
}
export function assertColorsAlmostEqual(actual: Color, expected: Color, message?: string) {
  if (!colorsAlmostEqual(actual, expected))
    throw new Error("assertColorsAlmostEqual failed: Expected " + expected + " actual value: " + actual + " " + (message ?? ""));
}

function colorsAlmostEqual(actual: Color, expected: Color): Boolean {
  return !(
    Math.abs(actual.r - expected.r) > EPSILON ||
    Math.abs(actual.g - expected.g) > EPSILON ||
    Math.abs(actual.b - expected.b) > EPSILON ||
    Math.abs(actual.a - expected.a) > EPSILON
  );
}

function colorsEqual(actual: Color, expected: Color): Boolean {
  return actual.r == expected.r && actual.g == expected.g && actual.b == expected.b && actual.a == expected.a;
}

export function assertThrows(fun: () => void, message?: string) {
  try {
    fun();
  } catch {
    return;
  }
  throw new Error("assertThrows failed: Function did not throw error: " + message ?? "");
}