
import * as arsdk from "arsdk"

const OBJECT_UID = "leopard";

arsdk.experience.addObjectScript(OBJECT_UID, didCreateObject)

function didCreateObject(rootEntity: arsdk.Entity) { 
    console.log("didCreateObject")
    const audioSource = new arsdk.AudioSource()
    rootEntity.addComponent(audioSource)
    audioSource.setClip("synth")

    audioSource.play()

    // const onTap = (hitPointWorld: arsdk.Vector3) => { audioSource.play() }
    // const onTapComponent = new arsdk.OnTap(onTap)
    // rootEntity.addComponent(onTapComponent)
}
