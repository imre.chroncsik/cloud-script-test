import { AudioSource, Entity, experience, OnTap, System } from "arsdk";
import { assert, assertEqual, assertThrows, assertFloatAlmostEqual } from "./assert";
import { CUBE_SCENE_EXPERIENCE_UID, CUBE_SCENE_OBJECT_UID } from "./cube_scene";

import * as arsdk_ios_raw from "arsdk/ios/arsdk-ios-raw"

/**
 * Test experience
 * Composer: https://short.staging.immersive.verizonmedia.com/aIb8H
 * Android: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/android/index.json
 * iOS: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/ios/index.json
 * Web: https://cdn.launch3d.com/cu/xr/preview-stable/index.html?customJsonPath=https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/web/index.json
 */
// assertEqual(experience.uid, CUBE_SCENE_EXPERIENCE_UID, "Wrong experience uid");

const ICQ_SOUND_ID = "2fd468f4-8e6a-4757-af1d-d07d5b9e1e57";
const FAITH_SOUND_ID = "eeeb5063-1339-4123-8326-2a94e75fab23";
const ICQ_DURATION = 1.019;
const FAITH_DURATION = 3.563999891281128;

const tests = new Map<string, Boolean>();
const totalNumberOfTests = 9;
function testComplete(key: string) {
  if (tests.get(key)) {
    return; //already completed
  }
  tests.set(key, true);
  let completedCount = 0;
  tests.forEach((element) => {
    if (element) {
      completedCount++;
    }
  });
  console.log(key + " [✓] (" + completedCount + "/" + totalNumberOfTests + ")");
  if (completedCount == totalNumberOfTests) {
    console.log("All tests succeeded! :)");
  }
}

function testPlayWithoutClip(rootEntity: Entity) {
  let audioSource2 = new AudioSource();
  rootEntity.addComponent(audioSource2);
  assertThrows(() => {
    audioSource2.play();
  }, "No clip set");
  rootEntity.removeComponent(audioSource2);
}

//cube scene
experience.addObjectScript(CUBE_SCENE_OBJECT_UID, (rootEntity) => {
  let audiosource = new AudioSource();
  // audiosource.setClip(FAITH_SOUND_ID);
  //attach to camera. AudioSources attached to the camera are never attenuated by distance.
  if (experience.currentMode.isAR) {
    experience.currentMode.camera.addComponent(audiosource);
  } else {
    experience.currentMode.camera.entity.addComponent(audiosource);
  }
  audiosource.setClip(FAITH_SOUND_ID);
  const sphere = rootEntity.getChild(0)?.getChild(1)?.getChild(4)
  console.log(sphere?.name)
  // rootEntity.getChild(0)!.addComponent(
  sphere?.addComponent(
    new OnTap(() => {
      if (audiosource.isPlaying()) {
        audiosource.stop();
        testComplete("stop");
      } else {
        audiosource.play();
      }
    })
  );
});

class TestSystem extends System {
  entity: Entity | null = null;

  setEntity(entity: Entity | null) {
    this.entity = entity;
  }

  update(dt: number): void {
    if (!this.entity) {
      return;
    }
    let audiosource = this.entity!.getComponent(AudioSource)!;
    assert(audiosource.getCurrentProgress() >= 0 && audiosource.getCurrentProgress() <= 1);
    assert(audiosource.getCurrentTime() >= 0 && audiosource.getCurrentTime() <= FAITH_DURATION * 1.001);
    assertFloatAlmostEqual(audiosource.getDuration(), FAITH_DURATION);

    if (audiosource.getCurrentProgress() > 0.9) {
      testComplete("getCurrentProgress");
    }
    if (audiosource.getCurrentTime() > 0.9 * FAITH_DURATION) {
      testComplete("getCurrentTime");
    }
  }
}

const testSystem = new TestSystem();
experience.addSystem(testSystem);

//duck 1
experience.addObjectScript("085c661a-040f-4385-974d-90dfab565906", (rootEntity) => {
  testPlayWithoutClip(rootEntity);
  assertThrows(() => {
    new AudioSource().getDuration();
  }, "No clip set");
  let audiosource = new AudioSource();
  assertEqual(audiosource.volume, 1);
  assertEqual(audiosource.loop, false);
  assertEqual(audiosource.isPlaying(), false);
  audiosource.setClip(ICQ_SOUND_ID);

  assertThrows(() => {
    new AudioSource().play();
  }, "Not attached to entity");
  // assertFloatAlmostEqual(audiosource.getDuration(), ICQ_DURATION, "ICQ duration incorrect");
  assertEqual(audiosource.getCurrentProgress(), 0);
  assertEqual(audiosource.getCurrentTime(), 0);

  rootEntity.addComponent(audiosource);
  audiosource.play();

  const duck = rootEntity.getChild(0)?.getChild(0)?.getChild(0)
  console.log(duck?.name)
  console.log(duck?.transform.localPosition)
  console.log(duck?.transform.localScale)
  const ecsDuck = (duck as unknown) as arsdk_ios_raw.ECSEntity
  console.log("xyz")
  const mesh = ecsDuck.getComponentsByClassName("ECSMesh")[0]
  // console.log(mesh.objectId)

  duck?.addComponent(
    new OnTap(() => {
      audiosource.setClip(FAITH_SOUND_ID);
      audiosource.play();
      assertEqual(audiosource.isPlaying(), true);
      rootEntity.removeComponent(audiosource);
      assertEqual(audiosource.isPlaying(), false, "Removing component should stop audio");
      rootEntity.addComponent(audiosource);
      assertEqual(audiosource.isPlaying(), false, "Audio should not automatically resume when attached");
      testComplete("change clip, remove component");
    })
  );
  testComplete("test 1");

  //TODO attach to camera
});

//duck 2
experience.addObjectScript(
  "292b7f18-8873-4132-ae66-90539e31260c",
  (rootEntity) => {
    testSystem.setEntity(rootEntity);
    let audioSource = new AudioSource();
    audioSource.setClip(FAITH_SOUND_ID);
    rootEntity.addComponent(audioSource);

    // let wasPaused = false;
    // let duck2 = rootEntity.getChild(0)?.getChild(0)?.getChild(0)
    // duck2?.addComponent(
    //   new OnTap(() => {
    //     if (audioSource.isPlaying()) {
    //       // console.log("pause")
    //       // audioSource.pause();
    //       // // audioSource.loop = true;
    //       // wasPaused = true;
    //       // testComplete("looping sound");

    //       let wasLooping = audioSource.loop
    //       console.log("set loop ", wasLooping, " -> ", !wasLooping)
    //       audioSource.loop = !wasLooping
    //     } else {
    //       // if (audioSource.volume == 1.0) {
    //       //   audioSource.volume = 0.3;
    //       //   assertFloatAlmostEqual(audioSource.volume, 0.3)
    //       //   // assertEqual(audioSource.volume, 0.3);
    //       //   testComplete("volume 0.3");
    //       // } else {
    //         console.log("play")
    //         audioSource.volume = 1.0;

    //         audioSource.loop = true

    //         assertFloatAlmostEqual(audioSource.volume, 1.0)
    //         // assertEqual(audioSource.volume, 1.0);
    //         testComplete("volume 1.0");
    //       // }
    //       audioSource.play();
    //       if (wasPaused) testComplete("resume sound");
    //     }
    //   })
    // );

    let wasPaused = false;
    let duck2 = rootEntity.getChild(0)?.getChild(0)?.getChild(0)
    duck2?.addComponent(
      new OnTap(() => {
        if (audioSource.isPlaying()) {
          audioSource.pause();
          audioSource.loop = true;
          wasPaused = true;
          testComplete("looping sound");
        } else {
          if (audioSource.volume == 1.0) {
            audioSource.volume = 0.3;
            assertFloatAlmostEqual(audioSource.volume, 0.3);
            testComplete("volume 0.3");
          } else {
            audioSource.volume = 1.0;
            assertFloatAlmostEqual(audioSource.volume, 1.0);
            testComplete("volume 1.0");
          }
          audioSource.play();
          if (wasPaused) testComplete("resume sound");
        }
      })
    );    
  },
  () => {
    testSystem.setEntity(null);
  }
);