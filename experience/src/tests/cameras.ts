import {
    ARMode,
    Entity,
    experience,
    OrbitCameraController,
    PreviewCamera,
    PreviewMode,
    Quaternion,
    System,
    Transform,
    Vector3,
  } from "arsdk";
  import {
    assert,
    assertEqual,
    assertNotStrictEqual,
    assertNull,
    assertStrictEqual,
    assertThrows,
    assertVectorsAlmostEqual,
    vectorsAlmostEqual,
  } from "./assert";
//   import { CUBE_SCENE_OBJECT_UID } from "../libs/cube_scene";
const CUBE_SCENE_OBJECT_UID = "dd53e085-951a-4f55-8c27-b4f51da5dc1a";
const CUBE_SCENE_EXPERIENCE_UID = "dca941bb-099a-44d0-a2ce-510cc3f0afff";
import { print } from "arsdk/ios/arsdk-ios-raw"
  
  /**
   * Test experience
   * Composer: https://short.staging.immersive.verizonmedia.com/aIb8H
   * Android: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/android/index.json
   * iOS: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/ios/index.json
   * Web: https://cdn.launch3d.com/cu/xr/preview-stable/index.html?customJsonPath=https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/web/index.json
   */
  
  const tests = new Map<string, Boolean>();
  const totalNumberOfTests = 5;
  function testComplete(key: string) {
    if (tests.get(key)) {
      return; //already completed
    }
    tests.set(key, true);
    let completedCount = 0;
    tests.forEach((element) => {
      if (element) {
        completedCount++;
      }
    });
    // console.log(key + " [✓] (" + completedCount + "/" + totalNumberOfTests + ")");
    print(key + " [✓] (" + completedCount + "/" + totalNumberOfTests + ")");
    if (completedCount == totalNumberOfTests) {
      // console.log("All tests succeeded! :)");
      print("All tests succeeded! :)");
    }
  }
  
  //hold onto camera entity. it should be destroyed on mode switch
  let cameraEntity: Entity | null = null;
  let previewModeCamera: PreviewCamera | null = null;
  
  experience.addOnModeChangedListener((currentMode: ARMode | PreviewMode) => {
    // if (cameraEntity) {
    //   assertNotStrictEqual(cameraEntity, currentMode.camera, "Camera entity was not recreated");
    //   assertThrows(() => {
    //     cameraEntity!.getComponent(Transform);
    //   }, "Camera entity should be destroyed after mode switch");
    //   testComplete("AR Camera destroyed");
    // }
  
    // if (previewModeCamera) {
    //   assertNotStrictEqual(previewModeCamera, currentMode.camera, "Camera entity was not recreated");
    //   assertThrows(() => {
    //     previewModeCamera!.entity!.getComponent(Transform);
    //   }, "PreviewCamera should be destroyed after mode switch");
    //   testComplete("PreviewCamera destroyed");
    // }
  
    if (currentMode.isAR) {
      cameraEntity = currentMode.camera;
      cameraEntity.getComponent(Transform);
    } else {
      previewModeCamera = currentMode.camera;
      previewModeCamera.entity.getComponent(Transform);
    }
  });
  
  if (experience.currentMode.isAR) {
    cameraEntity = experience.currentMode.camera;
  } else {
    previewModeCamera = experience.currentMode.camera;
  }
  
  class TestSystem extends System {
    update(dt: number): void {
      // if (!experience.currentMode.isAR) {
      //   const orbitCameraController: OrbitCameraController = experience.currentMode.camera.orbitCameraController
      //   const pp: Vector3 = orbitCameraController.pivotPosition
      //   const cp = experience.currentMode.camera.orbitCameraController.cameraPosition
      //   // if (
      //   //   vectorsAlmostEqual(experience.currentMode.camera.orbitCameraController.pivotPosition, new Vector3(0, 0, 0)) &&
      //   //   vectorsAlmostEqual(experience.currentMode.camera.orbitCameraController.cameraPosition, new Vector3(2, 2, 2))
      //   // ) {
      //   if (
      //     vectorsAlmostEqual(pp, new Vector3(0, 0, 0)) &&
      //     vectorsAlmostEqual(cp, new Vector3(2, 2, 2))
      //   ) {
      //     testComplete("orbitCameraController.animateTo");
      //   }
      // }
    }
  }
  
  experience.addSystem(new TestSystem());
  
  if (!experience.currentMode.isAR) {
    //No object placed yet. The camera position should be zero.
    assertVectorsAlmostEqual(experience.currentMode.camera.orbitCameraController.pivotPosition, new Vector3(0, 0, 0));
    assertVectorsAlmostEqual(experience.currentMode.camera.orbitCameraController.cameraPosition, new Vector3(0, 0, 0));
  }
  
  experience.addObjectScript(CUBE_SCENE_OBJECT_UID, (rootEntity) => {
    //TODO: Composer currently inserts one extra node. Can we remove this?
    rootEntity = rootEntity.getChild(0)!;
  
    if (!experience.currentMode.isAR) {
      const previewModeCamera = experience.currentMode.camera;
      assertNull(previewModeCamera.transform.parent, "Camera parent is not null");
      assertEqual(previewModeCamera.entity, previewModeCamera.transform.entity);
      
      assertEqual(previewModeCamera.transform.isReadOnly, false);
  
      assertVectorsAlmostEqual(
        previewModeCamera.orbitCameraController.pivotPosition,
        new Vector3(-1.4322222, 0.99999994, -0.20388487),
        "Initial camera orbit pivot incorrect"
      );
      assertVectorsAlmostEqual(
        previewModeCamera.orbitCameraController.cameraPosition,
        new Vector3(-9.669805, 7.5098166, 19.053514),
        "Initial camera orbit position incorrect"
      );
      // assertStrictEqual(
      //   previewModeCamera.orbitCameraController.cameraPosition,
      //   previewModeCamera.entity.transform.worldPosition,
      //   "cameraPosition should be shortcut for entity.transform.worldPosition"
      // );
      assert(previewModeCamera.orbitCameraController.cameraPosition.equals(
        previewModeCamera.entity.transform.worldPosition), 
        "cameraPosition should be shortcut for entity.transform.worldPosition")
  
      previewModeCamera.orbitCameraController.animateTo(new Vector3(0, 0, 0), new Vector3(2, 2, 2), 3);
  
      assertNull(
        previewModeCamera.entity.removeComponent(previewModeCamera.orbitCameraController),
        "The OrbitCameraController can not be removed"
      );
  
      testComplete("Object placed in 3D");
    } else {
      const arCamera = experience.currentMode.camera;
      assertNull(arCamera.parent, "Camera parent is not null");

      //  TODO re-enable once isReadOnly is implemented
      // assertEqual(arCamera.transform.isReadOnly, true);
  
      console.log("ar camera position " + arCamera.transform.worldPosition);
  
      // assertThrows(() => {
      //   arCamera.transform.worldPosition.set(0, 0, 0);
      // }, "AR camera transform is not read-only");
  
      // assertThrows(() => {
      //   arCamera.transform.localScale.setLength(1.0);
      // }, "AR camera transform is not read-only");
  
      // assertThrows(() => {
      //   arCamera.transform.localRotation.multiply(new Quaternion(0, 0, 0, 1));
      // }, "AR camera transform is not read-only");
  
      // assertThrows(() => {
      //   arCamera.transform.parent = rootEntity.transform;
      // }, "AR camera transform is not read-only");
  
      // assertThrows(() => {
      //   arCamera.parent = rootEntity;
      // }, "AR camera transform is not read-only");
  
      //should not throw
      arCamera.transform.localPosition.clone();
      new Vector3().copy(arCamera.transform.localPosition);
      arCamera.transform.localPosition.dot(new Vector3(0, 1, 0));
  
      testComplete("Object placed in AR");
    }
  });