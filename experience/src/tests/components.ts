import { 
    Component, Entity, experience, MeshRenderable, ObjectAnimator, OnTap, Transform } 
    from "arsdk";
import {
  assert,
  assertEqual,
  assertNotEqual,
  assertNotNull,
  assertNull,
  assertThrows,
  assertNotStrictEqual,
  assertStrictEqual,
} from "./assert.js";

/**
 * Test experience
 * Composer: https://short.staging.immersive.verizonmedia.com/aIb8H
 * Android: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/android/index.json
 * iOS: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/ios/index.json
 * Web: https://cdn.launch3d.com/cu/xr/preview-stable/index.html?customJsonPath=https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/web/index.json
 */

class CustomComponent extends Component {}

//  TODO ObjectAnimator
let testableComponents = [Transform, MeshRenderable/*, ObjectAnimator*/, OnTap];

experience.addObjectScript("c4189b56-e181-4ed5-be91-419e23b6c528", (rootEntity) => {
  assertEqual(rootEntity.getComponents(Transform).length, 1, "Entity must have one Transform");
  assertNull(rootEntity.removeComponent(rootEntity.getComponent(Transform)!), "Removing the Transform component is not allowed");
  assertNull(rootEntity.addComponent(new Transform()), "Adding two Transform components is not allowed");
  assertStrictEqual(rootEntity.transform, rootEntity.getComponent(Transform));

  assertThrows(() => {
    rootEntity.getComponent(Transform)!.parent = new Transform();
  }, "Can not set parent to a transform that is not assigned to an entity");

  //check if "entity" access is possible for each component
  testableComponents.forEach((componentClass) => {
    experience.forEachComponent(componentClass, (component) => {
      assertNotNull(component.entity, "component.entity is null");
    });
  });

  let customComponent = new CustomComponent();
  rootEntity.addComponent(customComponent);
  assertNotNull(rootEntity.getComponent(CustomComponent), "Custom component was not added");

  const result = rootEntity.getChild(0)!.addComponent(customComponent);
  assertNull(result, "Component is already attached to an entity, attaching it to another entity should fail");
  assertEqual(customComponent.entity, rootEntity);

  //two components of the same type can be added to an entity if the component allows it
  assertNotNull(rootEntity.addComponent(new CustomComponent()));
  const customComponents = rootEntity.getComponents(CustomComponent);
  assertEqual(customComponents.length, 2, "Two components of the same type can be added to an entity if the component allows it");
  assertNotStrictEqual(customComponents[0], customComponents[1]);
  assertNotStrictEqual(
    rootEntity.getComponents(CustomComponent),
    rootEntity.getComponents(CustomComponent),
    "getComponents should return a copy, not a reference"
  );
  assertNotNull(rootEntity.removeComponent(customComponents[0]));
  assertEqual(rootEntity.getComponents(CustomComponent).length, 1, "Invalid amounts of components");
  assertStrictEqual(rootEntity.getComponent(CustomComponent), customComponents[1]);
  assertNotNull(rootEntity.removeComponent(customComponents[1]));
  assertNull(rootEntity.getComponent(CustomComponent));
  
  //check creating a new entity from script
  let entity = new Entity();
  assertEqual(entity.getComponents(Transform).length, 1, "Entity must have one Transform");
  assertEqual(entity.getComponents(MeshRenderable).length, 0, "Entity should not have MeshRenderable Component");
  assertEqual(entity.getComponents(OnTap).length, 0, "Entity should not have MeshRenderable Component");
  //  TODO ObjectAnimator
  // assertEqual(entity.getComponents(ObjectAnimator).length, 0, "Entity should not have ObjectAnimator Component");
  assertEqual(entity.name, "Entity");

  //TODO test adding all built-in components

  //@ts-ignore
  console.log("All tests succeeded! :)");
});
