import {
    Component,
    Entity,
    experience,
    Material,
    MeshRenderable,
    Quaternion,
    System,
    Transform,
    Vector3,
    ObjectAnimator,
    Color,
    OnTap
    } from "arsdk";
  
  class RigidBody extends Component {
    velocity = new Vector3(0, 0, 0);
    initialYPos: number = 0;
  }
  
  class PhysicsSystem extends System {
    scaledVelocity = new Vector3(); //create working data here to avoid allocating memory every frame
    rigidBodyEntities: Array<Entity> | null = null;
  
    update(dt: number): void {
      if (dt === 0)
        return

      let step = dt / 0.016;
  
      //integrate physics
      if (this.rigidBodyEntities) {
        for (let i = 0; i < this.rigidBodyEntities.length; i++) {
          let rigidBodyEntity = this.rigidBodyEntities[i];
          let rigidBody = rigidBodyEntity.getComponent(RigidBody)!;
          let transform = rigidBodyEntity.getComponent(Transform)!;
  
          let worldPos = transform.worldPosition;
          if (!rigidBody.initialYPos) {
            rigidBody.initialYPos = worldPos.y;
          }
  
          this.scaledVelocity.copy(rigidBody.velocity);
          this.scaledVelocity.multiplyScalar(step);
          worldPos.add(this.scaledVelocity);
  
          rigidBody.velocity.y -= 0.01 * step;
  
          //"collide" with ground plane
          if (worldPos.y < rigidBody.initialYPos) {
            rigidBody.velocity.y = 0;
            worldPos.y = rigidBody.initialYPos;
          }
  
          //friction on ground plane
          if (worldPos.y == rigidBody.initialYPos) {
            rigidBody.velocity.multiplyScalar(0.7 / step);
            if (isNaN(rigidBody.velocity.x)) { 
              console.log("nan")
            }
          }
        }
      }
    }
  
    setRigidbodies(rigidBodies: Array<Entity>) {
      this.rigidBodyEntities = rigidBodies;
    }
  }
  
  let physicsSystem = new PhysicsSystem();
  experience.addSystem(physicsSystem);
  
  experience.addObjectScript("c4189b56-e181-4ed5-be91-419e23b6c528", (rootEntity) => {
    // let animation = rootEntity.getComponent(ObjectAnimator)!.getAnimation("Anim_0");
    // // animation.setRepeats(-1); //loop
    // animation.start();
  
    //TODO: Composer currently inserts one extra node. Will be removed.
    //rootEntity -> gltf root -> extra composer node
    rootEntity = rootEntity.getChild(0)!.getChild(0)!;
  
    let cubesParentTransform = rootEntity.getComponent(Transform)!.getChild(0)!;
    let cubeCount = cubesParentTransform.childCount();
    // let cubeCount = Math.min(4, cubesParentTransform.childCount());
    let rigidBodyEntities = Array<Entity>(cubeCount);
    for (let i = 0; i < cubeCount; i++) {
      let rigidBody = new RigidBody();
      rigidBodyEntities[i] = cubesParentTransform.getChild(i)!.entity!;
      rigidBodyEntities[i].addComponent(rigidBody);
  
      //set cube to random color
    //   rigidBodyEntities[i].getComponent(MeshRenderable)!.material.color = new Color(
    //     Math.random(),
    //     Math.random(),
    //     Math.random(),
    //     Math.random()
    //   );
    }
  
    let sphereEntity = rootEntity.getComponent(Transform)!.getChild(1)!.getChild(2)!.entity!;
    let rigidBody = new RigidBody();
    sphereEntity.addComponent(rigidBody);
  
    rigidBodyEntities.push(sphereEntity);
    physicsSystem.setRigidbodies(rigidBodyEntities);
  
    sphereEntity.addComponent(
      new OnTap((tapWorldPosition) => {
        rigidBody.velocity = new Vector3(0, 0.3, 0);
      })
    );
  
    let planeEntity = rootEntity.getComponent(Transform)!.getChild(1)!.getChild(1)!.entity!;
  
    planeEntity.addComponent(
      new OnTap((tapWorldPosition) => {
        for (let i = 0; i < cubeCount; i++) {
          let cubeWorldPos = rigidBodyEntities[i].getComponent(Transform)!.worldPosition;
  
          let dir = cubeWorldPos.clone();
          dir.y += 0.1;
          dir.sub(tapWorldPosition);
          let dist = dir.length();
  
          if (dist < 0.5) {
            dir.setLength(0.1);
            rigidBodyEntities[i].getComponent(RigidBody)!.velocity = dir;
          }
        }
      })
    );
  });