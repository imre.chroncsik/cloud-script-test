import { Quaternion, Vector3 } from "arsdk";
import { assertEqual, assertIsNaN, assertQuaternionsAlmostEqual, assertQuaternionsEqual, assertVectorsEqual } from "./assert.js";

assertVectorsEqual(new Vector3(), new Vector3(0, 0, 0), "Incorrect default values for Vector3");
assertVectorsEqual(new Vector3(1), new Vector3(1, 0, 0), "Incorrect default values for Vector3");
assertVectorsEqual(new Vector3(1, 2), new Vector3(1, 2, 0), "Incorrect default values for Vector3");
assertQuaternionsEqual(new Quaternion(), new Quaternion(0, 0, 0, 1), "Incorrect default values for Quaternion");
assertQuaternionsEqual(new Quaternion(1), new Quaternion(1, 0, 0, 1), "Incorrect default values for Quaternion");
assertQuaternionsEqual(new Quaternion(1, 2), new Quaternion(1, 2, 0, 1), "Incorrect default values for Quaternion");
assertQuaternionsEqual(new Quaternion(1, 2, 3), new Quaternion(1, 2, 3, 1), "Incorrect default values for Quaternion");
// assertQuaternionsEqual(
//   new Quaternion(Number.NaN, Number.NaN, Number.NaN, Number.NaN),
//   new Quaternion(),
//   "Incorrect default values for Quaternion"
// );

let q = new Quaternion(1, 2, 3, 4);
//check that quaternion is not normalized automatically
assertEqual(q.x, 1);
assertEqual(q.y, 2);
assertEqual(q.z, 3);
assertEqual(q.w, 4);

assertQuaternionsAlmostEqual(q.normalize(), new Quaternion(0.18257418, 0.36514837, 0.5477226, 0.73029673));
q.set(2, 3, 4, 5);
assertQuaternionsEqual(q, new Quaternion(2, 3, 4, 5), "Quaternion.set error");

q.x = Number.NaN;
q.normalize();
assertIsNaN(q.x);
assertIsNaN(q.y);
assertIsNaN(q.z);
assertIsNaN(q.w);

assertQuaternionsEqual(new Quaternion(5).identity(), new Quaternion(0, 0, 0, 1), "Quaternion.identity");

//check euler angles are applied in correct order
assertQuaternionsAlmostEqual(
  new Quaternion().setEulerAngles(new Vector3(10, 20, 30)),

  //    the first line is Fabian's original values, 
  //    the second line is from https://www.andre-gaschler.com/rotationconverter/, 
  //    the latter seems to be consistent with my code. 
//   new Quaternion(0.12767944, 0.14487813, 0.23929833, 0.9515485),
  new Quaternion(0.0381346, 0.1893079, 0.2392983, 0.9515485), 

  "setEulerAngles"
);

//TODO add more tests

console.log("All tests completed!");