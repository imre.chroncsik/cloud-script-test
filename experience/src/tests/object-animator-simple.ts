
import * as arsdk from "arsdk"

const OBJECT_UID = "9b793829-cd68-4f2d-8f0e-85dddb009ff6";

arsdk.experience.addObjectScript(OBJECT_UID, didCreateObject)

function didCreateObject(rootEntity: arsdk.Entity) {     
    let animator = rootEntity.getComponent(arsdk.ObjectAnimator)
    if (animator === null)
        throw "animator === null"
    
    let lt = animator.getAnimation("Linear Translation")
    lt.layer = 1
    lt.start()
    let st = animator.getAnimation("Step Translation")
    st.layer = 2
    st.start()
    let ct = animator.getAnimation("CubicSpline Translation")
    ct.layer = 3
    ct.start()

    let lr = animator.getAnimation("Linear Rotation")
    lr.layer = 4
    lr.start()
    let sr = animator.getAnimation("Step Rotation")
    sr.layer = 5
    sr.start()
    let cr = animator.getAnimation("CubicSpline Rotation")
    cr.layer = 6
    cr.start()

    let ls = animator.getAnimation("Linear Scale")
    ls.layer = 7
    ls.start()
    let ss = animator.getAnimation("Step Scale")
    ss.layer = 8
    ss.start()
    let cs = animator.getAnimation("CubicSpline Scale")
    cs.layer = 9
    cs.start()

    let allAnimations = animator.getAnimations()
}
