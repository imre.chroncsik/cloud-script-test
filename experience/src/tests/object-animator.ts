import * as arsdk from "arsdk"
import { Animation, Entity, experience, MeshRenderable, ObjectAnimator, OnTap, System } from "arsdk";
import { assert, assertEqual, assertFloatAlmostEqual, assertThrows } from "./assert";

/**
 * Test experience
 * Composer: https://short.staging.immersive.verizonmedia.com/EReER
 * Android: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/fa3f5772-6522-475f-9f90-fbaf20a6a5cb/v1/android/index.json
 * iOS: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/fa3f5772-6522-475f-9f90-fbaf20a6a5cb/v1/ios/index.json
 * Web: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/fa3f5772-6522-475f-9f90-fbaf20a6a5cb/v1/web/index.json
 * Video: https://www.youtube.com/watch?v=xUE3l_ATGPU
 */

// assertEqual(experience.uid, "fa3f5772-6522-475f-9f90-fbaf20a6a5cb", "Wrong experience uid");

class TestSystem extends System {
  entity: Entity | null = null;

  setEntity(entity: Entity | null) {
    this.entity = entity;
  }

  update(dt: number): void {
    if (!this.entity) {
      return;
    }

    let animations = this.entity.getComponent(ObjectAnimator)!.getAnimations();
    let set = new Set();

    for (let anim of animations) {
      if (anim.isRunning()) {
        assert(!set.has(anim.layer), "Maximum one animation can play at the same time for one layer");
        set.add(anim.layer);
        assert(anim.getCurrentProgress() >= 0 && anim.getCurrentProgress() <= 1);
      }
    }
  }
}

let testSystem = new TestSystem();
experience.addSystem(testSystem);

let cubeSceneRootAnimator: ObjectAnimator | null = null;
experience.addObjectScript(
  "9b793829-cd68-4f2d-8f0e-85dddb009ff6",
  (rootEntity) => {
    testSystem.setEntity(rootEntity);

    const animator = rootEntity.getComponent(ObjectAnimator)!;
    cubeSceneRootAnimator = animator;
    let animations = animator.getAnimations();
    assertEqual(animations.length, 9, "Incorrect number of animations");

    let linearTranslationAnimation = animator!.getAnimation("Linear Translation");
    assertEqual(linearTranslationAnimation.isRunning(), false);
    assertFloatAlmostEqual(linearTranslationAnimation.getDuration(), 1.6666666269302368);
    assertEqual(linearTranslationAnimation.name, "Linear Translation");
    linearTranslationAnimation.start();

    //tests for what happens if we delete or reparent entities of the subgraph
    let cube1BottomLeft = rootEntity.findDescendantInHierarchy((entity) => {
      return entity.name == "Cube.001";
    })!;
    cube1BottomLeft.getComponent(MeshRenderable)?.material.color.set(0, 1, 0, 1);
    cube1BottomLeft.parent = null;

    let cube3Middle = rootEntity.findDescendantInHierarchy((entity) => {
      return entity.name == "Cube.003";
    })!;
    cube3Middle.getComponent(MeshRenderable)?.material.color.set(1, 0, 0, 1);
    let cube4MiddleRight = rootEntity.findDescendantInHierarchy((entity) => {
      return entity.name == "Cube.004";
    })!;
    cube4MiddleRight.getComponent(MeshRenderable)?.material.color.set(0, 0, 1, 1);
    cube3Middle.parent = cube4MiddleRight;

    let cube8TopRight = rootEntity.findDescendantInHierarchy((entity) => {
      return entity.name == "Cube.008";
    })!;
    cube8TopRight.destroy();

    let tapCount = 0;
    console.log("childCount: ", rootEntity.getChild(0)?.childCount())

    const cubeCount = rootEntity.getChild(0)?.childCount() ?? 0
    for (let c = 0; c < cubeCount; ++c) { 
      const cube = rootEntity.getChild(0)?.getChild(c)
      const didTapCube_ = (hitPointWorld: arsdk.Vector3) => { 
        didTapCube(animations)
      }
      cube?.addComponent(new OnTap(didTapCube_))
    }

    // rootEntity.getChild(0)!.addComponent(
    //   new OnTap(() => {
    //     console.log("playing " + animations[tapCount].name);
    //     let anim = animations[tapCount];
    //     tapCount = (tapCount + 1) % animations.length;

    //     //layers to allow simultaneous playing of animations
    //     anim.layer = tapCount;

    //     if (anim.isRunning()) {
    //       anim.pause();
    //     } else {
    //       anim.start();
    //       anim.loop = true;
    //     }
    //   })
    // );
  },
  () => {
    testSystem.setEntity(null);

    let animations = cubeSceneRootAnimator!.getAnimations();
    for (let anim of animations) {
      assertEqual(anim.isRunning(), false, "Object was destroyed, all animations should be stopped");
    }
    animations[0].start();
  }
);

let tapCount = 0;

function didTapCube(animations: readonly Animation[]) { 
  console.log("playing " + animations[tapCount].name);
  let anim = animations[tapCount];
  tapCount = (tapCount + 1) % animations.length;

  //layers to allow simultaneous playing of animations
  anim.layer = tapCount;

  if (anim.isRunning()) {
    anim.pause();
  } else {
    anim.start();
    anim.loop = true;
  }
}

//Brainstem
//This System sets the position of a joint while the animation is playing
class JointPositionSetterSystem extends System {
  entity: Entity | null = null;

  setEntity(entity: Entity | null) {
    this.entity = entity;
  }

  update(dt: number): void {
    if (!this.entity) {
      return;
    }

    let entity = this.entity.getChild(0)!.getChild(0)!.getChild(0)!.getChild(0)!.getChild(0)!.getChild(0)!.getChild(0)!.getChild(1)!;
    entity.transform.worldPosition.set(1, 1, 0);
  }
}

let jointPositionSetterSystem = new JointPositionSetterSystem();
experience.addObjectScript(
  "ad183bef-1efc-4dc2-8cee-9508ebe3334f",
  (rootEntity) => {
    const animator = rootEntity.getComponent(ObjectAnimator)!;
    experience.addSystem(jointPositionSetterSystem);
    jointPositionSetterSystem.setEntity(rootEntity);

    //This animation is unnamed in GLTF. As per Composer/Web alignment, it will be automatically named Animation_1.
    //This should best be sanitized in a preprocessing step.
    assertEqual(animator.getAnimations()[0].name, "Animation_1");
    let animation = animator!.getAnimation("Animation_1");
    animation.start();

    rootEntity.getChild(0)!.addComponent(
      new OnTap(() => {
        animation.start();
      })
    );

    //check that scenegraph is correct

    let count = 0;
    rootEntity.forEachDescendantInHierarchy(() => {
      count++;
    });

    assertEqual(
      count,
      24,
      "Invalid number of entities in the hierarchy. Make sure joint nodes are included. See https://jira.vzbuilders.com/browse/XR-6196"
    );
  },
  () => {
    experience.removeSystem(jointPositionSetterSystem);
  }
);

//RobotAnimated
experience.addObjectScript("a2244dd0-0755-4018-a7af-88d0cf89a3d9", (rootEntity) => {
  const animator = rootEntity.getComponent(ObjectAnimator)!;
  let animations = animator.getAnimations();

  let tapCount = 0;
  let previousAnim: Animation | null = null;
  rootEntity.getChild(0)!.addComponent(
    new OnTap(() => {
      console.log("playing " + animations[tapCount].name);
      let anim = animations[tapCount];

      anim.layer = tapCount;
      if (tapCount != 0)
        assertThrows(() => {
          anim.start();
        }, "Can not play animation on same targets");
      anim.layer = 0;
      anim.start();
      if (previousAnim) assertEqual(previousAnim.isRunning(), false, "Animation on the same layer should have been stopped automatically");
      anim.loop = true;
      previousAnim = anim;
      tapCount = (tapCount + 1) % animations.length;
    })
  );
});