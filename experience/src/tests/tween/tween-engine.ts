/*
 * anime.js v3.2.1
 * (c) 2020 Julian Garnier
 * Released under the MIT license
 * animejs.com
 */

"use strict";

import { Component, experience, System } from "arsdk";

// Defaults

var defaultInstanceSettings = {
  update: null,
  begin: null,
  loopBegin: null,
  changeBegin: null,
  change: null,
  changeComplete: null,
  loopComplete: null,
  complete: null,
  loop: 1,
  direction: "normal",
  autoplay: true,
  timelineOffset: 0,
};

var defaultTweenSettings = {
  duration: 1000,
  delay: 0,
  endDelay: 0,
  easing: "easeOutElastic(1, .5)",
  round: 0,
};

var validTransforms = [
  "translateX",
  "translateY",
  "translateZ",
  "rotate",
  "rotateX",
  "rotateY",
  "rotateZ",
  "scale",
  "scaleX",
  "scaleY",
  "scaleZ",
  "skew",
  "skewX",
  "skewY",
  "perspective",
  "matrix",
  "matrix3d",
];

// Caching

var cache: any = {
  CSS: {},
  springs: {},
};

// Utils

function minMax(val: number, min: number, max: number) {
  return Math.min(Math.max(val, min), max);
}

function stringContains(str: string, text: string) {
  return str.indexOf(text) > -1;
}

function applyArguments(
  func: {
    (mX1: any, mY1: any, mX2: any, mY2: any): ((x: any) => any) | undefined;
    (steps: any): (t: any) => number;
    apply?: any;
  },
  args: number[]
) {
  return func.apply(null, args);
}

var is = {
  arr: function (a: any) {
    return Array.isArray(a);
  },
  obj: function (a: any) {
    return stringContains(Object.prototype.toString.call(a), "Object");
  },
  pth: function (a: { hasOwnProperty: (arg0: string) => any }) {
    return is.obj(a) && a.hasOwnProperty("totalLength");
  },
  /*svg: function (a: any) {
    return a instanceof SVGElement;
  },*/
  /*inp: function (a: any) {
    return a instanceof HTMLInputElement;
  },*/
  /*dom: function (a: { nodeType: any }) {
    return a.nodeType || is.svg(a);
  },*/
  str: function (a: any) {
    return typeof a === "string";
  },
  fnc: function (a: any) {
    return typeof a === "function";
  },
  und: function (a: any) {
    return typeof a === "undefined";
  },
  nil: function (a: null) {
    return is.und(a) || a === null;
  },
  hex: function (a: string) {
    return /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(a);
  },
  rgb: function (a: string) {
    return /^rgb/.test(a);
  },
  hsl: function (a: string) {
    return /^hsl/.test(a);
  },
  col: function (a: any) {
    return is.hex(a) || is.rgb(a) || is.hsl(a);
  },
  key: function (a: PropertyKey) {
    return !defaultInstanceSettings.hasOwnProperty(a) && !defaultTweenSettings.hasOwnProperty(a) && a !== "targets" && a !== "keyframes";
  },
};

// Easings

function parseEasingParameters(string: string) {
  var match = /\(([^)]+)\)/.exec(string);
  return match
    ? match[1].split(",").map(function (p) {
        return parseFloat(p);
      })
    : [];
}

// Spring solver inspired by Webkit Copyright © 2016 Apple Inc. All rights reserved. https://webkit.org/demos/spring/spring.js

function spring(string: string, duration?: number) {
  var params = parseEasingParameters(string.toString());
  var mass = minMax(is.und(params[0]) ? 1 : params[0], 0.1, 100);
  var stiffness = minMax(is.und(params[1]) ? 100 : params[1], 0.1, 100);
  var damping = minMax(is.und(params[2]) ? 10 : params[2], 0.1, 100);
  var velocity = minMax(is.und(params[3]) ? 0 : params[3], 0.1, 100);
  var w0 = Math.sqrt(stiffness / mass);
  var zeta = damping / (2 * Math.sqrt(stiffness * mass));
  var wd = zeta < 1 ? w0 * Math.sqrt(1 - zeta * zeta) : 0;
  var a = 1;
  var b = zeta < 1 ? (zeta * w0 + -velocity) / wd : -velocity + w0;

  function solver(t: number) {
    var progress = duration ? (duration * t) / 1000 : t;
    if (zeta < 1) {
      progress = Math.exp(-progress * zeta * w0) * (a * Math.cos(wd * progress) + b * Math.sin(wd * progress));
    } else {
      progress = (a + b * progress) * Math.exp(-progress * w0);
    }
    if (t === 0 || t === 1) {
      return t;
    }
    return 1 - progress;
  }

  function getDuration() {
    var cached = cache.springs[string];
    if (cached) {
      return cached;
    }
    var frame = 1 / 6;
    var elapsed = 0;
    var rest = 0;
    while (true) {
      elapsed += frame;
      if (solver(elapsed) === 1) {
        rest++;
        if (rest >= 16) {
          break;
        }
      } else {
        rest = 0;
      }
    }
    var duration = elapsed * frame * 1000;
    cache.springs[string] = duration;
    return duration;
  }

  return duration ? solver : getDuration;
}

// Basic steps easing implementation https://developer.mozilla.org/fr/docs/Web/CSS/transition-timing-function

function steps(steps: number | undefined) {
  if (steps === undefined) steps = 10;

  return function (t: any) {
    return Math.ceil(minMax(t, 0.000001, 1) * steps!) * (1 / steps!);
  };
}

// BezierEasing https://github.com/gre/bezier-easing

var bezier = (function () {
  var kSplineTableSize = 11;
  var kSampleStepSize = 1.0 / (kSplineTableSize - 1.0);

  function A(aA1: number, aA2: number) {
    return 1.0 - 3.0 * aA2 + 3.0 * aA1;
  }
  function B(aA1: number, aA2: number) {
    return 3.0 * aA2 - 6.0 * aA1;
  }
  function C(aA1: number) {
    return 3.0 * aA1;
  }

  function calcBezier(aT: number, aA1: any, aA2: any) {
    return ((A(aA1, aA2) * aT + B(aA1, aA2)) * aT + C(aA1)) * aT;
  }
  function getSlope(aT: number, aA1: any, aA2: any) {
    return 3.0 * A(aA1, aA2) * aT * aT + 2.0 * B(aA1, aA2) * aT + C(aA1);
  }

  function binarySubdivide(aX: number, aA: number, aB: number, mX1: any, mX2: any) {
    var currentX,
      currentT,
      i = 0;
    do {
      currentT = aA + (aB - aA) / 2.0;
      currentX = calcBezier(currentT, mX1, mX2) - aX;
      if (currentX > 0.0) {
        aB = currentT;
      } else {
        aA = currentT;
      }
    } while (Math.abs(currentX) > 0.0000001 && ++i < 10);
    return currentT;
  }

  function newtonRaphsonIterate(aX: number, aGuessT: number, mX1: any, mX2: any) {
    for (var i = 0; i < 4; ++i) {
      var currentSlope = getSlope(aGuessT, mX1, mX2);
      if (currentSlope === 0.0) {
        return aGuessT;
      }
      var currentX = calcBezier(aGuessT, mX1, mX2) - aX;
      aGuessT -= currentX / currentSlope;
    }
    return aGuessT;
  }

  function bezier(mX1: number, mY1: any, mX2: number, mY2: any) {
    if (!(0 <= mX1 && mX1 <= 1 && 0 <= mX2 && mX2 <= 1)) {
      return;
    }
    var sampleValues = new Float32Array(kSplineTableSize);

    if (mX1 !== mY1 || mX2 !== mY2) {
      for (var i = 0; i < kSplineTableSize; ++i) {
        sampleValues[i] = calcBezier(i * kSampleStepSize, mX1, mX2);
      }
    }

    function getTForX(aX: number) {
      var intervalStart = 0;
      var currentSample = 1;
      var lastSample = kSplineTableSize - 1;

      for (; currentSample !== lastSample && sampleValues[currentSample] <= aX; ++currentSample) {
        intervalStart += kSampleStepSize;
      }

      --currentSample;

      var dist = (aX - sampleValues[currentSample]) / (sampleValues[currentSample + 1] - sampleValues[currentSample]);
      var guessForT = intervalStart + dist * kSampleStepSize;
      var initialSlope = getSlope(guessForT, mX1, mX2);

      if (initialSlope >= 0.001) {
        return newtonRaphsonIterate(aX, guessForT, mX1, mX2);
      } else if (initialSlope === 0.0) {
        return guessForT;
      } else {
        return binarySubdivide(aX, intervalStart, intervalStart + kSampleStepSize, mX1, mX2);
      }
    }

    return function (x: number) {
      if (mX1 === mY1 && mX2 === mY2) {
        return x;
      }
      if (x === 0 || x === 1) {
        return x;
      }
      return calcBezier(getTForX(x), mY1, mY2);
    };
  }

  return bezier;
})();

var penner = (function () {
  // Based on jQuery UI's implemenation of easing equations from Robert Penner (http://www.robertpenner.com/easing)

  var eases: any = {
    linear: function () {
      return function (t: any) {
        return t;
      };
    },
  };

  var functionEasings: any = {
    Sine: function () {
      return function (t: number) {
        return 1 - Math.cos((t * Math.PI) / 2);
      };
    },
    Circ: function () {
      return function (t: number) {
        return 1 - Math.sqrt(1 - t * t);
      };
    },
    Back: function () {
      return function (t: number) {
        return t * t * (3 * t - 2);
      };
    },
    Bounce: function () {
      return function (t: number) {
        var pow2,
          b = 4;
        while (t < ((pow2 = Math.pow(2, --b)) - 1) / 11) {}
        return 1 / Math.pow(4, 3 - b) - 7.5625 * Math.pow((pow2 * 3 - 2) / 22 - t, 2);
      };
    },
    Elastic: function (amplitude: number | undefined, period: number | undefined) {
      if (amplitude === void 0) amplitude = 1;
      if (period === void 0) period = 0.5;

      var a = minMax(amplitude, 1, 10);
      var p = minMax(period, 0.1, 2);
      return function (t: number) {
        return t === 0 || t === 1
          ? t
          : -a * Math.pow(2, 10 * (t - 1)) * Math.sin(((t - 1 - (p / (Math.PI * 2)) * Math.asin(1 / a)) * (Math.PI * 2)) / p);
      };
    },
  };

  var baseEasings = ["Quad", "Cubic", "Quart", "Quint", "Expo"];

  baseEasings.forEach(function (name, i) {
    functionEasings[name] = function () {
      return function (t: number) {
        return Math.pow(t, i + 2);
      };
    };
  });

  Object.keys(functionEasings).forEach(function (name) {
    var easeIn = functionEasings[name];
    eases["easeIn" + name] = easeIn;
    eases["easeOut" + name] = function (a: any, b: any) {
      return function (t: number) {
        return 1 - easeIn(a, b)(1 - t);
      };
    };
    eases["easeInOut" + name] = function (a: any, b: any) {
      return function (t: number) {
        return t < 0.5 ? easeIn(a, b)(t * 2) / 2 : 1 - easeIn(a, b)(t * -2 + 2) / 2;
      };
    };
    eases["easeOutIn" + name] = function (a: any, b: any) {
      return function (t: number) {
        return t < 0.5 ? (1 - easeIn(a, b)(1 - t * 2)) / 2 : (easeIn(a, b)(t * 2 - 1) + 1) / 2;
      };
    };
  });

  return eases;
})();

function parseEasings(easing: string, duration?: any) {
  if (is.fnc(easing)) {
    return easing;
  }
  var name = easing.split("(")[0];
  var ease = penner[name];
  var args = parseEasingParameters(easing);
  switch (name) {
    case "spring":
      return spring(easing, duration);
    case "cubicBezier":
      return applyArguments(bezier as any, args);
    case "steps":
      return applyArguments(steps, args);
    default:
      return applyArguments(ease, args);
  }
}

// Strings

/*function selectString(str: any) {
  try {
    var nodes = document.querySelectorAll(str);
    return nodes;
  } catch (e) {
    return;
  }
}*/

// Arrays

function filterArray(
  arr: any[],
  callback: {
    (item: any, pos: any, self: any): boolean;
  }
) {
  var len = arr.length;
  var thisArg = arguments.length >= 2 ? arguments[1] : void 0;
  var result = [];
  for (var i = 0; i < len; i++) {
    if (i in arr) {
      var val = arr[i];
      if (callback.call(thisArg, val, i, arr)) {
        result.push(val);
      }
    }
  }
  return result;
}

function flattenArray(arr: any[]): any {
  return arr.reduce(function (a: string | any[], b: any) {
    return a.concat(is.arr(b) ? flattenArray(b) : b);
  }, []);
}

function toArray(o: any) {
  if (is.arr(o)) {
    return o;
  }
  /*if (is.str(o)) {
    o = selectString(o) || o;
  }*/
  /*if (o instanceof NodeList || o instanceof HTMLCollection) {
    return [].slice.call(o);
  }*/
  return [o];
}

function arrayContains(arr: any[], val: string | undefined) {
  return arr.some(function (a: any) {
    return a === val;
  });
}

// Objects

function cloneObject(o: { [x: string]: any }) {
  var clone: any = {};
  for (var p in o) {
    clone[p] = o[p];
  }
  return clone;
}

function replaceObjectProps(
  o1: {
    [x: string]: any;
    update?: null;
    begin?: null;
    loopBegin?: null;
    changeBegin?: null;
    change?: null;
    changeComplete?: null;
    loopComplete?: null;
    complete?: null;
    loop?: number;
    direction?: string;
    autoplay?: boolean;
    timelineOffset?: number;
    duration?: number;
    delay?: number;
    endDelay?: number;
    easing?: string;
    round?: number;
  },
  o2: { [x: string]: any; hasOwnProperty: (arg0: string) => any }
) {
  var o = cloneObject(o1);
  for (var p in o1) {
    o[p] = o2.hasOwnProperty(p) ? o2[p] : o1[p];
  }
  return o;
}

function mergeObjects(
  o1: { [x: string]: any },
  o2: {
    [x: string]: any;
    id?: number;
    children?: any[];
    animatables?: {
      target: any;
      id: number;
      total: number;
      transforms: { list: any };
    }[];
    animations?: any[];
    duration?: any;
    delay?: any;
    endDelay?: any;
  }
) {
  var o = cloneObject(o1);
  for (var p in o2) {
    o[p] = is.und(o1[p]) ? o2[p] : o1[p];
  }
  return o;
}

// Colors

function rgbToRgba(rgbValue: string) {
  var rgb = /rgb\((\d+,\s*[\d]+,\s*[\d]+)\)/g.exec(rgbValue);
  return rgb ? "rgba(" + rgb[1] + ",1)" : rgbValue;
}

function hexToRgba(hexValue: string) {
  var rgx = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
  var hex = hexValue.replace(rgx, function (m: any, r: any, g: any, b: any) {
    return r + r + g + g + b + b;
  });
  var rgb = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  var r = parseInt(rgb![1], 16);
  var g = parseInt(rgb![2], 16);
  var b = parseInt(rgb![3], 16);
  return "rgba(" + r + "," + g + "," + b + ",1)";
}

function hslToRgba(hslValue: string) {
  var hsl =
    /hsl\((\d+),\s*([\d.]+)%,\s*([\d.]+)%\)/g.exec(hslValue) || /hsla\((\d+),\s*([\d.]+)%,\s*([\d.]+)%,\s*([\d.]+)\)/g.exec(hslValue);
  var h = parseInt(hsl![1], 10) / 360;
  var s = parseInt(hsl![2], 10) / 100;
  var l = parseInt(hsl![3], 10) / 100;
  var a = hsl![4] || 1;
  function hue2rgb(p: number, q: number, t: number) {
    if (t < 0) {
      t += 1;
    }
    if (t > 1) {
      t -= 1;
    }
    if (t < 1 / 6) {
      return p + (q - p) * 6 * t;
    }
    if (t < 1 / 2) {
      return q;
    }
    if (t < 2 / 3) {
      return p + (q - p) * (2 / 3 - t) * 6;
    }
    return p;
  }
  var r, g, b;
  if (s == 0) {
    r = g = b = l;
  } else {
    var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
    var p = 2 * l - q;
    r = hue2rgb(p, q, h + 1 / 3);
    g = hue2rgb(p, q, h);
    b = hue2rgb(p, q, h - 1 / 3);
  }
  return "rgba(" + r * 255 + "," + g * 255 + "," + b * 255 + "," + a + ")";
}

function colorToRgb(val: any) {
  if (is.rgb(val)) {
    return rgbToRgba(val);
  }
  if (is.hex(val)) {
    return hexToRgba(val);
  }
  if (is.hsl(val)) {
    return hslToRgba(val);
  }
}

// Units

function getUnit(val: string) {
  var split = /[+-]?\d*\.?\d+(?:\.\d+)?(?:[eE][+-]?\d+)?(%|px|pt|em|rem|in|cm|mm|ex|ch|pc|vw|vh|vmin|vmax|deg|rad|turn)?$/.exec(val);
  if (split) {
    return split[1];
  }
}

function getTransformUnit(propName: string) {
  if (stringContains(propName, "translate") || propName === "perspective") {
    return "px";
  }
  if (stringContains(propName, "rotate") || stringContains(propName, "skew")) {
    return "deg";
  }
}

// Values

function getFunctionValue(
  val: (arg0: any, arg1: any, arg2: any) => any,
  animatable: {
    target: any;
    id: any;
    total: any;
    transforms?: { list: any };
  }
) {
  if (!is.fnc(val)) {
    return val;
  }
  return val(animatable.target, animatable.id, animatable.total);
}

/*function getAttribute(el: { getAttribute: (arg0: any) => any }, prop: string) {
  return el.getAttribute(prop);
}*/

/*function convertPxToUnit(el: any, value: string, unit: number) {
  var valueUnit = getUnit(value);
  if (arrayContains([unit, "deg", "rad", "turn"], valueUnit)) {
    return value;
  }
  var cached = cache.CSS[value + unit];
  if (!is.und(cached)) {
    return cached;
  }
  var baseline = 100;
  var tempEl = document.createElement(el.tagName);
  var parentEl =
    el.parentNode && el.parentNode !== document ? el.parentNode : document.body;
  parentEl.appendChild(tempEl);
  tempEl.style.position = "absolute";
  tempEl.style.width = baseline + unit;
  var factor = baseline / tempEl.offsetWidth;
  parentEl.removeChild(tempEl);
  var convertedUnit = factor * parseFloat(value);
  cache.CSS[value + unit] = convertedUnit;
  return convertedUnit;
}*/

/*function getCSSValue(el: any, prop: string, unit: any) {
  if (prop in el.style) {
    var uppercasePropName = prop
      .replace(/([a-z])([A-Z])/g, "$1-$2")
      .toLowerCase();
    var value =
      el.style[prop] ||
      getComputedStyle(el).getPropertyValue(uppercasePropName) ||
      "0";
    return unit ? convertPxToUnit(el, value, unit) : value;
  }
}*/

function getAnimationType(el: { [x: string]: null }, prop: string) {
  /*if (
    is.dom(el) &&
    !is.inp(el) &&
    (!is.nil(getAttribute(el, prop)) || (is.svg(el) && el[prop]))
  ) {
    return "attribute";
  }
  if (is.dom(el) && arrayContains(validTransforms, prop)) {
    return "transform";
  }
  if (is.dom(el) && prop !== "transform" && getCSSValue(el, prop)) {
    return "css";
  }*/
  if (el[prop] != null) {
    return "object";
  }
}

/*function getElementTransforms(el: any) {
  
  if (!is.dom(el)) {
    return;
  }
  var str = el.style.transform || "";
  var reg = /(\w+)\(([^)]*)\)/g;
  var transforms = new Map();
  var m;
  while ((m = reg.exec(str))) {
    transforms.set(m[1], m[2]);
  }
  return transforms;
}*/

/*function getTransformValue(
  el: any,
  propName: any,
  animatable: {
    transforms: {
      [x: string]: any;
      list: { set: (arg0: any, arg1: any) => void };
    };
  },
  unit: any
) {
  var defaultVal = stringContains(propName, "scale")
    ? 1
    : 0 + getTransformUnit(propName)!;
  var value = getElementTransforms(el)!.get(propName) || defaultVal;
  if (animatable) {
    animatable.transforms.list.set(propName, value);
    animatable.transforms["last"] = propName;
  }
  return unit ? convertPxToUnit(el, value, unit) : value;
}*/

function getOriginalTargetValue(
  target: { [x: string]: any },
  propName: string,
  unit: string | undefined,
  animatable: {
    target: any;
    id: number;
    total: number;
    transforms: { list: Map<any, any> | undefined };
  }
) {
  /*switch (getAnimationType(target, propName)) {
    case "transform":
      return getTransformValue(target, propName, animatable, unit);
    case "css":
      return getCSSValue(target, propName, unit);
    case "attribute":
      return getAttribute(target, propName);
    default:
      return target[propName] || 0;
  }*/

  return target[propName] || 0;
}

function getRelativeValue(to: string, from: string) {
  var operator = /^(\*=|\+=|-=)/.exec(to);
  if (!operator) {
    return to;
  }
  var u = parseFloat(getUnit(to) || "0");
  var x = parseFloat(from);
  var y = parseFloat(to.replace(operator[0], ""));
  switch (operator[0][0]) {
    case "+":
      return x + y + u;
    case "-":
      return x - y + u;
    case "*":
      return x * y + u;
  }
}

function validateValue(val: string, unit?: string) {
  if (is.col(val)) {
    return colorToRgb(val);
  }
  if (/\s/g.test(val)) {
    return val;
  }
  var originalUnit = getUnit(val);
  var unitLess = originalUnit ? val.substr(0, val.length - originalUnit.length) : val;
  if (unit) {
    return unitLess + unit;
  }
  return unitLess;
}

// getTotalLength() equivalent for circle, rect, polyline, polygon and line shapes
// adapted from https://gist.github.com/SebLambla/3e0550c496c236709744

function getDistance(p1: { x: any; y: any }, p2: { x: any; y: any }) {
  return Math.sqrt(Math.pow(p2.x - p1.x, 2) + Math.pow(p2.y - p1.y, 2));
}

/*function getCircleLength(el: any) {
  return Math.PI * 2 * getAttribute(el, "r");
}

function getRectLength(el: any) {
  return getAttribute(el, "width") * 2 + getAttribute(el, "height") * 2;
}

function getLineLength(el: any) {
  return getDistance(
    { x: getAttribute(el, "x1"), y: getAttribute(el, "y1") },
    { x: getAttribute(el, "x2"), y: getAttribute(el, "y2") }
  );
}

function getPolylineLength(el: { points: any }) {
  var points = el.points;
  var totalLength = 0;
  var previousPos;
  for (var i = 0; i < points.numberOfItems; i++) {
    var currentPos = points.getItem(i);
    if (i > 0) {
      totalLength += getDistance(previousPos, currentPos);
    }
    previousPos = currentPos;
  }
  return totalLength;
}

function getPolygonLength(el: { points: any }) {
  var points = el.points;
  return (
    getPolylineLength(el) +
    getDistance(points.getItem(points.numberOfItems - 1), points.getItem(0))
  );
}

// Path animation

function getTotalLength(el: { getTotalLength: () => any; tagName: string }) {
  if (el.getTotalLength) {
    return el.getTotalLength();
  }
  switch (el.tagName.toLowerCase()) {
    case "circle":
      return getCircleLength(el);
    case "rect":
      return getRectLength(el);
    case "line":
      return getLineLength(el);
    case "polyline":
      return getPolylineLength(el);
    case "polygon":
      return getPolygonLength(el);
  }
}

function setDashoffset(el: {
  setAttribute: (arg0: string, arg1: any) => void;
}) {
  var pathLength = getTotalLength(el);
  el.setAttribute("stroke-dasharray", pathLength);
  return pathLength;
}

// Motion path

function getParentSvgEl(el: { parentNode: any }) {
  var parentEl = el.parentNode;
  while (is.svg(parentEl)) {
    if (!is.svg(parentEl.parentNode)) {
      break;
    }
    parentEl = parentEl.parentNode;
  }
  return parentEl;
}

function getParentSvg(pathEl: any, svgData: {} | undefined) {
  var svg = svgData || {};
  var parentSvgEl = svg.el || getParentSvgEl(pathEl);
  var rect = parentSvgEl.getBoundingClientRect();
  var viewBoxAttr = getAttribute(parentSvgEl, "viewBox");
  var width = rect.width;
  var height = rect.height;
  var viewBox =
    svg.viewBox ||
    (viewBoxAttr ? viewBoxAttr.split(" ") : [0, 0, width, height]);
  return {
    el: parentSvgEl,
    viewBox: viewBox,
    x: viewBox[0] / 1,
    y: viewBox[1] / 1,
    w: width,
    h: height,
    vW: viewBox[2],
    vH: viewBox[3],
  };
}

function getPath(path: any, percent: number) {
  var pathEl = is.str(path) ? selectString(path)[0] : path;
  var p = percent || 100;
  return function (property: any) {
    return {
      property: property,
      el: pathEl,
      svg: getParentSvg(pathEl),
      totalLength: getTotalLength(pathEl) * (p / 100),
    };
  };
}

function getPathProgress(
  path: {
    el: { getPointAtLength: (arg0: any) => any };
    svg: any;
    property: any;
  },
  progress: number,
  isPathTargetInsideSVG: any
) {
  function point(offset: number | undefined) {
    if (offset === void 0) offset = 0;

    var l = progress + offset >= 1 ? progress + offset : 0;
    return path.el.getPointAtLength(l);
  }
  var svg = getParentSvg(path.el, path.svg);
  var p = point();
  var p0 = point(-1);
  var p1 = point(+1);
  var scaleX = isPathTargetInsideSVG ? 1 : svg.w / svg.vW;
  var scaleY = isPathTargetInsideSVG ? 1 : svg.h / svg.vH;
  switch (path.property) {
    case "x":
      return (p.x - svg.x) * scaleX;
    case "y":
      return (p.y - svg.y) * scaleY;
    case "angle":
      return (Math.atan2(p1.y - p0.y, p1.x - p0.x) * 180) / Math.PI;
  }
}*/

// Decompose value

function decomposeValue(val: any, unit?: string) {
  // const rgx = /-?\d*\.?\d+/g; // handles basic numbers
  // const rgx = /[+-]?\d+(?:\.\d+)?(?:[eE][+-]?\d+)?/g; // handles exponents notation
  var rgx = /[+-]?\d*\.?\d+(?:\.\d+)?(?:[eE][+-]?\d+)?/g; // handles exponents notation
  var value = validateValue(is.pth(val) ? val.totalLength : val, unit) + "";
  return {
    original: value,
    numbers: value.match(rgx) ? value.match(rgx)!.map(Number) : [0],
    strings: is.str(val) || unit ? value.split(rgx) : [],
  };
}

// Animatables

function parseTargets(targets: any) {
  var targetsArray = targets ? flattenArray(is.arr(targets) ? targets.map(toArray) : toArray(targets)) : [];
  return filterArray(targetsArray, (item: any, pos: any, self: any[]) => {
    return self.indexOf(item) === pos;
  });
}

function getAnimatables(targets: any) {
  var parsed = parseTargets(targets);
  return parsed.map(function (t, i) {
    return {
      target: t,
      id: i,
      total: parsed.length,
      transforms: { list: new Map() },
    };
  });
}

// Properties

function normalizePropertyTweens(prop: string | any[], tweenSettings: { duration: number; delay: any; endDelay: any }) {
  var settings = cloneObject(tweenSettings);
  // Override duration if easing is a spring
  if (/^spring/.test(settings.easing)) {
    settings.duration = spring(settings.easing);
  }
  if (is.arr(prop)) {
    var l = prop.length;
    var isFromTo = l === 2 && !is.obj(prop[0]);
    if (!isFromTo) {
      // Duration divided by the number of tweens
      if (!is.fnc(tweenSettings.duration)) {
        settings.duration = tweenSettings.duration / l;
      }
    } else {
      // Transform [from, to] values shorthand to a valid tween value
      prop = { value: prop } as any;
    }
  }
  var propArray: any = is.arr(prop) ? prop : [prop];
  return propArray
    .map(function (v: any, i: number) {
      var obj = is.obj(v) && !is.pth(v) ? v : { value: v };
      // Default delay value should only be applied to the first tween
      if (is.und(obj.delay)) {
        obj.delay = !i ? tweenSettings.delay : 0;
      }
      // Default endDelay value should only be applied to the last tween
      if (is.und(obj.endDelay)) {
        obj.endDelay = i === propArray.length - 1 ? tweenSettings.endDelay : 0;
      }
      return obj;
    })
    .map(function (k: any) {
      return mergeObjects(k, settings);
    });
}

function flattenKeyframes(keyframes: any[]) {
  var propertyNames = filterArray(
    flattenArray(
      keyframes.map(function (key: {}) {
        return Object.keys(key);
      })
    ),
    function (p: any) {
      return is.key(p);
    }
  ).reduce(function (a, b) {
    if (a.indexOf(b) < 0) {
      a.push(b);
    }
    return a;
  }, []);
  var properties: any = {};
  var loop = function (i: number) {
    var propName = propertyNames[i];
    properties[propName] = keyframes.map(function (key: { [x: string]: any }) {
      var newKey: any = {};
      for (var p in key) {
        if (is.key(p)) {
          if (p == propName) {
            newKey.value = key[p];
          }
        } else {
          newKey[p] = key[p];
        }
      }
      return newKey;
    });
  };

  for (var i = 0; i < propertyNames.length; i++) loop(i);
  return properties;
}

function getProperties(tweenSettings: any, params: { [x: string]: any; keyframes?: any }) {
  var properties = [];
  var keyframes = params.keyframes;
  if (keyframes) {
    params = mergeObjects(flattenKeyframes(keyframes), params);
  }
  for (var p in params) {
    if (is.key(p)) {
      properties.push({
        name: p,
        tweens: normalizePropertyTweens(params[p], tweenSettings),
      });
    }
  }
  return properties;
}

// Tweens

function normalizeTweenValues(tween: { [x: string]: any }, animatable: any) {
  var t: any = {};
  for (var p in tween) {
    var value = getFunctionValue(tween[p], animatable);
    if (is.arr(value)) {
      value = value.map(function (v: any) {
        return getFunctionValue(v, animatable);
      });
      if (value.length === 1) {
        value = value[0];
      }
    }
    t[p] = value;
  }
  t.duration = parseFloat(t.duration);
  t.delay = parseFloat(t.delay);
  return t;
}

function normalizeTweens(prop: { tweens: any[]; name: any }, animatable: any) {
  var previousTween: { to?: any; end?: any };
  return prop.tweens.map(function (t: any) {
    var tween = normalizeTweenValues(t, animatable);
    var tweenValue = tween.value;
    var to = is.arr(tweenValue) ? tweenValue[1] : tweenValue;
    var toUnit = getUnit(to);
    var originalValue = getOriginalTargetValue(animatable.target, prop.name, toUnit, animatable);
    var previousValue = previousTween ? previousTween.to.original : originalValue;
    var from = is.arr(tweenValue) ? tweenValue[0] : previousValue;
    var fromUnit = getUnit(from) || getUnit(originalValue);
    var unit = toUnit || fromUnit;
    if (is.und(to)) {
      to = previousValue;
    }
    tween.from = decomposeValue(from, unit);
    tween.to = decomposeValue(getRelativeValue(to, from), unit);
    tween.start = previousTween ? previousTween.end : 0;
    tween.end = tween.start + tween.delay + tween.duration + tween.endDelay;
    tween.easing = parseEasings(tween.easing, tween.duration);
    tween.isPath = is.pth(tweenValue);
    tween.isPathTargetInsideSVG = false; //tween.isPath && is.svg(animatable.target);
    tween.isColor = is.col(tween.from.original);
    if (tween.isColor) {
      tween.round = 1;
    }
    previousTween = tween;
    return tween;
  });
}

// Tween progress

var setProgressValue: any = {
  /*css: function (
    t: { style: { [x: string]: any } },
    p: string | number,
    v: any
  ) {
    return (t.style[p] = v);
  },*/
  /*attribute: function (
    t: { setAttribute: (arg0: any, arg1: any) => any },
    p: any,
    v: any
  ) {
    return t.setAttribute(p, v);
  },*/
  object: function (t: { [x: string]: any }, p: string | number, v: any) {
    return (t[p] = v);
  },
  /*transform: function (
    t: { style: { transform: string } },
    p: any,
    v: any,
    transforms: {
      list: {
        set: (arg0: any, arg1: any) => void;
        forEach: (arg0: (value: any, prop: any) => void) => void;
      };
      last: any;
    },
    manual: any
  ) {
    transforms.list.set(p, v);
    if (p === transforms.last || manual) {
      var str = "";
      transforms.list.forEach(function (value: string, prop: string) {
        str += prop + "(" + value + ") ";
      });
      t.style.transform = str;
    }
  },*/
};

// Set Value helper

function setTargetsValue(targets: any, properties: { [x: string]: any }) {
  var animatables = getAnimatables(targets);
  animatables.forEach(function (animatable) {
    for (var property in properties) {
      var value = getFunctionValue(properties[property], animatable);
      var target = animatable.target;
      var valueUnit = getUnit(value);
      var originalValue = getOriginalTargetValue(target, property, valueUnit, animatable);
      var unit = valueUnit || getUnit(originalValue);
      var to = getRelativeValue(validateValue(value, unit)!, originalValue);
      var animType = getAnimationType(target, property);
      setProgressValue[animType!](target, property, to, animatable.transforms, true);
    }
  });
}

// Animations

function createAnimation(animatable: { target: any }, prop: any) {
  var animType = getAnimationType(animatable.target, prop.name);
  if (animType) {
    var tweens = normalizeTweens(prop, animatable);
    var lastTween = tweens[tweens.length - 1];
    return {
      type: animType,
      property: prop.name,
      animatable: animatable,
      tweens: tweens,
      duration: lastTween.end,
      delay: tweens[0].delay,
      endDelay: lastTween.endDelay,
    };
  }
}

function getAnimations(animatables: any[], properties: any[]) {
  return filterArray(
    flattenArray(
      animatables.map(function (animatable: any) {
        return properties.map(function (prop: any) {
          return createAnimation(animatable, prop);
        });
      })
    ),
    function (a: any) {
      return !is.und(a);
    }
  );
}

// Create Instance

function getInstanceTimings(animations: any[], tweenSettings: { duration?: any; delay?: any; endDelay?: any }) {
  var animLength = animations.length;
  var getTlOffset = function (anim: { timelineOffset: any }) {
    return anim.timelineOffset ? anim.timelineOffset : 0;
  };
  var timings: any = {};
  timings.duration = animLength
    ? Math.max.apply(
        Math,
        animations.map(function (anim: any) {
          return getTlOffset(anim) + anim.duration;
        })
      )
    : tweenSettings.duration;
  timings.delay = animLength
    ? Math.min.apply(
        Math,
        animations.map(function (anim: any) {
          return getTlOffset(anim) + anim.delay;
        })
      )
    : tweenSettings.delay;
  timings.endDelay = animLength
    ? timings.duration -
      Math.max.apply(
        Math,
        animations.map(function (anim: any) {
          return getTlOffset(anim) + anim.duration - anim.endDelay;
        })
      )
    : tweenSettings.endDelay;
  return timings;
}

var instanceID = 0;

function createNewInstance(params: { targets: any }) {
  var instanceSettings = replaceObjectProps(defaultInstanceSettings, params);
  var tweenSettings = replaceObjectProps(defaultTweenSettings, params);
  var properties = getProperties(tweenSettings, params);
  var animatables = getAnimatables(params.targets);
  var animations = getAnimations(animatables, properties);
  var timings = getInstanceTimings(animations, tweenSettings);
  var id = instanceID;
  instanceID++;
  return mergeObjects(instanceSettings, {
    id: id,
    children: [],
    animatables: animatables,
    animations: animations,
    duration: timings.duration,
    delay: timings.delay,
    endDelay: timings.endDelay,
  });
}

// Core

// var activeInstances = [];

/*var engine = (function () {
  var raf;

  function play() {
    if (!raf && activeInstances.length > 0) {
      raf = requestAnimationFrame(step);
    }
  }
  function step(t) {
    
    // memo on algorithm issue:
    // dangerous iteration over mutable `activeInstances`
    // (that collection may be updated from within callbacks of `tick`-ed animation instances)
    var activeInstancesLength = activeInstances.length;
    var i = 0;
    while (i < activeInstancesLength) {
      var activeInstance = activeInstances[i];
      if (!activeInstance.paused) {
        activeInstance.tick(t);
        i++;
      } else {
        activeInstances.splice(i, 1);
        activeInstancesLength--;
      }
    }
    raf = i > 0 ? requestAnimationFrame(step) : undefined;
  }

  return play;
})();*/

// Public Instance

function anime(params: any) {
  if (params === undefined) params = {};

  var startTime = 0,
    lastTime = 0,
    now = 0;
  var children: any[],
    childrenLength = 0;

  var instance = createNewInstance(params);

  function toggleInstanceDirection() {
    var direction = instance.direction;
    if (direction !== "alternate") {
      instance.direction = direction !== "normal" ? "normal" : "reverse";
    }
    instance.reversed = !instance.reversed;
    children.forEach(function (child: { reversed: any }) {
      return (child.reversed = instance.reversed);
    });
  }

  function adjustTime(time: number) {
    return instance.reversed ? instance.duration - time : time;
  }

  function resetTime() {
    startTime = 0;
    lastTime = adjustTime(instance.currentTime) * (1 / anime.speed);
  }

  function seekChild(time: number, child: { seek: (arg0: number) => void; timelineOffset: number }) {
    if (child) {
      child.seek(time - child.timelineOffset);
    }
  }

  function syncInstanceChildren(time: any) {
    if (!instance.reversePlayback) {
      for (var i = 0; i < childrenLength; i++) {
        seekChild(time, children[i]);
      }
    } else {
      for (var i$1 = childrenLength; i$1--; ) {
        seekChild(time, children[i$1]);
      }
    }
  }

  function setAnimationsProgress(insTime: number) {
    var i = 0;
    var animations = instance.animations;
    var animationsLength = animations.length;
    while (i < animationsLength) {
      var anim = animations[i];
      var animatable = anim.animatable;
      var tweens = anim.tweens;
      var tweenLength = tweens.length - 1;
      var tween = tweens[tweenLength];
      // Only check for keyframes if there is more than one tween
      if (tweenLength) {
        tween =
          filterArray(tweens, function (t: { end: number }) {
            return insTime < t.end;
          })[0] || tween;
      }
      var elapsed = minMax(insTime - tween.start - tween.delay, 0, tween.duration) / tween.duration;
      var eased = isNaN(elapsed) ? 1 : tween.easing(elapsed);
      var strings = tween.to.strings;
      var round = tween.round;
      var numbers = [];
      var toNumbersLength = tween.to.numbers.length;
      var progress = undefined;
      for (var n = 0; n < toNumbersLength; n++) {
        var value = undefined;
        var toNumber = tween.to.numbers[n];
        var fromNumber = tween.from.numbers[n] || 0;
        if (!tween.isPath) {
          value = fromNumber + eased * (toNumber - fromNumber);
        } /*else {
          value = getPathProgress(
            tween.value,
            eased * toNumber,
            tween.isPathTargetInsideSVG
          );
        }*/
        if (round) {
          if (!(tween.isColor && n > 2)) {
            value = Math.round(value! * round) / round;
          }
        }
        numbers.push(value);
      }
      // Manual Array.reduce for better performances
      var stringsLength = strings.length;
      if (!stringsLength) {
        progress = numbers[0];
      } else {
        progress = strings[0];
        for (var s = 0; s < stringsLength; s++) {
          var a = strings[s];
          var b = strings[s + 1];
          var n$1 = numbers[s];
          if (!isNaN(n$1)) {
            if (!b) {
              progress += n$1 + " ";
            } else {
              progress += n$1 + b;
            }
          }
        }
      }
      setProgressValue[anim.type](animatable.target, anim.property, progress, animatable.transforms);
      anim.currentValue = progress;
      i++;
    }
  }

  function setCallback(cb: string) {
    if (instance[cb] && !instance.passThrough) {
      instance[cb](instance);
    }
  }

  function countIteration() {
    if (instance.remaining && instance.remaining !== true) {
      instance.remaining--;
    }
  }

  function setInstanceProgress(engineTime: number) {
    var insDuration = instance.duration;
    var insDelay = instance.delay;
    var insEndDelay = insDuration - instance.endDelay;
    var insTime = adjustTime(engineTime);
    instance.progress = minMax((insTime / insDuration) * 100, 0, 100);
    instance.reversePlayback = insTime < instance.currentTime;
    if (children) {
      syncInstanceChildren(insTime);
    }
    if (!instance.began && instance.currentTime > 0) {
      instance.began = true;
      setCallback("begin");
    }
    if (!instance.loopBegan && instance.currentTime > 0) {
      instance.loopBegan = true;
      setCallback("loopBegin");
    }
    if (insTime <= insDelay && instance.currentTime !== 0) {
      setAnimationsProgress(0);
    }
    if ((insTime >= insEndDelay && instance.currentTime !== insDuration) || !insDuration) {
      setAnimationsProgress(insDuration);
    }
    if (insTime > insDelay && insTime < insEndDelay) {
      if (!instance.changeBegan) {
        instance.changeBegan = true;
        instance.changeCompleted = false;
        setCallback("changeBegin");
      }
      setCallback("change");
      setAnimationsProgress(insTime);
    } else {
      if (instance.changeBegan) {
        instance.changeCompleted = true;
        instance.changeBegan = false;
        setCallback("changeComplete");
      }
    }
    instance.currentTime = minMax(insTime, 0, insDuration);
    if (instance.began) {
      setCallback("update");
    }
    if (engineTime >= insDuration) {
      lastTime = 0;
      countIteration();
      if (!instance.remaining) {
        instance.paused = true;
        if (!instance.completed) {
          instance.completed = true;
          setCallback("loopComplete");
          setCallback("complete");
        }
      } else {
        startTime = now;
        setCallback("loopComplete");
        instance.loopBegan = false;
        if (instance.direction === "alternate") {
          toggleInstanceDirection();
        }
      }
    }
  }

  instance.reset = function () {
    var direction = instance.direction;
    instance.passThrough = false;
    instance.currentTime = 0;
    instance.progress = 0;
    instance.paused = true;
    instance.began = false;
    instance.loopBegan = false;
    instance.changeBegan = false;
    instance.completed = false;
    instance.changeCompleted = false;
    instance.reversePlayback = false;
    instance.reversed = direction === "reverse";
    instance.remaining = instance.loop;
    children = instance.children;
    childrenLength = children.length;
    for (var i = childrenLength; i--; ) {
      instance.children[i].reset();
    }
    if ((instance.reversed && instance.loop !== true) || (direction === "alternate" && instance.loop === 1)) {
      instance.remaining++;
    }
    setAnimationsProgress(instance.reversed ? instance.duration : 0);
  };

  // Set Value helper

  instance.set = function (targets: any, properties: any) {
    setTargetsValue(targets, properties);
    return instance;
  };

  instance.tick = function (t: number) {
    now = t;
    if (!startTime) {
      startTime = now;
    }
    setInstanceProgress((now + (lastTime - startTime)) * anime.speed);
  };

  instance.seek = function (time: any) {
    setInstanceProgress(adjustTime(time));
  };

  instance.pause = function () {
    instance.paused = true;
    resetTime();
  };

  instance.play = function () {
    if (!instance.paused) {
      return;
    }
    if (instance.completed) {
      instance.reset();
    }
    instance.paused = false;
    // activeInstances.push(instance);
    resetTime();
  };

  instance.reverse = function () {
    toggleInstanceDirection();
    instance.completed = instance.reversed ? false : true;
    resetTime();
  };

  instance.restart = function () {
    instance.reset();
    instance.play();
  };

  instance.remove = function (targets: any) {
    var targetsArray = parseTargets(targets);
    removeTargetsFromInstance(targetsArray, instance);
  };

  instance.reset();

  if (instance.autoplay) {
    instance.play();
  }

  return instance;
}

// Remove targets from animation

function removeTargetsFromAnimations(targetsArray: any, animations: any[]) {
  for (var a = animations.length; a--; ) {
    if (arrayContains(targetsArray, animations[a].animatable.target)) {
      animations.splice(a, 1);
    }
  }
}

function removeTargetsFromInstance(targetsArray: any[], instance: { animations?: any; children?: any; pause?: any }) {
  var animations = instance.animations;
  var children = instance.children;
  removeTargetsFromAnimations(targetsArray, animations);
  for (var c = children.length; c--; ) {
    var child = children[c];
    var childAnimations = child.animations;
    removeTargetsFromAnimations(targetsArray, childAnimations);
    if (!childAnimations.length && !child.children.length) {
      children.splice(c, 1);
    }
  }
  if (!animations.length && !children.length) {
    instance.pause();
  }
}

/*function removeTargetsFromActiveInstances(targets: any) {
  var targetsArray = parseTargets(targets);
  for (var i = activeInstances.length; i--; ) {
    var instance = activeInstances[i];
    removeTargetsFromInstance(targetsArray, instance);
  }
}*/

// Stagger helpers

function stagger(
  val: any,
  params:
    | {
        direction?: any;
        easing?: any;
        grid?: any;
        axis?: any;
        from?: any;
        start?: any;
      }
    | undefined
) {
  if (params === void 0) params = {};

  var direction = params.direction || "normal";
  var easing = params.easing ? parseEasings(params.easing) : null;
  var grid = params.grid;
  var axis = params.axis;
  var fromIndex = params.from || 0;
  var fromFirst = fromIndex === "first";
  var fromCenter = fromIndex === "center";
  var fromLast = fromIndex === "last";
  var isRange = is.arr(val);
  var val1 = isRange ? parseFloat(val[0]) : parseFloat(val);
  var val2 = isRange ? parseFloat(val[1]) : 0;
  var unit = getUnit(isRange ? val[1] : val) || 0;
  var start = params.start || 0 + (isRange ? val1 : 0);
  var values: number[] = [];
  var maxValue = 0;
  return function (el: any, i: any, t: number) {
    if (fromFirst) {
      fromIndex = 0;
    }
    if (fromCenter) {
      fromIndex = (t - 1) / 2;
    }
    if (fromLast) {
      fromIndex = t - 1;
    }
    if (!values.length) {
      for (var index = 0; index < t; index++) {
        if (!grid) {
          values.push(Math.abs(fromIndex - index));
        } else {
          var fromX = !fromCenter ? fromIndex % grid[0] : (grid[0] - 1) / 2;
          var fromY = !fromCenter ? Math.floor(fromIndex / grid[0]) : (grid[1] - 1) / 2;
          var toX = index % grid[0];
          var toY = Math.floor(index / grid[0]);
          var distanceX = fromX - toX;
          var distanceY = fromY - toY;
          var value = Math.sqrt(distanceX * distanceX + distanceY * distanceY);
          if (axis === "x") {
            value = -distanceX;
          }
          if (axis === "y") {
            value = -distanceY;
          }
          values.push(value);
        }
        maxValue = Math.max.apply(Math, values);
      }
      if (easing) {
        values = values.map(function (val) {
          return easing(val / maxValue) * maxValue;
        });
      }
      if (direction === "reverse") {
        values = values.map(function (val) {
          return axis ? (val < 0 ? val * -1 : -val) : Math.abs(maxValue - val);
        });
      }
    }
    var spacing = isRange ? (val2 - val1) / maxValue : val1;
    return start + spacing * (Math.round(values[i] * 100) / 100) + unit;
  };
}

// Timeline
function timeline(params: { targets?: any } | undefined) {
  /*
  if (params === void 0) params = {};

  var tl = anime(params);
  tl.duration = 0;
  tl.add = function (instanceParams: any, timelineOffset: any) {
    var tlIndex = activeInstances.indexOf(tl);
    var children = tl.children;
    if (tlIndex > -1) {
      activeInstances.splice(tlIndex, 1);
    }
    function passThrough(ins: { passThrough?: any }) {
      ins.passThrough = true;
    }
    for (var i = 0; i < children.length; i++) {
      passThrough(children[i]);
    }
    var insParams = mergeObjects(
      instanceParams,
      replaceObjectProps(defaultTweenSettings, params)
    );
    insParams.targets = insParams.targets || params.targets;
    var tlDuration = tl.duration;
    insParams.autoplay = false;
    insParams.direction = tl.direction;
    insParams.timelineOffset = is.und(timelineOffset)
      ? tlDuration
      : getRelativeValue(timelineOffset, tlDuration);
    passThrough(tl);
    tl.seek(insParams.timelineOffset);
    var ins = anime(insParams);
    passThrough(ins);
    children.push(ins);
    var timings = getInstanceTimings(children, params);
    tl.delay = timings.delay;
    tl.endDelay = timings.endDelay;
    tl.duration = timings.duration;
    tl.seek(0);
    tl.reset();
    if (tl.autoplay) {
      tl.play();
    }
    return tl;
  };
  return tl;*/
}

anime.version = "3.2.1";
anime.speed = 1;
// TODO:#review: naming, documentation

// anime.running = activeInstances;
// anime.remove = removeTargetsFromActiveInstances;
anime.get = getOriginalTargetValue;
anime.set = setTargetsValue;
// anime.convertPx = convertPxToUnit;
// anime.path = getPath;
// anime.setDashoffset = setDashoffset;
anime.stagger = stagger;
anime.timeline = timeline;
anime.easing = parseEasings;
anime.penner = penner;
anime.random = function (min: number, max: number) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

/*class TweenSystem {
  update(t) {
    components.forEach((c) => c.update(t));
    // var activeInstancesLength = activeInstances.length;
    // var i = 0;
    // while (i < activeInstancesLength) {
    //   var activeInstance = activeInstances[i];
    //   if (!activeInstance.paused) {
    //     activeInstance.tick(t);
    //     i++;
    //   } else {
    //     activeInstances.splice(i, 1);
    //     activeInstancesLength--;
    //   }
    // }
  }
}*/

class TweenSystem extends System {
  public update(dt: number) {
    experience.forEachComponent(TweenComponent, (tweenComponent) => {
      tweenComponent.update(dt);
    });
  }
}

class TweenComponent extends Component {
  time = 0;
  private animeIntance: any;
  constructor(params: any) {
    super();
    this.animeIntance = anime(params);
  }

  update(dt: number) {
    // @ts-ignore

    this.time += dt;

    if (!this.animeIntance.paused) {
      this.animeIntance.tick(this.time * 1000);
    }
  }
}

export { TweenSystem, TweenComponent };