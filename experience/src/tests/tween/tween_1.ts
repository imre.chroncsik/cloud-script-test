import { experience, Transform } from "arsdk";
import { TweenSystem, TweenComponent } from "./tween-engine";

/**
 * Test experience
 * Composer: https://short.staging.immersive.verizonmedia.com/aIb8H
 * Android: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/android/index.json
 * iOS: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/ios/index.json
 * Web: https://cdn.launch3d.com/cu/xr/preview-stable/index.html?customJsonPath=https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/web/index.json
 */

experience.addSystem(new TweenSystem());

experience.addObjectScript("c4189b56-e181-4ed5-be91-419e23b6c528", (rootEntity) => {
  //TODO: Composer currently inserts one extra node. Can we remove this?
  rootEntity = rootEntity.getChild(0)!.getChild(0)!;

  const ball = rootEntity.getChild(1)!.getChild(2)!;
  ball.addComponent(
    new TweenComponent({
      targets: ball.getComponent(Transform)!.localPosition,
      z: ball.getComponent(Transform)!.localPosition.z - 1,
      y: ball.getComponent(Transform)!.localPosition.y + 1,
      easing: "spring(1, 80, 10, 0)",
      direction: "alternate",
      loop: true,
    })
  );
});
