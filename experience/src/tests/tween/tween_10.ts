import { experience, Transform } from "arsdk";
import { TweenSystem, TweenComponent } from "./tween-engine";

/**
 * Test experience
 * Composer: https://short.staging.immersive.verizonmedia.com/aIb8H
 * Android: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/android/index.json
 * iOS: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/ios/index.json
 * Web: https://cdn.launch3d.com/cu/xr/preview-stable/index.html?customJsonPath=https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/web/index.json
 */

 console.log("1")
 experience.addSystem(new TweenSystem());

 console.log("2")
 experience.addObjectScript("c4189b56-e181-4ed5-be91-419e23b6c528", (rootEntity) => {
  //TODO: Composer currently inserts one extra node. Can we remove this?
  console.log("3")
  rootEntity = rootEntity.getChild(0)!.getChild(0)!;

  let i = 0;
  rootEntity.getChild(0)!.transform.forEachChild((c) => {
    if (i++ < 10) {
      c.entity!.addComponent(
        new TweenComponent({
          targets: c.localScale,
          easing: "easeInOutSine",
          y: 0.1,
          duration: Math.random() * 500 + 100,
          direction: "alternate",
          loop: true,
        })
      );
    }
  });
});

console.log("tween_10 end")
