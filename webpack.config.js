const path = require("path");
const webpack = require("webpack");
const glob = require("glob");
const inputFolder = "./immersive-scripts/";
const outputFolder = "./immersive-scripts-output/";

module.exports = (env) => {
  const entry = env.platform === "android" ? ["core-js/es/promise", "core-js/es/map"] : [];

  const buildProcess = {
    /**
     * FOR DEVELOPMENT PURPOSES ONLY
     * Serve transpiled files.
     *
     * running 'npm run serve' will serve /immersive-scripts-output
     * from https://[IP_OR_LOCALHOST]/
     * e.g https://192.168.1.1/performance_tests/tween_10/web_bundle.js
     *
     * Contents are server with self signed certificate, so make sure your platform allows it
     * (For example you have to approve it on browsers before they let you in).
     */
    devServer: {
      https: true,
      port: 9000,
      contentBase: path.join(__dirname, "immersive-scripts-output"),
      host: "0.0.0.0",
    },

    optimization: {
      minimize: false,
    },

    entry: glob.sync(inputFolder + "**/index.ts").reduce(function (entries, el) {
      const fullDir = path.parse(el).dir;
      const subFolder = fullDir.substr(fullDir.indexOf(inputFolder) + inputFolder.length, fullDir.length);

      entries[subFolder] = {
        import: [...entry, el],
      };
      return entries;
    }, {}),
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: [
            {
              loader: "ts-loader",
              options: {
                compilerOptions: {
                  target: env.platform === "android" ? "ES5" : "ES6",
                },
                "projectReferences": true
              },
            },
          ],
          exclude: /node_modules/,
        },
      ],
    },

    plugins: [
      /*new webpack.IgnorePlugin({
        contextRegExp: /arsdk/,
        resourceRegExp: /arsdk/,
      }),*/
      new webpack.optimize.LimitChunkCountPlugin({
        maxChunks: 1
      }),       
    ],

    resolve: {
      extensions: [".tsx", ".ts", ".js"],

      alias: {
        arsdk$: path.resolve(__dirname, env.platform === "android" ? "lib/arsdk_android.ts" : "lib/arsdk.ts"),
      },

      // modules: ["node_modules"], 
    },

    output: {
      filename: "[name]/" + env.platform + "-bundle.js",
      path: path.resolve(__dirname, outputFolder),
    },
  };

  if (env.platform === "web") {
    class BundlesizeWebpackPlugin {
      constructor(options) {
        this.options = options;
      }

      apply(compiler) {
        compiler.hooks.thisCompilation.tap("Replace", (compilation) => {
          compilation.hooks.processAssets.tap(
            {
              name: "Replace",
              stage: webpack.Compilation.PROCESS_ASSETS_STAGE_OPTIMIZE,
            },
            () => {
              compilation.getAssets().forEach((asset) => {
                const source = asset.source.source();

                const output = `window.startUserScript = (nativeExperience) => {${source}}; //so its done`;

                // update main.js with new content
                compilation.updateAsset(asset.name, new webpack.sources.RawSource(output));
              });
            }
          );
        });
      }
    }

    buildProcess.plugins.push(new BundlesizeWebpackPlugin());
  }

  if (env.platform === "ios") {
    delete buildProcess.resolve.alias
    buildProcess.resolve.modules = ["node_modules"]
  }

  return buildProcess;
};